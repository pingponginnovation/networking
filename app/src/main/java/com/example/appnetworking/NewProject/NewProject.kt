package com.example.appnetworking.NewProject

import com.example.appgenesys.RequestCrearProyecto
import com.example.appgenesys.ResponseCrearProyecto
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProfilePonente
import com.example.appnetworking.ResponseProyectos
import com.example.appnetworking.ResponseProyectosGeneral

interface NewProject{

    interface View{
        fun ErrorProyecto(resp: String)
        fun SuccessProyecto(response: ResponseCrearProyecto)
    }

    interface Presenter{
        fun CrearProyecto(req: RequestCrearProyecto)
        fun ErrorProyecto(resp: String)
        fun SuccessProyecto(response: ResponseCrearProyecto)
    }

    interface Model{
        fun CrearProyecto(req: RequestCrearProyecto)
        fun ErrorProyecto(resp: String)
        fun SuccessProyecto(response: ResponseCrearProyecto)
    }
}