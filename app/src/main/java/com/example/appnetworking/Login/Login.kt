//package com.example.appnetworking
package com.example.appgenesys.Retrofit

import com.example.appnetworking.ResponseLogin

interface Login{

    interface View{
        fun MakeLogin()
        fun ErrorLogin(resp: String)
        fun SuccessLogin(name: String)
        fun ErrorServer(msj: String)
    }

    interface Presenter{
        fun MakeLogin(user: String, password: String)
        fun ErrorLogin(resp: String)
        fun SuccessLogin(name: String)
        fun ErrorServer(msj: String)
    }

    interface Model{
        fun MakeLogin(user: String, password: String)
        fun ErrorLogin(resp: String)
        fun SuccessLogin(response: ResponseLogin)
        fun ErrorServer(msj: String)
    }
}