package com.example.appnetworking.Adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.NewProject.NewProjectView
import com.example.appnetworking.R

class NewProjectCategoriesAdapter: RecyclerView.Adapter<NewProjectCategoriesAdapter.ViewHolder>(){

    private var list: ArrayList<String>? = null
    private var listIDS: ArrayList<String>? = null
    private lateinit var actNewProject: NewProjectView
    data class Categories(val name: String, val id: Int)
    var miArray: ArrayList<Categories> = ArrayList()
    companion object{

        private var lastSelected: TextView? = null
        var id_categoria_selected = -1
        var category_name = ""

        fun ClearCategories(){
            if(lastSelected != null){
                lastSelected!!.setTextColor(Color.parseColor("#820A47"))
                lastSelected!!.setBackgroundResource(R.drawable.buttons_round_main)
                id_categoria_selected = -1
                category_name = ""
                lastSelected = null
            }
        }
    }

    fun NewProjectCategoriesAdapter(categories: ArrayList<String>, ids: ArrayList<String>,
                                    actNewProject: NewProjectView){
        list = categories
        listIDS = ids
        ConfigCats(list!!, listIDS!!)
        this.actNewProject = actNewProject
    }

    fun ConfigCats(list: ArrayList<String>, ids: ArrayList<String>){
        list.forEachIndexed{i, it->
            miArray.add(Categories(it, ids[i].toInt()))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val categorie = holder.itemView.findViewById(R.id.btn_categorie) as TextView
        categorie.text = miArray[position].name

        categorie.setOnClickListener{view->

            actNewProject.Autocomplete(false)

            if(lastSelected != null){
                lastSelected!!.setTextColor(Color.parseColor("#820A47"))
                lastSelected!!.setBackgroundResource(R.drawable.buttons_round_main)
                lastSelected = categorie
            }
            else lastSelected = categorie

            category_name = miArray[position].name
            id_categoria_selected = miArray[position].id
            categorie.setTextColor(Color.WHITE)
            categorie.setBackgroundResource(R.drawable.buttons_round_main_selected)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_categories_projects, parent, false))
    }

    override fun getItemCount(): Int{
        if(list!!.size > 9) return 9
        else return list!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}