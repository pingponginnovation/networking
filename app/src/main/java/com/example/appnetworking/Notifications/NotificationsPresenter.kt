package com.example.appnetworking.Notifications

import android.content.Context
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseNotificaciones

class NotificationsPresenter(viewMatch: Notifications.View, ctx: Context): Notifications.Presenter{

    private val view = viewMatch
    private val model = NotificationsModel(this, ctx)

    //MODEL
    override fun GetNotifications(){
        model.GetNotifications()
    }
    //VIEW
    override fun ErrorNotifications(resp: String){
        view.ErrorNotifications(resp)
    }

    override fun SuccessNotifications(response: ResponseNotificaciones){
        view.SuccessNotifications(response)
    }
}