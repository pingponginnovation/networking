package com.example.appnetworking.AgendaEvento

import android.content.Context
import com.example.appgenesys.RequestJoinStartMeeting
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseJoinStartMeeting

class AgendaEventoModel(viewPresenter: AgendaEvento.Presenter, ctx: Context): AgendaEvento.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetInfoCita(request: RequestDatosCita){
        val retrofit = RetrofitController(context!!)
        retrofit.GetDatosCita(this, request)
    }

    override fun JoinStartMeeting(req: RequestJoinStartMeeting){
        val retrofit = RetrofitController(context!!)
        retrofit.JoinStartMeeting(this, req)
    }

    override fun ErrorCita(resp: String){
        presenter.ErrorCita(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        presenter.SuccessInfo(response)
    }

    override fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting){
        presenter.SuccessJoinStartMeeting(response)
    }

    override fun ErrorJoinStartMeeting(resp: String){
        presenter.ErrorJoinStartMeeting(resp)
    }
}