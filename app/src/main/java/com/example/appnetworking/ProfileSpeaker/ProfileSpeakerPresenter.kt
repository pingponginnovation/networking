package com.example.appnetworking.Profile

import android.content.Context
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appnetworking.RequestSuscribeConference
import com.example.appnetworking.ResponseProfilePonente

class ProfileSpeakerPresenter(viewMatch: ProfileSpeaker.View, ctx: Context): ProfileSpeaker.Presenter{

    private val view = viewMatch
    private val model = ProfileSpeakerModel(this, ctx)

    //MODEL
    override fun GetProfile(idPonente: Int){
        model.GetProfile(idPonente)
    }

    override fun SuscribeToConference(req: RequestSuscribeConference){
        model.SuscribeToConference(req)
    }

    //VIEW
    override fun ErrorProfile(resp: String){
        view.ErrorProfile(resp)
    }

    override fun SuccessProfile(response: ResponseProfilePonente){
        view.SuccessProfile(response)
    }

    override fun SuccessSuscribeConference(response: ResponseSuscribeConference){
        view.SuccessSuscribeConference(response)
    }

    override fun ErrorSuscribe(msj: String){
        view.ErrorSuscribe(msj)
    }
}