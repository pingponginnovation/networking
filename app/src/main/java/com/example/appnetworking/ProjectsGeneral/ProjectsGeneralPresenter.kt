package com.example.appnetworking.ProjectsGeneral

import android.content.Context
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProyectos
import com.example.appnetworking.ResponseProyectosGeneral

class ProjectsGeneralPresenter(viewMatch: ProjectsGeneral.View, ctx: Context): ProjectsGeneral.Presenter{

    private val view = viewMatch
    private val model = ProjectsGeneralModel(this, ctx)

    //MODEL
    override fun GetProyectos(req: RequestProyectosGeneral){
        model.GetProyectos(req)
    }
    //VIEW
    override fun ErrorProyectos(resp: String){
        view.ErrorProyectos(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectosGeneral){
        view.SuccessProyectos(response)
    }
}