package com.example.appnetworking.ProjectsGeneral

import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProfilePonente
import com.example.appnetworking.ResponseProyectos
import com.example.appnetworking.ResponseProyectosGeneral

interface ProjectsGeneral{

    interface View{
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectosGeneral)
    }

    interface Presenter{
        fun GetProyectos(req: RequestProyectosGeneral)
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectosGeneral)
    }

    interface Model{
        fun GetProyectos(req: RequestProyectosGeneral)
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectosGeneral)
    }
}