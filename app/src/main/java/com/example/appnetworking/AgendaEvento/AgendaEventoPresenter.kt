package com.example.appnetworking.AgendaEvento

import android.content.Context
import com.example.appgenesys.RequestJoinStartMeeting
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseJoinStartMeeting

class AgendaEventoPresenter(viewMatch: AgendaEvento.View, ctx: Context): AgendaEvento.Presenter{

    private val view = viewMatch
    private val model = AgendaEventoModel(this, ctx)

    //MODEL
    override fun GetInfoCita(request: RequestDatosCita){
        model.GetInfoCita(request)
    }

    override fun JoinStartMeeting(req: RequestJoinStartMeeting){
        model.JoinStartMeeting(req)
    }

    //VIEW
    override fun ErrorCita(resp: String){
        view.ErrorCita(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        view.SuccessInfo(response)
    }

    override fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting){
        view.SuccessJoinStartMeeting(response)
    }

    override fun ErrorJoinStartMeeting(resp: String){
        view.ErrorJoinStartMeeting(resp)
    }
}