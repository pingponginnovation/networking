package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseActividadesOrder(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("actividad")
    val name: String? = null,

    @field:SerializedName("fecha_evento")
    val date_event: String? = null,

    @field:SerializedName("horainicio")
    val start_hour: String? = null,

    @field:SerializedName("flag_agregar")
    val flag_show_btn: Int? = null,

    @field:SerializedName("inscrito")
    val inscrito: Int? = null,

    @field:SerializedName("conferencia_id")
    val id_conference: Int? = null
)