package com.example.appnetworking.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.AgendaEvento.AgendaEventoView
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.asistentes
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PersonasByIntereses.PersonasByInteresesView
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.Profile.PerfilContactoView.Companion.idperfil
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import org.jetbrains.anko.toast
import java.util.ArrayList

class PeopleByInteresesAdapter: RecyclerView.Adapter<PeopleByInteresesAdapter.ViewHolder>(), Filterable{

    var list: ArrayList<ResponseMatchOrder>? = null
    private var listFiltered: List<ResponseMatchOrder>? = null
    lateinit var contexto: Activity

    fun PeopleByInteresesAdapter(lista: ArrayList<ResponseMatchOrder>, context: Activity){
        list = lista
        listFiltered = lista
        contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val name = holder.itemView.findViewById(R.id.txt_name_c) as TextView
        val job = holder.itemView.findViewById(R.id.txt_job_c) as TextView
        val city = holder.itemView.findViewById(R.id.txt_city) as TextView
        val foto = holder.itemView.findViewById(R.id.my_img) as ImageView

        name.text = listFiltered!![position].name + " " + listFiltered!![position].apellidos
        job.text = listFiltered!![position].ocupation
        city.text = listFiltered!![position].address
        Picasso.get().load(listFiltered!![position].img_perfil).into(foto)

        holder.itemView.setOnClickListener{view ->
            val intent = Intent(contexto, PerfilContactoView::class.java)
            intent.putExtra("ID_PERFIL", listFiltered!![position].id)
            intent.putExtra("FROM", "MY_CONTACT")
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_people_by_intereses, parent, false))
    }

    override fun getItemCount(): Int{
        return listFiltered!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {

                val charString = charSequence.toString()

                if (charString.isEmpty()) listFiltered = list
                else{
                    val filteredList = ArrayList<ResponseMatchOrder>()
                    for (row in list!!) {

                        if (row.name.toString().toLowerCase().contains(charString.toLowerCase()) || row.apellidos.toString().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row)
                    }
                    listFiltered = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                listFiltered = filterResults.values as ArrayList<ResponseMatchOrder>
                notifyDataSetChanged()
            }
        }
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}