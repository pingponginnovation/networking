package com.example.appnetworking.Matches

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.appnetworking.Adapters.CardsMatchAdapter
import com.example.appnetworking.Adapters.CardsMatchAdapter.Companion.auxiliar
import com.example.appnetworking.CoreActivity
import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import com.lorentzos.flingswipe.SwipeFlingAdapterView
import kotlinx.android.synthetic.main.activity_matches.view.*

class MatchesView(core: CoreActivity): Fragment(){

    private var arrayAdapter: CardsMatchAdapter? = null
    private lateinit var preferences: PreferencesController
    var coreAct = core
    private var aux = 0
    companion object{
        lateinit var arrayInfoMatches: ArrayList<ResponseMatchOrder>
        lateinit var flingContainer: SwipeFlingAdapterView
        var type = "matches"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):View{
        val view: View = inflater.inflate(R.layout.activity_matches, container, false)

        preferences = PreferencesController(context!!)
        //if(arrayInfoMatches.isNullOrEmpty()) view.LY_NORESULTS.visibility = View.VISIBLE
        //else view.LY_NORESULTS.visibility = View.GONE

        flingContainer = view.findViewById(R.id.frame_swipecard)
        arrayAdapter = CardsMatchAdapter(view.context, arrayInfoMatches, coreAct)
        flingContainer.adapter = arrayAdapter
        flingContainer.setFlingListener(object : SwipeFlingAdapterView.onFlingListener{

            override fun removeFirstObjectInAdapter(){
                Log.e("remove", "remove first")
                arrayInfoMatches.removeAt(0)
                arrayAdapter!!.notifyDataSetChanged()
            }

            override fun onLeftCardExit(dataObject: Any?){
                //Auxiliar its used to validate where the click comes from, beacuse click on button Tick
                //from CardMatchAdapter, activate this method. So we need to check if the action is from
                //sliding the card or from the Tick button, this is just for avoid showing 2 times the AlertDialog
                if(auxiliar == 0){
                    val item: ResponseMatchOrder = dataObject as ResponseMatchOrder
                    coreAct.SetLikeDislike(item.id.toString(), "0")
                }
                else auxiliar = 0
            }

            override fun onRightCardExit(dataObject: Any?){

                if(auxiliar == 0){
                    val item: ResponseMatchOrder = dataObject as ResponseMatchOrder
                    coreAct.SetLikeDislike(item.id.toString(), "1")
                }
                else auxiliar = 0
            }

            override fun onAdapterAboutToEmpty(itemsInAdapter: Int){
                // Ask for more data here
                /*datosArray!!.add(itemsInAdapter)
                arrayAdapter!!.notifyDataSetChanged()
                Log.d("LIST", "notified")
                i++*/
            }

            override fun onScroll(scrollProgressPercent: Float) {
            }
        })

        flingContainer.setOnItemClickListener{itemPosition, dataObject ->
            if(aux == 0){
                val item: ResponseMatchOrder = flingContainer.getItemAtPosition(itemPosition) as ResponseMatchOrder
                coreAct.SearchContacto(item.id!!.toInt())
                aux = 1
            }
        }
        return view
    }

    override fun onResume(){
        super.onResume()
        Log.e("RESUME", "MATCHES")
        aux = 0
        val pause = preferences.getPause()
        //if(pause.equals("pause")) coreAct.GetMatches(coreAct.filtroActivo, "")
        type = "matches"
    }

    override fun onPause(){
        super.onPause()
        preferences.SetPausa("pause")
    }
}