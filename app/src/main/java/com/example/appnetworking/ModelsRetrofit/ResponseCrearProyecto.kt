//package com.example.appnetworking
package com.example.appgenesys

import com.google.gson.annotations.SerializedName

data class ResponseCrearProyecto(
    @SerializedName("mensaje")
    val msj: String
)