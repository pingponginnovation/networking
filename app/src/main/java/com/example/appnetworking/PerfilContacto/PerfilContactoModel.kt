package com.example.appnetworking.Profile

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponsePerfilContacto
import com.example.appnetworking.ResponseProyectos

class PerfilContactoModel(viewPresenter: PerfilContacto.Presenter, ctx: Context): PerfilContacto.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetDataPerfil(id_perfil: Int){
        val retrofit = RetrofitController(context!!)
        retrofit.SearchContactoNotification(this, id_perfil)
    }

    override fun SuccessPerfil(resp: ResponsePerfilContacto){
        presenter.SuccessPerfil(resp)
    }
}