package com.example.appnetworking.Proyecto

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.*
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyecto

class ProyectoModel(viewPresenter: Proyecto.Presenter, ctx: Context): Proyecto.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun EditProyecto(req: RequestModificarProyecto){
        val retrofit = RetrofitController(context!!)
        retrofit.EditProyecto(this, req)
    }

    override fun VotaProyecto(req: RequestVotarProyecto){
        val retrofit = RetrofitController(context!!)
        retrofit.VotaProyecto(this, req)
    }

    override fun ErrorEdit(resp: String){
        presenter.ErrorEdit(resp)
    }

    override fun SuccessEdit(response: ResponseModificarProyecto){
        presenter.SuccessEdit(response)
    }

    override fun SuccessVotacion(resp: ResponseVotarProyecto){
        presenter.SuccessVotacion(resp)
    }

    override fun ErrorVotacion(resp: String){
        presenter.ErrorVotacion(resp)
    }
}