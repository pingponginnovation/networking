package com.example.appnetworking.ContactosAndChats

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.example.appnetworking.Adapters.TabsAdapter
import com.example.appnetworking.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.tabs_viewpager_activity.*

class TabsViewpagerActivity: AppCompatActivity(){

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tabs_viewpager_activity)

        ConfigToolbar()
        tabLayout = findViewById(R.id.tabLayout)
        viewPager = findViewById(R.id.viewPager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText("Contacts"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Chats"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = TabsAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {
            }
            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)

        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}