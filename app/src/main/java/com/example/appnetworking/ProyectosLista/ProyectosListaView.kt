package com.example.appnetworking.ProyectosLista

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.ProyectosAdapter
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosCategoriasOrder
import com.example.appnetworking.NewProject.NewProjectView
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.Proyecto.EditProyectView.Companion.responseEditado
import com.example.appnetworking.R
import com.example.appnetworking.ResponseProyectos
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.proyectos_activity.*
import org.jetbrains.anko.toast

class ProyectosListaView: AppCompatActivity(), ProyectosLista.View{

    private lateinit var preferences: PreferencesController
    private lateinit var presenter: ProyectosLista.Presenter
    lateinit var responseProyecto: ResponseProyectos
    var name = ""
    var puesto = ""
    var direcc = ""
    var img = ""
    lateinit var idprofile: String
    companion object{
        var categoriasArray = arrayListOf<String>() //Usado en NewProject
        var IDScategoriasArray = arrayListOf<String>() //Este igual
        var categoriesArray: ArrayList<ResponseProyectosCategoriasOrder> = ArrayList()//Usado en EditProyectView
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.proyectos_activity)

        preferences = PreferencesController(this)
        ConfigToolbar()
        presenter = ProyectosListaPresenter(this, applicationContext)
        GetInfo()
        idprofile = intent.getStringExtra("ID_PROFILE")
        ListaProyectos(idprofile.toInt())

        if(idprofile.toInt() != preferences.getMyIDprofile()!!.toInt()) btn_add_project.visibility = View.GONE

        btn_add_project.setOnClickListener{
            val intent = Intent(this, NewProjectView::class.java)
            intent.putStringArrayListExtra("CATEGORIES", categoriasArray)
            intent.putStringArrayListExtra("CATEGORIES_IDS", IDScategoriasArray)
            startActivity(intent)
        }
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun GetInfo(){
        name = "${intent.getStringExtra("Nombre")!!} ${intent.getStringExtra("APELLIDOS")}"
        puesto = intent.getStringExtra("Cargo")!!
        direcc = intent.getStringExtra("Direccion")!!
        img = intent.getStringExtra("Foto")!!
        txt_name.text = name
        txt_ocupacion.text = puesto
        txt_residencia.text = direcc
        Picasso.get().load(img).into(img_myphoto)
    }

    fun ListaProyectos(id: Int){
        presenter.GetProyectos(id)
    }

    override fun ErrorProyectos(resp: String){
        toast(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectos){

        categoriesArray = response.categories

        response.categories.forEach{
            categoriasArray.add(it.categoria!!)
            IDScategoriasArray.add(it.id_categorie.toString())
        }
        responseProyecto = response
        SetProyectos()
    }

    fun SetProyectos(){
        rclv_proyectos.setHasFixedSize(true)
        rclv_proyectos.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val arrayAdapter = ProyectosAdapter()
        arrayAdapter.ProyectosAdapter(responseProyecto.data, this, name, puesto, direcc, img)
        rclv_proyectos.adapter = arrayAdapter
    }

    override fun onResume(){
        super.onResume()
        responseEditado = null //Para mandar la info cuando volvamos a entrar a detalle del proyecto
        ListaProyectos(idprofile.toInt()) //For update data
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}