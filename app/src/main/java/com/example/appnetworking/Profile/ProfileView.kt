package com.example.appnetworking.Profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseDeleteInteres
import com.example.appgenesys.ResponseDeleteSkill
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.Adapters.ExperiencesAdapter
import com.example.appnetworking.Adapters.InterestsAdapter
import com.example.appnetworking.Adapters.SkillsAdapter
import com.example.appnetworking.Models.InteresesForEdit
import com.example.appnetworking.Models.SkillsForEdit
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ProyectosLista.ProyectosListaView
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_myprofile_new.*
import kotlinx.android.synthetic.main.item_speakers.txt_name
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class ProfileView: AppCompatActivity(), Profile.View{

    private lateinit var preferences: PreferencesController
    private lateinit var presenter: Profile.Presenter
    private var arrayAdapter: ExperiencesAdapter? = null
    var aux = "no pause"
    var miID = -1
    private var expanded = false
    private var expanded_about = false
    private var img: String? = null
    private var myIntereses: ArrayList<InteresesForEdit> = arrayListOf()
    private var mySkills: ArrayList<SkillsForEdit> = arrayListOf()
    companion object{
            //Para usarlo en ShowToEditExperiences
        var responseExperiences: List<ResponseProfileExperienciasOrder>? = null
            //Para usarlo en EditExperience
        var responseCatalogoEmpresas: ArrayList<ResponseProfileCatalogoEmpresasOrder> = ArrayList()
            //Para usarlo en EditExperience
        var responseJornadas: ArrayList<ResponseProfileJornadasOrder> = ArrayList()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_myprofile_new)

        preferences = PreferencesController(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back)
        presenter = ProfilePresenter(this, applicationContext)
        GetProfile()

        btn_my_projects.setOnClickListener{
            val intent = Intent(this, ProyectosListaView::class.java)
            intent.putExtra("Nombre", txt_name.text.toString())
            intent.putExtra("APELLIDOS", txt_apellidos.text.toString())
            intent.putExtra("Cargo", txt_ocupacion.text.toString())
            intent.putExtra("Direccion", txt_residencia.text.toString())
            intent.putExtra("Foto", img)
            intent.putExtra("ID_PROFILE", preferences.getMyIDprofile())
            startActivity(intent)
        }

        see_more.setOnClickListener{
            if(!expanded){
                expanded = true
                expandable.expand()
                see_more.text = "Hide"
            }
            else{
                expandable.collapse()
                expanded = false
                see_more.text = "See more"
            }
        }

        see_more_about.setOnClickListener{
            if(!expanded_about){
                expanded_about = true
                expandable_about1.collapse()
                expandable_about.expand()
                see_more_about.text = "Hide"
            }
            else{
                expandable_about1.expand()
                expandable_about.collapse()
                expanded_about = false
                see_more_about.text = "See more"
            }
        }

        btn_edit.setOnClickListener{
            val intent = Intent(this, EditPerfil::class.java)
            intent.putExtra("NAME", txt_name.text.toString())
            intent.putExtra("APELLIDOS", txt_apellidos.text.toString())
            intent.putExtra("PUESTO", txt_ocupacion.text.toString())
            intent.putExtra("CITY", txt_residencia.text.toString())
            intent.putExtra("PHONE", txt_phone.text.toString())
            intent.putExtra("EMAIL", txt_email.text.toString())
            intent.putExtra("WEBSITE", txt_web_n.text.toString())
            intent.putExtra("BIOGRAPHY", txt_about.text.toString())
            intent.putExtra("MI_ID", miID)
            intent.putExtra("IMAGEN", img)
            startActivity(intent)
        }

        btn_edit_experiences.setOnClickListener{
            startActivity<ShowToEditExperiences>()
        }

        btn_edit_about.setOnClickListener{
            val intent = Intent(this, EditBiography::class.java)
            intent.putExtra("BIOGRAPHY", txt_about.text.toString())
            startActivity(intent)
        }

        btn_edit_intereses.setOnClickListener{
            val intent = Intent(this, EditIntereses::class.java)
            intent.putExtra("INTERESES", myIntereses)
            startActivity(intent)
        }

        btn_edit_skills.setOnClickListener{
            val intent = Intent(this, EditSkills::class.java)
            intent.putExtra("SKILLS", mySkills)
            startActivity(intent)
        }
    }

    fun GetProfile(){
        presenter.GetProfile()
    }

    override fun ErrorProfile(resp: String){
        toast(resp)
    }

    override fun SuccessProfile(response: ResponseProfile){
        responseCatalogoEmpresas = response.catalogo_empresas
        responseJornadas = response.jornadas

        SetData(response.data)
        SetExperiences(response.data.experiences!!.data)
        SetFirst2Experiences(response.data.experiences.data)
        SetInterest(response.data.interest!!.data)
        SetSkills(response.data.habilidades!!.data)

        LY_LOADER_PROFILE.visibility = View.GONE
        LY_PROFILE.visibility = View.VISIBLE
    }

    fun SetData(datos: ResponseProfileOrder){

        miID = datos.id!!.toInt()

        txt_name.setText(datos.name)
        txt_apellidos.setText(datos.apellidos)
        txt_ocupacion.setText(datos.ocupation)
        txt_residencia.setText(datos.address)
        txt_phone.setText(datos.cel)
        txt_email.setText(datos.email)
        txt_web_n.setText(datos.web)
        txt_about_some.text = "About ${datos.name}"
        txt_about.text = datos.description
        txt_about_more.text = datos.description //inside of expandable layout
        img = datos.img_perfil.toString()
        Picasso.get().load(datos.img_perfil).into(img_myphoto)
    }

    fun SetFirst2Experiences(response: List<ResponseProfileExperienciasOrder>){

        if(response.isNullOrEmpty()){
            see_more.visibility = View.GONE
            EXPERIENCE_NO.visibility = View.VISIBLE
            EXPERIENCE_1.visibility = View.GONE
            EXPERIENCE_2.visibility = View.GONE
        }
        else if(response.size == 1){
            see_more.visibility = View.GONE
            EXPERIENCE_2.visibility = View.GONE

            Picasso.get().load(response[0].logo).error(R.drawable.error_img).into(img_experience_1)
            txt_empresa_1.text = response[0].empresa
            txt_cargo_1.text = response[0].cargo
            if(response[0].actualmente == 1) txt_fecha_1.text = "${response[0].date_start} - Presente"
            else txt_fecha_1.text = "${response[0].date_start} - ${response[0].date_end}"
            txt_years_woking_1.text = response[0].year_expe
            //txt_ubicacion_1.text = response[0].location
        }
        else{

            if(response.size == 2) see_more.visibility = View.GONE

            Picasso.get().load(response[0].logo).error(R.drawable.error_img).into(img_experience_1)
            txt_empresa_1.text = response[0].empresa
            txt_cargo_1.text = response[0].cargo
            if(response[0].actualmente == 1) txt_fecha_1.text = "${response[0].date_start} - Presente"
            else txt_fecha_1.text = "${response[0].date_start} - ${response[0].date_end}"
            txt_years_woking_1.text = response[0].year_expe
            //txt_ubicacion_1.text = response[0].location

            Picasso.get().load(response[1].logo).error(R.drawable.error_img).into(img_experience_2)
            txt_empresa_2.text = response[1].empresa
            txt_cargo_2.text = response[1].cargo
            if(response[1].actualmente == 1) txt_fecha_2.text = "${response[1].date_start} - Presente"
            else txt_fecha_2.text = "${response[1].date_start} - ${response[1].date_end}"
            txt_years_woking_2.text = response[1].year_expe
            //txt_ubicacion_2.text = response[1].location
        }
    }

    fun SetExperiences(response: List<ResponseProfileExperienciasOrder>){

        responseExperiences = response
        rclv_experiences.setHasFixedSize(true)
        rclv_experiences.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        arrayAdapter = ExperiencesAdapter()
        arrayAdapter!!.ExperiencesAdapter(response, this, "Profile")
        rclv_experiences.adapter = arrayAdapter
    }

    fun SetInterest(response: List<ResponseProfileInteresesOrder>){

        response.forEach{
            myIntereses.add(InteresesForEdit(it.interes!!, it.id!!))
        }

        rclv_interests.setHasFixedSize(true)
        rclv_interests.layoutManager = GridLayoutManager(this, 4)
        //rclv_interests.addItemDecoration(GridItemDecoration(10, 2))
        val arrayAdapter = InterestsAdapter()
        arrayAdapter.InterestsAdapter(response, this)
        rclv_interests.adapter = arrayAdapter
    }

    fun SetSkills(response: List<ResponseProfileHabilidadesOrder>){

        response.forEach{
            mySkills.add(SkillsForEdit(it.skill!!, it.id!!))
        }

        rclv_skills.setHasFixedSize(true)
        rclv_skills.layoutManager = GridLayoutManager(this, 4)
        //rclv_skills.addItemDecoration(GridItemDecoration(10, 2))
        val arrayAdapter = SkillsAdapter()
        arrayAdapter.SkillsAdapter(response, this)
        rclv_skills.adapter = arrayAdapter
    }

    override fun onResume(){
        super.onResume()
        if(aux.equals("pause")){
            myIntereses.clear()
            mySkills.clear()
            GetProfile()
            aux = "no pause"
        } //For update data
    }

    override fun onPause(){
        super.onPause()
        aux = "pause"
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }


    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorCrearExperiencia(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SucccessEdit(resp: ResponseEditExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorEdit(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessEditBiography(response: ResponseEditBiography) {
        TODO("Not yet implemented")
    }
    override fun ErrorEditBiography(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessInterest(response: ResponseCrearInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessSkill(response: ResponseCrearSkill) {
    }
    override fun ErrorSkill(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteSkill(resp: String) {
        TODO("Not yet implemented")
    }
}