package com.example.appnetworking.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.R
import com.squareup.picasso.Picasso

class ExperiencesContactoAdapter(items: List<ResponseProfileExperienciasOrder>, context: Context): RecyclerView.Adapter<ExperiencesContactoAdapter.ViewHolder>(){

    var experiencias: List<ResponseProfileExperienciasOrder> = items
    var contexto: Context = context

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_empresa) as TextView
        val puesto = holder.itemView.findViewById(R.id.txt_cargo) as TextView
        val years = holder.itemView.findViewById(R.id.txt_years) as TextView
        val fecha = holder.itemView.findViewById(R.id.txt_fecha) as TextView
        val logo = holder.itemView.findViewById(R.id.img_logo_contacto) as ImageView

        titulo.text = experiencias[position].empresa
        puesto.text = experiencias[position].cargo
        years.text = experiencias[position].year_expe

        if(experiencias[position].actualmente == 1) fecha.text = "${experiencias[position].date_start} - Presente"
        else fecha.text = "${experiencias[position].date_start} - ${experiencias[position].date_end}"

        Picasso.get().load(experiencias[position].logo).into(logo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_experiences_contacto, parent, false))
    }

    override fun getItemCount(): Int{
        return experiencias.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

    }
}