package com.example.appnetworking.Proyecto

import com.example.appnetworking.*
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyecto

interface Proyecto{

    interface View{
        fun ErrorEdit(resp: String)
        fun SuccessEdit(response: ResponseModificarProyecto)

        fun SuccessVotacion(resp: ResponseVotarProyecto)
        fun ErrorVotacion(resp: String)
    }

    interface Presenter{
        fun EditProyecto(req: RequestModificarProyecto)
        fun ErrorEdit(resp: String)
        fun SuccessEdit(response: ResponseModificarProyecto)

        fun VotaProyecto(req: RequestVotarProyecto)
        fun SuccessVotacion(resp: ResponseVotarProyecto)
        fun ErrorVotacion(resp: String)
    }

    interface Model{
        fun EditProyecto(req: RequestModificarProyecto)
        fun ErrorEdit(resp: String)
        fun SuccessEdit(response: ResponseModificarProyecto)

        fun VotaProyecto(req: RequestVotarProyecto)
        fun SuccessVotacion(resp: ResponseVotarProyecto)
        fun ErrorVotacion(resp: String)
    }
}