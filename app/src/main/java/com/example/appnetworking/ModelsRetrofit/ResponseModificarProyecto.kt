package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ResponseModificarProyecto(
    @field:SerializedName("mensaje")
    val msj: String? = null,
    @field:SerializedName("proyecto")
    val response: ResponseModificarProyectoOrder? = null
)