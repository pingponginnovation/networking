package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.example.appgenesys.ResponseContacts
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseChatThreadOrder(

    @field:SerializedName("perfil_id")
    val perfil_id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("url_perfil")
    val img: String? = null,

    @field:SerializedName("mensaje")
    val msj: String? = null,

    @field:SerializedName("fecha")
    val fecha: String? = null
)