package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileExperiencias(
    @SerializedName("datos")
    val data: List<ResponseProfileExperienciasOrder>
)