package com.example.appnetworking.Profile

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import at.huber.youtubeExtractor.VideoMeta
import at.huber.youtubeExtractor.YouTubeExtractor
import at.huber.youtubeExtractor.YtFile
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.*
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyecto
import com.example.appnetworking.Proyecto.EditProyectView
import com.example.appnetworking.Proyecto.EditProyectView.Companion.responseEditado
import com.example.appnetworking.Proyecto.Proyecto
import com.example.appnetworking.Proyecto.ProyectoPresenter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.proyecto_show.*

class ProyectoView: AppCompatActivity(), Proyecto.View{

    private lateinit var presenter: Proyecto.Presenter
    private var controlAlert: ControllerAlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.proyecto_show)

        controlAlert = ControllerAlertDialog(this)
        presenter = ProyectoPresenter(this, applicationContext)
        ConfigToolbar()
        SetData()
        HandleClick()

        btn_edit_proyect.setOnClickListener{

            if(responseEditado != null){
                val intentAct = Intent(this, EditProyectView::class.java)
                intentAct.putExtra("TITULO", responseEditado!!.titulo)
                intentAct.putExtra("DESCRIPCION", responseEditado!!.descripcion)
                intentAct.putExtra("VIDEO", responseEditado!!.video)
                intentAct.putExtra("FOTO_PROYECTO", responseEditado!!.img_proyecto)
                intentAct.putExtra("CATEGORIA_NAME", responseEditado!!.categoria_name)
                intentAct.putExtra("IDPROYECTO", responseEditado!!.id_proyecto)
                intentAct.putExtra("ESTATUS", responseEditado!!.status)
                startActivity(intentAct)
            }
            else{
                val intentAct = Intent(this, EditProyectView::class.java)
                intentAct.putExtra("TITULO", intent.getStringExtra("TITULO"))
                intentAct.putExtra("DESCRIPCION", intent.getStringExtra("DESCRIPCION"))
                intentAct.putExtra("VIDEO", intent.getStringExtra("VIDEO"))
                intentAct.putExtra("FOTO_PROYECTO", intent.getStringExtra("FOTO_PROYECTO"))
                intentAct.putExtra("CATEGORIA_NAME", intent.getStringExtra("CATEGORIA"))
                intentAct.putExtra("IDPROYECTO", intent.getIntExtra("ID_PROYECTO", -1))
                intentAct.putExtra("ESTATUS", intent.getIntExtra("ESTATUS", -1))
                startActivity(intentAct)
            }
        }
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun HandleClick(){

        val option = intent.getStringExtra("OPTION_VOTED")
        val id = intent.getIntExtra("ID_PROYECTO", -1)

        if(option!!.equals("donate")){ //We mark the option that the user had voted before
            btn_donate.setBackgroundResource(R.drawable.button_voted_option)
            btn_donate.setTextColor(Color.WHITE)
        }
        else if(option.equals("invest")){
            btn_invest.setBackgroundResource(R.drawable.button_voted_option)
            btn_invest.setTextColor(Color.WHITE)
        }
        else if(option.equals("contribute")){
            btn_contribute.setBackgroundResource(R.drawable.button_voted_option)
            btn_contribute.setTextColor(Color.WHITE)
        }

        btn_donate.setOnClickListener{
            controlAlert!!.ProgressBar()
            presenter.VotaProyecto(RequestVotarProyecto(id.toInt(), "donate"))
            btn_donate.setBackgroundResource(R.drawable.button_voted_option)
            btn_donate.setTextColor(Color.WHITE)
            DesmarkButtons("donate")
        }

        btn_invest.setOnClickListener{
            controlAlert!!.ProgressBar()
            presenter.VotaProyecto(RequestVotarProyecto(id.toInt(), "invest"))
            btn_invest.setBackgroundResource(R.drawable.button_voted_option)
            btn_invest.setTextColor(Color.WHITE)
            DesmarkButtons("invest")
        }

        btn_contribute.setOnClickListener{
            controlAlert!!.ProgressBar()
            presenter.VotaProyecto(RequestVotarProyecto(id.toInt(), "contribute"))
            btn_contribute.setBackgroundResource(R.drawable.button_voted_option)
            btn_contribute.setTextColor(Color.WHITE)
            DesmarkButtons("contribute")
        }
    }

    fun DesmarkButtons(name: String){

        if(name.equals("donate")){
            btn_invest.setBackgroundResource(R.drawable.button_add_contact)
            btn_invest.setTextColor(Color.parseColor("#820A47"))
            btn_contribute.setBackgroundResource(R.drawable.button_add_contact)
            btn_contribute.setTextColor(Color.parseColor("#820A47"))
        }
        else if(name.equals("invest")){
            btn_contribute.setBackgroundResource(R.drawable.button_add_contact)
            btn_contribute.setTextColor(Color.parseColor("#820A47"))
            btn_donate.setBackgroundResource(R.drawable.button_add_contact)
            btn_donate.setTextColor(Color.parseColor("#820A47"))
        }
        else{
            btn_donate.setBackgroundResource(R.drawable.button_add_contact)
            btn_donate.setTextColor(Color.parseColor("#820A47"))
            btn_invest.setBackgroundResource(R.drawable.button_add_contact)
            btn_invest.setTextColor(Color.parseColor("#820A47"))
        }
    }

    fun SetData(){
        var video = ""
        val flag = intent.getBooleanExtra("FLAG_EDITAR", false)
        if(flag) btn_edit_proyect.visibility = View.VISIBLE
        else btn_edit_proyect.visibility = View.GONE

        if(responseEditado == null){
            Picasso.get().load(intent.getStringExtra("IMG")).into(img_myphoto)
            txt_name.text = intent.getStringExtra("NAME")
            txt_ocupacion.text = intent.getStringExtra("PUESTO")
            txt_residencia.text = intent.getStringExtra("DIRECC")
            txt_titulo_pr.text = intent.getStringExtra("TITULO")
            txt_descripcion_pr.text = intent.getStringExtra("DESCRIPCION")
            if(!intent.getStringExtra("VIDEO").isNullOrEmpty())
                video = intent.getStringExtra("VIDEO")!!
        }
        else{
            btn_edit_proyect.visibility = View.VISIBLE
            Picasso.get().load(responseEditado!!.img_proyecto).into(img_replace)
            Log.e("FTO", responseEditado!!.img_proyecto.toString())
            txt_titulo_pr.text = responseEditado!!.titulo
            txt_descripcion_pr.text = responseEditado!!.descripcion
            if(!responseEditado!!.video.isNullOrEmpty()) video = responseEditado!!.video!!
        }

        if(!video.isEmpty() && video.contains("https://")){

            img_replace.visibility = View.GONE

            var videoDownloaded: String?

            object: YouTubeExtractor(this) {
                override fun onExtractionComplete(ytFiles: SparseArray<YtFile>, vMeta: VideoMeta){
                    if (ytFiles != null){

                        val itag = 22
                        videoDownloaded = ytFiles[itag].url

                        video_view.setVideoURI(Uri.parse(videoDownloaded))
                        video_view.setOnPreparedListener{
                            video_view.start()
                        }
                    }
                }
            }.extract(video, true, true)
        }
        else{
            video_view.visibility = View.GONE
            if(!intent.getStringExtra("FOTO_PROYECTO").isNullOrEmpty())
                Picasso.get().load(intent.getStringExtra("FOTO_PROYECTO")).into(img_replace)
        }
    }

    override fun SuccessVotacion(resp: ResponseVotarProyecto){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp.msj!!)
    }

    override fun ErrorVotacion(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }

    override fun onResume() {
        super.onResume()
        if(responseEditado != null) SetData()
    }


    override fun ErrorEdit(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessEdit(response: ResponseModificarProyecto) {
        TODO("Not yet implemented")
    }
}


