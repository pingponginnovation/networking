package com.example.appnetworking.Proyecto

import android.content.Context
import com.example.appnetworking.*
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyecto

class ProyectoPresenter(viewMatch: Proyecto.View, ctx: Context): Proyecto.Presenter{

    private val view = viewMatch
    private val model = ProyectoModel(this, ctx)

    //MODEL
    override fun EditProyecto(req: RequestModificarProyecto){
        model.EditProyecto(req)
    }

    override fun VotaProyecto(req: RequestVotarProyecto){
        model.VotaProyecto(req)
    }

    //VIEW
    override fun ErrorEdit(resp: String){
        view.ErrorEdit(resp)
    }

    override fun SuccessEdit(response: ResponseModificarProyecto){
        view.SuccessEdit(response)
    }

    override fun SuccessVotacion(resp: ResponseVotarProyecto){
        view.SuccessVotacion(resp)
    }

    override fun ErrorVotacion(resp: String){
        view.ErrorVotacion(resp)
    }
}