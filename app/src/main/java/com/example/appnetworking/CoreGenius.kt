package com.example.appnetworking

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.appnetworking.Genius.Genius
import com.example.appnetworking.Genius.GeniusPresenter
import kotlinx.android.synthetic.main.core_genius_activity.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class CoreGenius: AppCompatActivity(), Genius.View{

    private lateinit var presenter: Genius.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.core_genius_activity)

        presenter = GeniusPresenter(this, applicationContext)
        presenter.GetInformacion()

        btn_start.setOnClickListener{
            startActivity<CoreActivity>()
            finish()
        }
    }

    override fun ErrorGenius(resp: String) {
        toast(resp)
    }

    override fun SuccessInfo(response: ResponseInfoGenius){
        SetData(response)
    }

    fun SetData(datos: ResponseInfoGenius){
        txt_welcome.text = "Hi ${intent.getStringExtra("NAME")}"
        txt_info_cg.text = datos.data.sinopsis
    }
}