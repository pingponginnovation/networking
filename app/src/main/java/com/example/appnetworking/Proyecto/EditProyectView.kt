package com.example.appnetworking.Proyecto

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.example.appnetworking.*
import com.example.appnetworking.Adapters.EditProjectCategoriesAdapter
import com.example.appnetworking.Adapters.EditProjectCategoriesAdapter.Companion.ClearCategoriesList
import com.example.appnetworking.Adapters.EditProjectCategoriesAdapter.Companion.categoriaActivada
import com.example.appnetworking.Adapters.EditProjectCategoriesAdapter.Companion.category_namee
import com.example.appnetworking.Adapters.EditProjectCategoriesAdapter.Companion.id_categoria_selectedd
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyecto
import com.example.appnetworking.ModelsRetrofit.ResponseModificarProyectoOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosCategoriasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosGeneralCategoriesOrder
import com.example.appnetworking.ProjectsGeneral.ProjectsGeneralView.Companion.categoriesArray2
import com.example.appnetworking.ProyectosLista.ProyectosListaView.Companion.categoriesArray
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nuevo_proyecto_activity.*
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream

class EditProyectView: AppCompatActivity(), Proyecto.View{

    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: Proyecto.Presenter
    private var adaptador_dropdownList: ArrayAdapter<String>? = null
    private val IMAGE_PICK_CODE = 1000;
    private val PERMISSION_CODE = 1001;
    private var encodedImage = "nothing"
    private lateinit var preferences: PreferencesController
    private var addedCategoryBtn = 0
    private var flag_category = -1
    private var categoria = ""
    private var id_category = -1
    private var newImage = 0
    private var project_activo = 1
    companion object{
            //Este es para actualizar la info en la vista de detalle del proyecto
        var responseEditado: ResponseModificarProyectoOrder? = null
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nuevo_proyecto_activity)

        controlAlert = ControllerAlertDialog(this)
        presenter = ProyectoPresenter(this, applicationContext)
        preferences = PreferencesController(applicationContext)
        adaptador_dropdownList = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)
        GetExtras()
        ConfigToolbar()
        SetCategories()

        btn_upload.setOnClickListener{
            CheckPermissionGallery()
        }

        btn_save.setOnClickListener{
            if(edt_projectname.text.isNullOrEmpty() || edt_description.text.isNullOrEmpty() ||
                encodedImage.equals("nothing") ||
                (id_categoria_selectedd == -1 && autocomp_more_cats.text.toString().isEmpty())){
                toast("Please fill in all fields")
            }
            else{

                val id_proyecto = intent.getIntExtra("IDPROYECTO", -1)
                val flag_changed_image: Int

                if(newImage == 1) flag_changed_image = 1
                else flag_changed_image = 0

                Log.e("merma", category_namee)
                id_category = SeearchCategoriaID(category_namee)
                if(id_category == -1) flag_category = 0 //nueva categoria
                else flag_category = 1 //categoria existente

                if(addedCategoryBtn == 1)category_namee = autocomp_more_cats.text.toString()
                else flag_category = 1

                val req = RequestModificarProyecto(id_proyecto, preferences.getMyIDprofile()!!.toInt(),
                edt_projectname.text.toString(), flag_category.toString(), category_namee, id_category.toString(),
                edt_description.text.toString(), edt_urlvideo.text.toString(), flag_changed_image.toString(),
                    encodedImage, project_activo)

                Log.e("Req", req.toString())

                controlAlert!!.ProgressBar()
                presenter.EditProyecto(req)
            }
        }

        btn_add.setOnClickListener{
            if(autocomp_more_cats.text.toString().isEmpty()) toast("Please type a category")
            else{
                addedCategoryBtn = 1
                Autocomplete(true)
            }
        }

        switch_project?.setOnCheckedChangeListener {_, isChecked ->
            if(isChecked){
                switch_project.setText("Enable project")
                project_activo = 0
                switch_project.isChecked = isChecked
            }
            else{
                switch_project.setText("Disable project")
                project_activo = 1
                switch_project.isChecked = isChecked
            }
        }

        if(intent.getIntExtra("ESTATUS", -1) == 0){
            switch_project.isChecked = true
            switch_project.setText("Enable project")
        }

        autocomp_more_cats.addTextChangedListener(yourTextWatcher)
    }

    protected var yourTextWatcher: TextWatcher = object: TextWatcher{
        override fun afterTextChanged(s: Editable){}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int){}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int){
            if(s.isEmpty()) Autocomplete(false)
        }
    }

    private fun SeearchCategoriaID(name: String): Int{

        Log.e("name", name)
        var id = -1

        if(categoriesArray2.isNullOrEmpty()){
            categoriesArray.forEach{
                if(it.categoria.equals(name, true)) id = it.id_categorie!!
            }
        }
        else{
            categoriesArray2.forEach{
                if(it.categoria.equals(name, true)) id = it.id_categorie!!
            }
        }
        return id
    }

    private fun GetExtras(){
        categoria = intent.getStringExtra("CATEGORIA_NAME")!!
        edt_projectname.setText(intent.getStringExtra("TITULO"))
        edt_description.setText(intent.getStringExtra("DESCRIPCION"))
        encodedImage = intent.getStringExtra("FOTO_PROYECTO")!!
        Picasso.get().load(encodedImage).into(img_project)
        edt_urlvideo.setText(intent.getStringExtra("VIDEO"))
    }

    fun Autocomplete(state: Boolean){
        if(state){
            ClearCategoriesList()
            autocomp_more_cats.setTextColor(Color.WHITE)
            autocomp_more_cats.setBackgroundResource(R.drawable.design_autocomplete_selected)
            autocomp_more_cats.clearFocus()
            HideKeyboard()
        }
        else{
            if(autocomp_more_cats.text.toString().isNotEmpty() || addedCategoryBtn == 1){
                addedCategoryBtn = 0
                autocomp_more_cats.setText("")
                autocomp_more_cats.setTextColor(Color.BLACK)
                autocomp_more_cats.setBackgroundResource(R.drawable.design_autocomplete)
                autocomp_more_cats.clearFocus()
                HideKeyboard()
            }
        }
    }

    fun HideKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(currentFocus == null) return
        if(currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        txt_name_tootlbar.setText("Edit project")
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun SetCategories(){
        rclv_cats.setHasFixedSize(true)
        rclv_cats.layoutManager = GridLayoutManager(this, 3)
        val arrayAdapter = EditProjectCategoriesAdapter()
        Log.e("cat1", categoriesArray.toString())
        Log.e("cat2", categoriesArray2.toString())
        arrayAdapter.EditProjectCategoriesAdapter(categoriesArray, categoriesArray2, this, categoria)
        rclv_cats.adapter = arrayAdapter
    }

    fun ConfigAutocomplete(categories: ArrayList<ResponseProyectosCategoriasOrder>,
    categories2: ArrayList<ResponseProyectosGeneralCategoriesOrder>){

        if(categories2.isEmpty()){
            categories.forEach{
                adaptador_dropdownList!!.add(it.categoria)
            }
        }
        else{
            categories2.forEach{
                adaptador_dropdownList!!.add(it.categoria)
            }
        }

        autocomp_more_cats.setThreshold(1)
        autocomp_more_cats.setAdapter(adaptador_dropdownList)

        if(categoriaActivada == 0){
            autocomp_more_cats.setText(categoria)
            addedCategoryBtn = 1
            Autocomplete(true)
        }
    }

    fun CheckPermissionGallery(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){

                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissions(permissions, PERMISSION_CODE)

            }else pickImageFromGallery()
        }else pickImageFromGallery()
    }

    private fun pickImageFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            val image: Uri = data!!.data!!
            val typeImage = contentResolver.getType(image) //exmple:   image/png

            Glide.with(this)
                .asBitmap()
                .load(image)
                .into(object : CustomTarget<Bitmap>(1500, 1000){
                    override fun onLoadCleared(placeholder: Drawable?){}
                    override fun onResourceReady(bitmap: Bitmap,
                                                 transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?){

                        newImage = 1
                        img_project.setImageBitmap(bitmap)
                        ConvertImageBase64(bitmap, typeImage!!)
                    }
                })
        }
    }

    fun ConvertImageBase64(bitmap: Bitmap, extension: String){
        val baoss = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baoss)
        val data = baoss.toByteArray()
        val codeimg = Base64.encodeToString(data, Base64.DEFAULT)
        encodedImage = "data:${extension};base64,${codeimg}"
    }

    override fun ErrorEdit(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun SuccessEdit(response: ResponseModificarProyecto){
        responseEditado = response.response
        controlAlert!!.StopProgress()
        controlAlert!!.AlertSuccessEditProject(response.msj!!, this)
    }


    override fun SuccessVotacion(resp: ResponseVotarProyecto) {
        TODO("Not yet implemented")
    }
    override fun ErrorVotacion(resp: String) {
        TODO("Not yet implemented")
    }
}