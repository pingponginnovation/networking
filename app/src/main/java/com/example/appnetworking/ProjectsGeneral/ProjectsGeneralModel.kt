package com.example.appnetworking.ProjectsGeneral

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProyectos
import com.example.appnetworking.ResponseProyectosGeneral

class ProjectsGeneralModel(viewPresenter: ProjectsGeneral.Presenter, ctx: Context): ProjectsGeneral.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetProyectos(req: RequestProyectosGeneral){
        val retrofit = RetrofitController(context!!)
        retrofit.GetAllProjects(this, req)
    }

    override fun ErrorProyectos(resp: String){
        presenter.ErrorProyectos(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectosGeneral){
        presenter.SuccessProyectos(response)
    }
}