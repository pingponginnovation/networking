package com.example.appnetworking.RedireccionAgendaInvitacion

import android.content.Context
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseInfoGenius

class AgendaInvitacionPresenter(viewMatch: AgendaInvitacion.View, ctx: Context): AgendaInvitacion.Presenter{

    private val view = viewMatch
    private val model = AgendaInvitacionModel(this, ctx)

    //MODEL
    override fun GetInfoCita(request: RequestDatosCita){
        model.GetInfoCita(request)
    }

    //VIEW
    override fun ErrorCita(resp: String){
        view.ErrorCita(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        view.SuccessInfo(response)
    }
}