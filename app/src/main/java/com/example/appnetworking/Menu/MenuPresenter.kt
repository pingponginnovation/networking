package com.example.appnetworking.Menu

import android.content.Context
import com.example.appnetworking.ModelsRetrofit.ResponseMenuOrder

class MenuPresenter(viewMatch: MenuInterface.View, ctx: Context): MenuInterface.Presenter{

    private val view = viewMatch
    private val model = MenuModel(this, ctx)

    //MODEL
    override fun GetMenu(){
        model.GetMenu()
    }
    //VIEW
    override fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String){
        view.SuccessMenu(resp, photo)
    }
}