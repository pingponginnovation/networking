//package com.example.appnetworking
package com.example.appgenesys

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseUpdateNotification(

    @SerializedName("success")
    val ok: String? = null,

    @SerializedName("notification_id")
    val idNotificationUpdated: Int? = null,

    @SerializedName("total_notificaciones")
    val total_notifs: Int? = null
)