package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseCrearInteres(
    @field:SerializedName("mensaje")
    val msj: String? = null,

    @field:SerializedName("interes_id")
    val id_interes: Int? = null,

    @field:SerializedName("interes")
    val interes: String? = null
)