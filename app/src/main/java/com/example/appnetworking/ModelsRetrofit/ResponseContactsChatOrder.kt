package com.example.appnetworking.ModelsRetrofit
import com.google.gson.annotations.SerializedName

data class ResponseContactsChatOrder(

    @field:SerializedName("perfil_id")
    val perfil_id_sender: Int? = null,

    @field:SerializedName("contador")
    val total_msj_unread: Int? = null,

    @field:SerializedName("mensaje")
    val last_msj: String? = null,

    @field:SerializedName("fecha")
    val date: String? = null,

    @field:SerializedName("hora")
    val hora: String? = null
)