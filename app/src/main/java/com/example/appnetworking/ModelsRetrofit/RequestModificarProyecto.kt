package com.example.appnetworking

data class RequestModificarProyecto(
    val proyecto_id: Int,
    val perfil_id: Int,
    val titulo: String,
    val categoria_flag: String,
    val categoria: String,
    val categoria_id: String,
    val descripcion: String,
    val contenido_video: String,
    val flag_change_img: String,
    val contenido_img: String,
    val activo: Int
)