package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseMenuOrder(

    @field:SerializedName("menu_id")
    val id: Int? = null,

    @field:SerializedName("menu_item")
    val name: String? = null,

    @field:SerializedName("menu_activo")
    val state: Int? = null,

    @field:SerializedName("total_notificaciones")
    val total_notificaciones: Int? = null
)