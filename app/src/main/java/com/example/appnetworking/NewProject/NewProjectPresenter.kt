package com.example.appnetworking.NewProject

import android.content.Context
import com.example.appgenesys.RequestCrearProyecto
import com.example.appgenesys.ResponseCrearProyecto
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProyectosGeneral

class NewProjectPresenter(viewMatch: NewProject.View, ctx: Context): NewProject.Presenter{

    private val view = viewMatch
    private val model = NewProjectModel(this, ctx)

    //MODEL
    override fun CrearProyecto(req: RequestCrearProyecto){
        model.CrearProyecto(req)
    }
    //VIEW
    override fun ErrorProyecto(resp: String){
        view.ErrorProyecto(resp)
    }

    override fun SuccessProyecto(response: ResponseCrearProyecto){
        view.SuccessProyecto(response)
    }
}