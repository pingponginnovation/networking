package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseAgendaAsistentesOrder(

    @field:SerializedName("perfil_id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("apellidos")
    val apellidos: String? = null
)