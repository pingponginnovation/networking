//package com.example.appnetworking
package com.example.appgenesys

import com.example.appnetworking.ModelsRetrofit.ResponseAgendaContactosOrder
import com.example.appnetworking.ModelsRetrofit.ResponseAgendaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileOrder
import com.google.gson.annotations.SerializedName

data class ResponseAgenda(

    @SerializedName("data")
    val data: ArrayList<ResponseAgendaOrder>,

    @SerializedName("contactos")
    val contacts: ArrayList<ResponseAgendaContactosOrder>
)