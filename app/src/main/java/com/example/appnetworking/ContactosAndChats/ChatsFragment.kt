package com.example.appnetworking.ContactosAndChats

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.Adapters.ChatsAdapter
import com.example.appnetworking.DecorationRecyclers.SimpleDividerItemDecoration
import com.example.appnetworking.FCMnotifications.Config
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.fragment_chats.*
import org.jetbrains.anko.toast

class ChatsFragment: Fragment(), Contactos.View{

    data class Chats(val id_his_profile: Int, val name: String, val apellidos: String, val puesto: String,
                     val img: String,
                     val chatID: Int,
                     val msj_unread_total: Int, val lastMSJ: String, val hora: String, val ID_sender: Int)
    private var myBroadcastReceiver: BroadcastReceiver? = null
    private lateinit var presenter: Contactos.Presenter
    private lateinit var preferences: PreferencesController
    private var adapterChats: ChatsAdapter? = null
    private var arrayChats: ArrayList<Chats> = ArrayList()
    private var aux = "nopause"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{

        RegisterBroadCast()
        preferences = PreferencesController(context!!)
        presenter = ContactosPresenter(this, context!!, preferences)
        GetContacts()
        return inflater.inflate(R.layout.fragment_chats, container, false)
    }

    private fun RegisterBroadCast(){
        myBroadcastReceiver = object : BroadcastReceiver(){
            override fun onReceive(context: Context, intent: Intent){
                when (intent.action){
                    Config.PUSH_NOTIFICATION ->{
                        handlePushNotification(intent)
                    }
                }
            }
        }
    }

    private fun handlePushNotification(intent: Intent){
        val message = intent.getStringExtra("message")
        val chatId = intent.getIntExtra("chat_ID", -1)
        updateRow(chatId, message!!)
    }

    private fun updateRow(chatRoomId: Int, message: String){
        var indx = -1
        var chat: Chats? = null
        arrayChats.forEach{
            if(it.chatID == chatRoomId){
                indx = arrayChats.indexOf(it)
                chat = Chats(it.id_his_profile, it.name, it.apellidos, it.puesto, it.img, it.chatID,
                    it.msj_unread_total+1,
                message, it.hora, it.ID_sender)
            }
        }

        if(indx != -1){
            arrayChats.removeAt(indx)
            arrayChats.add(indx, chat!!)
            adapterChats!!.notifyDataSetChanged()
        }
    }

    fun GetContacts(){
        presenter.GetContacts()
    }

    override fun ErrorContacts(resp: String){
        context!!.toast(resp)
    }

    override fun SuccessContacts(response: ResponseContacts){
        SetData(response)
    }

    fun SetData(datos: ResponseContacts){

        var aux = 0
        datos.contacts.forEach{
            if(it.chat_pendiente != null){
                aux = 1
                val chat = Chats(it.id!!, it.nombre!!, it.apellidos!!, it.puesto!!, it.img!!, it.chat_id!!,
                    it.chat_pendiente.total_msj_unread!!, it.chat_pendiente.last_msj!!,
                    it.chat_pendiente.hora!!,
                it.chat_pendiente.perfil_id_sender!!)
                arrayChats.add(chat)
            }
        }

        if(aux == 0){
            LY_LOADER_CHATS.visibility = View.VISIBLE
            LY_CHATS.visibility = View.GONE
        }
        else{
            rclv_chats.setHasFixedSize(true)
            rclv_chats.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            //rclv_chats.addItemDecoration(SimpleDividerItemDecoration(context!!))
            adapterChats = ChatsAdapter()
            adapterChats!!.ChatsAdapter(arrayChats, context!!, preferences.getMyIDprofile()!!.toInt())
            rclv_chats.adapter = adapterChats
        }
    }

    override fun onResume(){
        super.onResume()
        if(aux.equals("pause")){
            GetContacts()
            aux = "nopause"
        }
        LocalBroadcastManager.getInstance(context!!)
            .registerReceiver(myBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))
    }

    override fun onPause(){
        super.onPause()
        arrayChats.clear()
        aux = "pause"
        LocalBroadcastManager.getInstance(context!!).unregisterReceiver(myBroadcastReceiver!!)
    }
}