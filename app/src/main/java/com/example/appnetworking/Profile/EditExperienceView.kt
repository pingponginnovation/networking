package com.example.appnetworking.Profile

import android.app.AlertDialog
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.appgenesys.ResponseDeleteInteres
import com.example.appgenesys.ResponseDeleteSkill
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.Profile.ProfileView.Companion.responseJornadas
import com.example.appnetworking.Profile.SearchCompanyName.Companion.aux_catalogo
import com.example.appnetworking.Profile.SearchCompanyName.Companion.name_company
import com.example.appnetworking.R
import com.example.appnetworking.RequestModificarExperiencia
import kotlinx.android.synthetic.main.dialog_pickdate.view.*
import kotlinx.android.synthetic.main.edit_experience_profile.*
import kotlinx.android.synthetic.main.show_experiences_toedit_profile.back_button
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class EditExperienceView: AppCompatActivity(), Profile.View{

    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var preferences: PreferencesController
    private lateinit var presenter: Profile.Presenter
    private var jornadaType = ""
    var dateSelected: String? = null
    private var endDate = ""
    private var startDate = ""
    private var auxiliar = -1
    private var actualmente = 0

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_experience_profile)

        presenter = ProfilePresenter(this, applicationContext)
        controlAlert = ControllerAlertDialog(this)
        preferences = PreferencesController(this)
        ConfigToolbar()
        SetData()

        btn_save_experience.setOnClickListener{
            if(edt_company.text.toString().isEmpty() || edt_job.text.toString().isEmpty() ||
                    edt_startdate.text.toString().isEmpty() || edt_enddate.text.toString().isEmpty()
                || edt_address.text.toString().isEmpty() || edt_descripciont.text.toString().isEmpty())
                toast("Please fill in all fields")
            else{

                var id_jornada = -1
                responseJornadas.forEach{
                    if(it.tipo_jornada.equals(jornadaType))id_jornada = it.id!!
                }

                var flag_empresa = -1
                var company_name = ""
                var empresa_id = -1

                if(aux_catalogo == -1){
                    empresa_id = BuscaIDcatalogo(edt_company.text.toString())
                    flag_empresa = 1
                    company_name = edt_company.text.toString()
                }
                else if(aux_catalogo == 1){
                    empresa_id = BuscaIDcatalogo(name_company)
                    flag_empresa = 1
                    company_name = name_company
                }
                else{
                    empresa_id = 0
                    flag_empresa = 0
                    company_name = name_company
                }

                val req = RequestModificarExperiencia(preferences.getMyIDprofile()!!.toInt(),
                intent.getIntExtra("EXPERIENCIA_ID", -1), edt_job.text.toString(),
                id_jornada, flag_empresa.toString(), empresa_id, company_name,
                edt_address.text.toString(), actualmente.toString(), startDate, endDate,
                edt_descripciont.text.toString())

                controlAlert!!.ProgressBar()
                presenter.EditExperience(req)
            }
        }

        edt_company.setOnClickListener{
            startActivity<SearchCompanyName>()
        }

        edt_startdate.setOnClickListener{
            auxiliar = 1
            ShowCalendar()
        }

        edt_enddate.setOnClickListener{
            auxiliar = 0
            ShowCalendar()
        }

        checkbx_actual_cargo.setOnClickListener{
            if(checkbx_actual_cargo.isChecked){
                endDate = GetActualFecha()
                actualmente = 1
                edt_enddate.setText("Actual")
                edt_enddate.isEnabled = false
            }
            else{
                actualmente = 0
                if(!endDate.isEmpty()){
                    val sdf = SimpleDateFormat("yyyy-MM-dd") //OUTPUT: 2020-05-21
                    val dateParse: Date = sdf.parse(endDate)!!
                    val dateLetras = ConvierteALetras(dateParse)
                    edt_enddate.setText(dateLetras)
                }
                else edt_enddate.setText("")
                edt_enddate.isEnabled = true
            }
        }
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        txt_titletootlbar.text = "Edit experience"
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun GetActualFecha(): String{
        val currentDate = Date() //Fecha actual del celular y hora
        val hdf = SimpleDateFormat("yyyy-MM-dd")
        val date = hdf.format(currentDate)
        return date
    }

    fun BuscaIDcatalogo(empresa: String): Int{
        var id = 0
        ProfileView.responseCatalogoEmpresas.forEach{
            if(it.empresa.equals(empresa)) id = it.id!!
        }
        return id
    }

    fun SetData(){
        if(!responseJornadas.isNullOrEmpty()){
            val options = arrayListOf<String>()
            responseJornadas.forEach{
                options.add(it.tipo_jornada!!)
            }
            val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, options)
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            listview_jornadas.adapter = dataAdapter

            listview_jornadas.setOnItemSelectedListener(object: AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){
                    jornadaType = listview_jornadas.getItemAtPosition(position).toString()
                }
                override fun onNothingSelected(parent: AdapterView<*>?){}
            })
        }

        edt_company.setText(intent.getStringExtra("COMPANY"))
        edt_job.setText(intent.getStringExtra("JOB"))

        startDate = intent.getStringExtra("DATE_INICIO")!!
        val parsed = ParseFromStringToDate(startDate)
        val startDateLetras = ConvierteALetras(parsed)
        edt_startdate.setText(startDateLetras)

        endDate = intent.getStringExtra("DATE_END")!!
        val parsed2 = ParseFromStringToDate(endDate)
        val endDateLetras = ConvierteALetras(parsed2)
        edt_enddate.setText(endDateLetras)

        edt_address.setText(intent.getStringExtra("DIRECCION"))
        edt_descripciont.setText(intent.getStringExtra("DESCRIPTION"))
    }

    private fun ParseFromStringToDate(date: String): Date{
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val dateParse: Date = sdf.parse(date)!!
        return dateParse
    }

    private fun ConvierteALetras(date: Date): String{
        val sdf2 = SimpleDateFormat("dd-MMMM-yyyy") //OUTPUT: 05 December 2018
        return sdf2.format(date)
    }

    fun ShowCalendar(){

        val sdf = SimpleDateFormat("yyyy-MM-dd") //OUTPUT: 2020-05-21

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog: AlertDialog? = null
        val mView: View = layoutInflater.inflate(R.layout.dialog_pickdate, null)
        dateSelected = sdf.format(Date(mView.calendar_Ev.date))

        mView.calendar_Ev.setOnDateChangeListener{view, year, month, dayOfMonth ->
            dateSelected = sdf.format(Date(year - 1900, month, dayOfMonth))
        }

        mView.btn_save_date_Ev.setOnClickListener{

            if(auxiliar == 1){

                if(!endDate.isEmpty()){

                    if(dateSelected.toString() >= endDate) toast("Date must be lower")
                    else{
                        startDate = dateSelected!!
                        val dateParse: Date = sdf.parse(dateSelected!!)!!
                        val dateLetras = ConvierteALetras(dateParse)
                        edt_startdate.setText(dateLetras)
                        dialog!!.dismiss()
                    }
                }
                else{
                    startDate = dateSelected!!
                    val dateParse: Date = sdf.parse(dateSelected!!)!!
                    val dateLetras = ConvierteALetras(dateParse)
                    edt_startdate.setText(dateLetras)
                    dialog!!.dismiss()
                }
            }
            else{
                if(auxiliar == 0){

                    if(dateSelected!! > startDate){

                        endDate = dateSelected!!
                        val dateParse: Date = sdf.parse(dateSelected!!)!!
                        val dateLetras = ConvierteALetras(dateParse)
                        edt_enddate.setText(dateLetras)
                        dialog!!.dismiss()
                    }
                    else toast("Date must be greater")
                }
            }
        }
        mBuilder.setView(mView)
        dialog = mBuilder.create()
        dialog.show()
    }

    override fun SucccessEdit(resp: ResponseEditExperiencia){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp.msj!!)
    }

    override fun ErrorEdit(msj: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(msj)
    }

    override fun ErrorProfile(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessProfile(response: ResponseProfile) {
        TODO("Not yet implemented")
    }
    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorCrearExperiencia(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessEditBiography(response: ResponseEditBiography) {
        TODO("Not yet implemented")
    }
    override fun ErrorEditBiography(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessInterest(response: ResponseCrearInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessSkill(response: ResponseCrearSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorSkill(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteSkill(resp: String) {
        TODO("Not yet implemented")
    }
}