package com.example.appnetworking.Notifications

import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.ResponseNotificaciones

interface Notifications{

    interface View{
        fun ErrorNotifications(resp: String)
        fun SuccessNotifications(response: ResponseNotificaciones)
    }

    interface Presenter{
        fun GetNotifications()
        fun ErrorNotifications(resp: String)
        fun SuccessNotifications(response: ResponseNotificaciones)
    }

    interface Model{
        fun GetNotifications()
        fun ErrorNotifications(resp: String)
        fun SuccessNotifications(response: ResponseNotificaciones)
    }
}