package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseInfoGeniusOrder(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("logo")
    val logo: String? = null,

    @field:SerializedName("correo")
    val email: String? = null,

    @field:SerializedName("celular")
    val cel: String? = null,

    @field:SerializedName("pagina_web")
    val web: String? = null,

    @field:SerializedName("sinopsis")
    val sinopsis: String? = null
)