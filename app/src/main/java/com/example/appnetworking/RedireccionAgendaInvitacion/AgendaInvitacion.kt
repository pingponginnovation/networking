package com.example.appnetworking.RedireccionAgendaInvitacion

import com.example.appgenesys.ResponseAgenda
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseInfoGenius

interface AgendaInvitacion{

    interface View{
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)
    }

    interface Presenter{
        fun GetInfoCita(request: RequestDatosCita)
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)
    }

    interface Model{
        fun GetInfoCita(request: RequestDatosCita)
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)
    }
}