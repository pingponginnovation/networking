package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.AgendaEvento.AgendaEventoView
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.asistentes
import com.example.appnetworking.ContactoChat.ContactoChatView
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.ResponseActividadesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseAgendaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseContactsOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.Profile.PerfilContactoView.Companion.idperfil
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import org.jetbrains.anko.toast
import java.util.ArrayList

class ContactosAdapter: RecyclerView.Adapter<ContactosAdapter.ViewHolder>(), Filterable{

    var list: List<ResponseContactsOrder>? = null
    private var listFiltered: List<ResponseContactsOrder>? = null
    lateinit var contexto: Context

    fun ContactosAdapter(lista: List<ResponseContactsOrder>, context: Context){
        list = lista
        listFiltered = lista
        contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val name = holder.itemView.findViewById(R.id.txt_name_c) as TextView
        val job = holder.itemView.findViewById(R.id.txt_job_c) as TextView
        val foto = holder.itemView.findViewById(R.id.my_img) as ImageView
        val chat = holder.itemView.findViewById(R.id.btn_chat) as ImageButton

        name.text = listFiltered!![position].nombre + " " + listFiltered!![position].apellidos
        job.text = listFiltered!![position].puesto
        Picasso.get().load(listFiltered!![position].img).into(foto)

        chat.setOnClickListener{
            val intent = Intent(contexto, ContactoChatView::class.java)
            intent.putExtra("ID_HIS_PROFILE", listFiltered!![position].id)
            intent.putExtra("NAME", listFiltered!![position].nombre)
            intent.putExtra("IMG", listFiltered!![position].img)
            intent.putExtra("PUESTO", listFiltered!![position].puesto)
            intent.putExtra("CHAT_ID", listFiltered!![position].chat_id)
            contexto.startActivity(intent)
        }

        holder.itemView.setOnClickListener{view ->
            val intent = Intent(contexto, PerfilContactoView::class.java)
            intent.putExtra("ID_PERFIL", listFiltered!![position].id)
            intent.putExtra("FROM", "MY_CONTACT")
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_contactos, parent, false))
    }

    override fun getItemCount(): Int{
        return listFiltered!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {

                val charString = charSequence.toString()

                if (charString.isEmpty()) listFiltered = list
                else{

                    val filteredList = ArrayList<ResponseContactsOrder>()
                    for (row in list!!) {

                        if (row.nombre.toString().toLowerCase().contains(charString.toLowerCase()) || row.apellidos.toString().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row)
                    }

                    listFiltered = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                listFiltered = filterResults.values as ArrayList<ResponseContactsOrder>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}