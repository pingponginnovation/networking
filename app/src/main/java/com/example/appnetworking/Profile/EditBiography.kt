package com.example.appnetworking.Profile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.appgenesys.ResponseDeleteInteres
import com.example.appgenesys.ResponseDeleteSkill
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import com.example.appnetworking.RequestEditBiography
import kotlinx.android.synthetic.main.edit_biography.*

class EditBiography: AppCompatActivity(), Profile.View{

    private lateinit var preferences: PreferencesController
    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: Profile.Presenter

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_biography)

        presenter = ProfilePresenter(this, applicationContext)
        controlAlert = ControllerAlertDialog(this)
        ConfigToolbar()
        preferences = PreferencesController(this)
        GetExtras()

        btn_save_bio.setOnClickListener{
            val req = RequestEditBiography(preferences.getMyIDprofile()!!.toInt(),
            edt_biography.text.toString())

            controlAlert!!.ProgressBar()
            presenter.EditBiography(req)
        }
    }

    fun GetExtras(){
        edt_biography.setText(intent.getStringExtra("BIOGRAPHY"))
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    override fun SuccessEditBiography(response: ResponseEditBiography){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(response.msj!!)
    }

    override fun ErrorEditBiography(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }


    override fun ErrorProfile(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessProfile(response: ResponseProfile) {
        TODO("Not yet implemented")
    }
    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorCrearExperiencia(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SucccessEdit(resp: ResponseEditExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorEdit(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessInterest(response: ResponseCrearInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessSkill(response: ResponseCrearSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorSkill(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteSkill(resp: String) {
        TODO("Not yet implemented")
    }
}