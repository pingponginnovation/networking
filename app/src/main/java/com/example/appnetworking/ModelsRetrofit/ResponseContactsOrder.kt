package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.example.appgenesys.ResponseContacts
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseContactsOrder(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val nombre: String? = null,

    @field:SerializedName("apellidos")
    val apellidos: String? = null,

    @field:SerializedName("img_perfil")
    val img: String? = null,

    @field:SerializedName("puesto")
    val puesto: String? = null,

    @field:SerializedName("canal_id")
    val chat_id: Int? = null,

    @field:SerializedName("chat_pendiente")
    val chat_pendiente: ResponseContactsChatOrder? = null
)