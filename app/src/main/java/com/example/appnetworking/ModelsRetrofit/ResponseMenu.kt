package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMenuOrder
import com.example.appnetworking.ModelsRetrofit.ResponsePerfilContactoOrder
import com.google.gson.annotations.SerializedName

data class ResponseMenu(
    @SerializedName("data")
    val data: ArrayList<ResponseMenuOrder>,

    @field:SerializedName("imagen_perfil")
    val photo: String? = null
)