package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseProfilePonenteExperienciaOrder(

    @field:SerializedName("experiencia_id")
    val id: Int? = null,

    @field:SerializedName("area_experiencia")
    val name_experiencia: String? = null
)