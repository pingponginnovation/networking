package com.example.appnetworking.Profile

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appgenesys.RequestCrearInteres
import com.example.appgenesys.RequestDeleteInteres
import com.example.appgenesys.ResponseDeleteInteres
import com.example.appgenesys.ResponseDeleteSkill
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.Adapters.InterestsToEditAdapter
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.Models.InteresesForEdit
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.edit_biography.back_button
import kotlinx.android.synthetic.main.edit_intereses.*
import org.jetbrains.anko.toast

class EditIntereses: AppCompatActivity(), Profile.View{

    private lateinit var preferences: PreferencesController
    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: Profile.Presenter
    private var arrayIntereses: ArrayList<InteresesForEdit> = arrayListOf()
    private var arrayInteresesString: ArrayList<String> = ArrayList()
    private var name_deleted = ""
    private var arrayAdapter = InterestsToEditAdapter()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_intereses)

        presenter = ProfilePresenter(this, applicationContext)
        controlAlert = ControllerAlertDialog(this)
        ConfigToolbar()
        preferences = PreferencesController(this)
        GetExtras()

        btn_add_interest.setOnClickListener{
            if(autocomp_add_interest.text.toString().isEmpty()) toast("Field can't be empty")
            else{
                val exist = CheckExistSkill(autocomp_add_interest.text.toString())
                if(exist) toast("That interest already exist")
                else{
                    controlAlert!!.ProgressBar()
                    presenter.CreateInteres(RequestCrearInteres(preferences.getMyIDprofile()!!.toInt(),
                        autocomp_add_interest.text.toString()))
                }
            }
        }
    }

    fun GetExtras(){
        val serializable = intent.getSerializableExtra("INTERESES")
        arrayIntereses = serializable as ArrayList<InteresesForEdit>
        arrayIntereses.forEach{
            arrayInteresesString.add(it.name)
        }

        rclv_edit_intereses.setHasFixedSize(true)
        rclv_edit_intereses.layoutManager = GridLayoutManager(this, 4)
        arrayAdapter = InterestsToEditAdapter()
        arrayAdapter.InterestsToEditAdapter(arrayInteresesString, arrayIntereses, this)
        rclv_edit_intereses.adapter = arrayAdapter
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    private fun CheckExistSkill(skill: String): Boolean{
        arrayInteresesString.forEach{
            if(it.equals(skill, true)) return true
        }
        return false
    }

    private fun UpdateRecycler(interes: String, id_interes: Int){
        arrayInteresesString.add(interes)
        arrayIntereses.add(InteresesForEdit(interes, id_interes))
        arrayAdapter.notifyDataSetChanged()
    }

    fun DeleteInteres(id_interes: Int, name: String){
        name_deleted = name
        controlAlert!!.ProgressBar()
        presenter.DeleteInteres(RequestDeleteInteres(preferences.getMyIDprofile()!!.toInt(), id_interes))
    }

    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp.msj)

        var index = -1
        arrayInteresesString.forEach{
            if(it == name_deleted) index = arrayInteresesString.indexOf(it)
        }

        if(index != -1){
            arrayInteresesString.removeAt(index)
            arrayIntereses.removeAt(index)
            arrayAdapter.notifyDataSetChanged()
        }
    }

    override fun ErrorDeleteInteres(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun onPause(){
        super.onPause()
        arrayInteresesString.clear()
    }

    override fun SuccessInterest(response: ResponseCrearInteres){
        UpdateRecycler(autocomp_add_interest.text.toString(), response.id_interes!!)
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(response.msj!!)
    }

    override fun ErrorInteres(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }


    override fun ErrorProfile(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessProfile(response: ResponseProfile) {
        TODO("Not yet implemented")
    }
    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorCrearExperiencia(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SucccessEdit(resp: ResponseEditExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorEdit(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessEditBiography(response: ResponseEditBiography){
    }
    override fun ErrorEditBiography(resp: String){
    }
    override fun SuccessSkill(response: ResponseCrearSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorSkill(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteSkill(resp: String) {
        TODO("Not yet implemented")
    }
}