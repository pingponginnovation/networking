package com.example.appnetworking.Agenda

import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.ResponseDateTimeOrder
import com.example.appnetworking.RequestNewEvent
import java.lang.StringBuilder

interface Agenda{

    interface View{
        fun ErrorAgenda(resp: String)
        fun SuccessAgenda(response: ResponseAgenda)
        fun SuccessContacts(response: ResponseContacts)
        fun ErrorContacts(msj: String)
        fun SuccessEventCreated()
        fun SuccessDateTime(response: ResponseDateTimeOrder)
        fun ErrorDateTime(resp: String)
    }

    interface Presenter{
        fun GetMyAgenda(date: String)
        fun ErrorAgenda(resp: String)
        fun SuccessAgenda(response: ResponseAgenda)
        fun GetContactos()
        fun SuccessContacts(response: ResponseContacts)
        fun ErrorContacts(msj: String)
        fun CreateEvent(request: RequestNewEvent)
        fun SuccessEventCreated()
        fun GetDateTime()
        fun SuccessDateTime(response: ResponseDateTimeOrder)
        fun ErrorDateTime(resp: String)
    }

    interface Model{
        fun GetMyAgenda(date: String)
        fun ErrorAgenda(resp: String)
        fun SuccessAgenda(response: ResponseAgenda)
        fun GetContactos()
        fun SuccessContacts(response: ResponseContacts)
        fun ErrorContacts(msj: String)
        fun CreateEvent(request: RequestNewEvent)
        fun SuccessEventCreated()
        fun GetDateTime()
        fun SuccessDateTime(response: ResponseDateTimeOrder)
        fun ErrorDateTime(resp: String)
    }
}