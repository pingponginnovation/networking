package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponsePersonasByNameOrder(

    @field:SerializedName("perfil_id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("apellidos")
    val apellidos: String? = null,

    @field:SerializedName("direccion")
    val direecion: String? = null,

    @field:SerializedName("puesto")
    val job: String? = null,

    @field:SerializedName("img_perfil")
    val img: String? = null
)