package com.example.appnetworking.Profile

import android.content.Context
import com.example.appnetworking.ResponsePerfilContacto
import com.example.appnetworking.ResponseProyectos

class PerfilContactoPresenter(viewMatch: PerfilContacto.View, ctx: Context): PerfilContacto.Presenter{

    private val view = viewMatch
    private val model = PerfilContactoModel(this, ctx)

    //MODEL
    override fun GetDataPerfil(id_perfil: Int){
        model.GetDataPerfil(id_perfil)
    }

    //VIEW

    override fun SuccessPerfil(resp: ResponsePerfilContacto){
        view.SuccessPerfil(resp)
    }


}