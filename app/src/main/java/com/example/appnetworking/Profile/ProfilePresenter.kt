package com.example.appnetworking.Profile

import android.content.Context
import com.example.appgenesys.*
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.RequestCrearExperiencia
import com.example.appnetworking.RequestEditBiography
import com.example.appnetworking.RequestModificarExperiencia

class ProfilePresenter(viewMatch: Profile.View, ctx: Context): Profile.Presenter{

    private val view = viewMatch
    private val model = ProfileModel(this, ctx)

    //MODEL
    override fun GetProfile(){
        model.GetProfile()
    }

    override fun CrearExperiencia(req: RequestCrearExperiencia){
        model.CrearExperiencia(req)
    }

    override fun EditExperience(req: RequestModificarExperiencia){
        model.EditExperience(req)
    }

    override fun EditBiography(req: RequestEditBiography){
        model.EditBiography(req)
    }

    override fun DeleteInteres(req: RequestDeleteInteres){
        model.DeleteInteres(req)
    }

    override fun CreateSkill(req: RequestCrearSkill){
        model.CreateSkill(req)
    }

    override fun CreateInteres(req: RequestCrearInteres){
        model.CreateInteres(req)
    }

    override fun DeleteSkill(req: RequestDeleteSkill){
        model.DeleteSkill(req)
    }

    //VIEW
    override fun ErrorProfile(resp: String){
        view.ErrorProfile(resp)
    }

    override fun SuccessProfile(response: ResponseProfile){
        view.SuccessProfile(response)
    }

    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia){
        view.SuccessCrearExperiencia(response)
    }

    override fun ErrorCrearExperiencia(msj: String){
        view.ErrorCrearExperiencia(msj)
    }

    override fun SucccessEdit(resp: ResponseEditExperiencia){
        view.SucccessEdit(resp)
    }

    override fun ErrorEdit(msj: String){
        view.ErrorEdit(msj)
    }

    override fun SuccessEditBiography(response: ResponseEditBiography){
        view.SuccessEditBiography(response)
    }

    override fun ErrorEditBiography(resp: String){
        view.ErrorEditBiography(resp)
    }

    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres){
        view.SuccessDeleteInteres(resp)
    }

    override fun ErrorDeleteInteres(resp: String){
        view.ErrorDeleteInteres(resp)
    }

    override fun SuccessInterest(response: ResponseCrearInteres){
        view.SuccessInterest((response))
    }

    override fun ErrorInteres(resp: String){
        view.ErrorInteres(resp)
    }

    override fun SuccessSkill(response: ResponseCrearSkill){
        view.SuccessSkill(response)
    }

    override fun ErrorSkill(resp: String){
        view.ErrorSkill(resp)
    }

    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill){
        view.SuccessDeleteSkill(resp)
    }

    override fun ErrorDeleteSkill(resp: String){
        view.ErrorDeleteSkill(resp)
    }
}