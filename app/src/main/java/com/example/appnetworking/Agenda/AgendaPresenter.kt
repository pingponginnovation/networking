package com.example.appnetworking.Agenda

import android.content.Context
import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.ResponseDateTimeOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestNewEvent

class AgendaPresenter(viewMatch: Agenda.View, ctx: Context, prefs: PreferencesController): Agenda.Presenter{

    private val view = viewMatch
    private val model = AgendaModel(this, ctx, prefs)

    //MODEL
    override fun GetMyAgenda(date: String){
        model.GetMyAgenda(date)
    }

    override fun GetContactos(){
        model.GetContactos()
    }

    override fun CreateEvent(request: RequestNewEvent){
        model.CreateEvent(request)
    }

    override fun GetDateTime(){
        model.GetDateTime()
    }

    //VIEW
    override fun ErrorAgenda(resp: String){
        view.ErrorAgenda(resp)
    }

    override fun SuccessAgenda(response: ResponseAgenda){
        view.SuccessAgenda(response)
    }

    override fun SuccessContacts(response: ResponseContacts){
        view.SuccessContacts(response)
    }

    override fun ErrorContacts(msj: String){
        view.ErrorContacts(msj)
    }

    override fun SuccessEventCreated() {
        view.SuccessEventCreated()
    }

    override fun SuccessDateTime(response: ResponseDateTimeOrder){
        view.SuccessDateTime(response)
    }

    override fun ErrorDateTime(resp: String){
        view.ErrorDateTime(resp)
    }
}