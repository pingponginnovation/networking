package com.example.appnetworking.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Models.InteresesForEdit
import com.example.appnetworking.Models.SkillsForEdit
import com.example.appnetworking.Profile.EditIntereses
import com.example.appnetworking.Profile.EditSkills
import com.example.appnetworking.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class SkillsToEditAdapter: RecyclerView.Adapter<SkillsToEditAdapter.ViewHolder>(){

    private var list: ArrayList<SkillsForEdit>? = null
    private var listSkill: ArrayList<String>? = null
    lateinit var contexto: EditSkills

    fun SkillsToEditAdapter(listSkill: ArrayList<String>, lista: ArrayList<SkillsForEdit>,
                               context: EditSkills){
        list = lista
        this.contexto = context
        this.listSkill = listSkill
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val titulo = holder.itemView.findViewById(R.id.txt_skills_toeditt) as TextView
        val btn_close = holder.itemView.findViewById(R.id.delete_skill) as ImageButton
        titulo.text = listSkill!![position]

        holder.itemView.setOnClickListener{
            contexto.toast(listSkill!![position])
        }

        btn_close.setOnClickListener{
            contexto.alert("Are you sure?", "Warning"){
                positiveButton("Yes"){
                    contexto.DeleteSkill(list!![position].id, listSkill!![position])
                }
                negativeButton("Cancel"){}
            }.show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_skills_toedit, parent, false))
    }

    override fun getItemCount():Int{
        return listSkill!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}