package com.example.appnetworking.Agenda

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.ResponseDateTimeOrder
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.Adapters.AgendaAdapter
import com.example.appnetworking.Adapters.ChipsContactoEventoNuevo
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.misContactos
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import com.example.appnetworking.RequestNewEvent
import kotlinx.android.synthetic.main.dialog_new_cita.view.*
import kotlinx.android.synthetic.main.dialog_pickdate.view.*
import kotlinx.android.synthetic.main.my_agenda_activity.*
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*


class AgendaView: AppCompatActivity(), Agenda.View{

    private lateinit var presenter: Agenda.Presenter
    private lateinit var preferences: PreferencesController
    var dateSelected: String? = null
    private var respContacts: ResponseContacts? = null
    private var contactsAdded = arrayListOf<String>()
    private var idsContacts = arrayListOf<Int>()
    val arrayAdapter = ChipsContactoEventoNuevo()
    private var caseAlert = -1
    private var horaTimer1 = ""
    private var horatimer2 = ""
    private var actualHour = ""
    private lateinit var fechaActual: String
    private var state_checkbox = "normal"

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.my_agenda_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("My agenda")

        val currentDate = Date() //Fecha actual del celular
        val sdf = SimpleDateFormat("yyy-MM-dd")
        fechaActual = sdf.format(currentDate)

        preferences = PreferencesController(applicationContext)
        presenter = AgendaPresenter(this, applicationContext, preferences)

        GetContacts()
        GetDateTimeServer()

        calendar_id.setOnDateChangeListener{view, year, month, dayOfMonth ->
            dateSelected = sdf.format(Date(year - 1900, month, dayOfMonth))
            GoAgenda(dateSelected!!)
        }

        dateSelected = sdf.format(Date(calendar_id.date))
        GoAgenda(dateSelected!!)

        btn_schedule.setOnClickListener{

            if(dateSelected.toString() >= fechaActual){

                val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
                var dialog: AlertDialog? = null
                val mView: View = layoutInflater.inflate(R.layout.dialog_new_cita, null)

                mView.checkbox_noticas_generales.setOnClickListener{
                    if(mView.checkbox_noticas_generales.isChecked){
                        state_checkbox = "general"
                        mView.autocomplete_contacts.isEnabled = false
                        contactsAdded.clear()
                        idsContacts.clear()
                        arrayAdapter.notifyDataSetChanged()
                    }
                    else{
                        state_checkbox = "normal"
                        mView.autocomplete_contacts.isEnabled = true
                    }
                }

                mView.btn_close.setOnClickListener{
                    dialog!!.dismiss()
                    contactsAdded.clear()
                    arrayAdapter.notifyDataSetChanged()
                }

                if(dateSelected.toString() > fechaActual)SetUpSpinnerTimeGeneral(mView)
                else SetUpSpinnerTime(mView)

                SetUpSpinnerAlert(mView)
                SetUpSpinnerContacts(mView)
                SetFechaActual(mView.txt_date_nc, dateSelected!!)

                mView.txt_date_nc.setOnClickListener{
                    ShowCalendar(mView.txt_date_nc, mView)
                }

                /*mView.txt_time.setOnClickListener{
                    PickUpBeginTime(mView, "My event starts at:")
                }*/

                mView.btn_confirm_cita.setOnClickListener{
                    if(mView.txt_title_E.text.isNullOrEmpty() || mView.txt_descrip_E.text.isNullOrEmpty()
                        || mView.txt_location_E.text.isNullOrEmpty()
                        || mView.txt_descrip_E.text.isNullOrEmpty()) toast("You must complete all fields")
                    else{
                        if(state_checkbox.equals("normal")){
                            if(contactsAdded.isNullOrEmpty())toast("You must select a contact")
                            else{
                                LY_LOADER_MAKING_EVENT.visibility = View.VISIBLE
                                LY_AGENDA.visibility = View.GONE
                                //val strs = mView.txt_time.text.toString().trim().split("-").toTypedArray()

                                val newEvent = RequestNewEvent(
                                    mView.txt_title_E.text.toString(),
                                    idsContacts,
                                    mView.txt_date_nc.text.toString(),
                                    horaTimer1,
                                    horatimer2,
                                    mView.txt_descrip_E.text.toString(),
                                    mView.txt_location_E.text.toString(),
                                    caseAlert,
                                    state_checkbox
                                )

                                Log.e("req", newEvent.toString())
                                dateSelected = mView.txt_date_nc.text.toString() //For reload data events on SuccessEventCreated

                                presenter.CreateEvent(newEvent)
                                dialog!!.dismiss()
                                contactsAdded.clear() //Cleaning array contacts in case they create another event.
                                idsContacts.clear()
                                arrayAdapter.notifyDataSetChanged() //notifying adapter contacts
                            }
                        }
                        else{
                            LY_LOADER_MAKING_EVENT.visibility = View.VISIBLE
                            LY_AGENDA.visibility = View.GONE

                            val newEvent = RequestNewEvent(
                                mView.txt_title_E.text.toString(),
                                idsContacts,
                                mView.txt_date_nc.text.toString(),
                                horaTimer1,
                                horatimer2,
                                mView.txt_descrip_E.text.toString(),
                                mView.txt_location_E.text.toString(),
                                caseAlert,
                                state_checkbox
                            )

                            //Log.e("req", newEvent.toString())
                            dateSelected = mView.txt_date_nc.text.toString() //For reload data events on SuccessEventCreated

                            presenter.CreateEvent(newEvent)
                            dialog!!.dismiss()
                            contactsAdded.clear() //Cleaning array contacts in case they create another event.
                            idsContacts.clear()
                            arrayAdapter.notifyDataSetChanged() //notifying adapter contacts
                        }
                    }
                }

                mBuilder.setView(mView)
                dialog = mBuilder.create()
                dialog.show()
                dialog.setCancelable(false)
                dialog.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            else toast("Date must be equal or greater")
        }
    }

    fun SetFechaActual(date_txt: TextView, fecha: String){
        //val currentDate = Date() //Fecha actual del celular y hora
        //val sdf = SimpleDateFormat("yyy-MM-dd")
        //val fecha = sdf.format(currentDate)
        date_txt.text = fecha
    }

    fun ShowCalendar(date: TextView, view: View){

        val sdf = SimpleDateFormat("yyy-MM-dd")
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog: AlertDialog? = null
        val mView: View = layoutInflater.inflate(R.layout.dialog_pickdate, null)

        dateSelected = sdf.format(Date(mView.calendar_Ev.date))
        mView.calendar_Ev.setOnDateChangeListener{view, year, month, dayOfMonth ->
            dateSelected = sdf.format(Date(year - 1900, month, dayOfMonth))
        }

        mView.btn_save_date_Ev.setOnClickListener{

            if(dateSelected.toString() >= fechaActual){

                date.text = dateSelected
                dialog!!.dismiss()

                if(dateSelected.toString() > fechaActual) SetUpSpinnerTimeGeneral(view)
                else SetUpSpinnerTime(view)
            }
            else toast("Date must be equal or greater")
        }

        mBuilder.setView(mView)
        dialog = mBuilder.create()
        dialog.show()
    }

    /*fun ShowCalendar2(date: TextView, date1: TextView, view: View){

        val sdf = SimpleDateFormat("yyy-MM-dd")
        val currentDate = Date() //Fecha actual del celular
        val algo = sdf.format(currentDate)

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog: AlertDialog? = null
        val mView: View = layoutInflater.inflate(R.layout.dialog_pickdate, null)

        dateSelected = sdf.format(Date(mView.calendar_Ev.date))
        mView.calendar_Ev.setOnDateChangeListener{view, year, month, dayOfMonth ->
            dateSelected = sdf.format(Date(year - 1900, month, dayOfMonth))
        }

        mView.btn_save_date_Ev.setOnClickListener{

            if(dateSelected.toString() >= algo.toString()){
                if(dateSelected.toString() >= date1.text.toString()){
                    date.text = dateSelected
                    dialog!!.dismiss()
                    SetUpSpinnerTimeEnd(view, "0:00")
                }
                else toast("Date must be equal or greater")
            }
            else toast("Date must be equal or greater")
        }

        mBuilder.setView(mView)
        dialog = mBuilder.create()
        dialog.show()
    }*/

    fun SetUpSpinnerTimeGeneral(mview: View){
        val options = arrayListOf<String>()
        var done = false
        var hora = 0
        var minuto = 0
        var minutoS = ""

        do{
            val horaS = hora.toString()
            if(minuto == 0) minutoS = "00"
            else minutoS = minuto.toString()

            val hour = "${horaS}:${minutoS}"
            if(hour.equals("23:30")){
                options.add(hour)
                done = true
            }
            else{
                options.add(hour)
                if(minutoS.equals("30")){
                    hora += 1
                    minuto = 0
                }else minuto = 30
            }
        }while(done == false)

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mview.txt_time_spin.adapter = dataAdapter

        mview.txt_time_spin.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){
                horaTimer1 = mview.txt_time_spin.getItemAtPosition(position).toString()
                SetUpSpinnerTimeEnd(mview, horaTimer1)
                horaTimer1 = SplitHora(horaTimer1)
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        }
    }

    fun SetUpSpinnerTime(mView: View){

        val options = arrayListOf<String>()
        var done = false
        var minuto = 0
        var minutoS = ""
        val array = actualHour.trim().split(":").toTypedArray()
        var horaActual = array[0].toInt()
        val minuteActual = array[1].toInt()

        if(minuteActual in 30..59){
            minuto = 0
            horaActual += 1
        }
        else minuto = 30

        do{
            val horaS = horaActual.toString()
            if(minuto == 0) minutoS = "00"
            else minutoS = minuto.toString()

            val hour = "${horaS}:${minutoS}"
            if(hour.equals("23:30")){
                options.add(hour)
                done = true
            }
            else{
                options.add(hour)
                if(minutoS.equals("30")){
                    horaActual += 1
                    minuto = 0
                }else minuto = 30
            }
        }while(done == false)

        /*val hourMerged = "${horaActual}:${minutoS}"

        options.remove(hourMerged)
        options.add(0, hourMerged)*/

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.txt_time_spin.adapter = dataAdapter

        mView.txt_time_spin.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){
                horaTimer1 = mView.txt_time_spin.getItemAtPosition(position).toString()
                SetUpSpinnerTimeEnd(mView, horaTimer1)
                horaTimer1 = SplitHora(horaTimer1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?){}
        }
    }

    fun GetDateTimeServer(){
        presenter.GetDateTime()
    }

    override fun SuccessDateTime(response: ResponseDateTimeOrder){
        actualHour = response.hora!!
    }

    override fun ErrorDateTime(resp: String){
        toast(resp)
    }

    fun SplitHora(hora: String): String{

        var min = ""
        val array = hora.trim().split(":").toTypedArray()

        if(array[0].toInt() in 0..9){
            min = array[0]
            min = "0${min}"
        }else min = array[0]

        return "${min}:${array[1]}"
    }

    fun SetUpSpinnerTimeEnd(mView: View, lahora: String){

        val strs = lahora.trim().split(":").toTypedArray()
        val options = arrayListOf<String>()
        var done = false
                            //TODO. we put the next half hour , exmple: we received 16:30, time end will be 17:00
        var hora = strs[0].toInt()
        var minuto: Int
        if(strs[1].equals("00")) minuto = 30
        else{
            hora += 1
            minuto = 0
        }
        var minutoS = ""

        do{
            val horaS = hora.toString()
            if(minuto == 0) minutoS = "00"
            else minutoS = minuto.toString()

            val hour = "${horaS}:${minutoS}"
            if(hour.equals("23:30")){
                options.add(hour)
                done = true
            }
            else{
                options.add(hour)
                if(minutoS.equals("30")){
                    hora += 1
                    minuto = 0
                }else minuto = 30
            }
        }while(!done)



        /*if(mView.txt_date2_nc.text.toString().equals(mView.txt_date_nc.text.toString())){

            var hora = strs[0].toInt()
            var minuto = -1
            if(strs[1].equals("00")) minuto = 0
            else minuto = strs[1].toInt()
            var minutoS = ""

            do{
                val horaS = hora.toString()
                if(minuto == 0) minutoS = "00"
                else minutoS = minuto.toString()

                val hour = "${horaS}:${minutoS}"
                if(hour.equals("23:30")){
                    options.add(hour)
                    done = true
                }
                else{
                    options.add(hour)
                    if(minutoS.equals("30")){
                        hora += 1
                        minuto = 0
                    }else minuto = 30
                }
            }while(done == false)
        }
        else{
            //fecha distinta
            var hora = 0
            var minuto = 0
            var minutoS = ""

            do{
                val horaS = hora.toString()
                if(minuto == 0) minutoS = "00"
                else minutoS = minuto.toString()

                val hour = "${horaS}:${minutoS}"
                if(hour.equals("23:30")){
                    options.add(hour)
                    done = true
                }
                else{
                    options.add(hour)
                    if(minutoS.equals("30")){
                        hora += 1
                        minuto = 0
                    }else minuto = 30
                }
            }while(done == false)
        }*/

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.txt_time2_spin.adapter = dataAdapter

        mView.txt_time2_spin.setOnItemSelectedListener(object: OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){

                horatimer2 = mView.txt_time2_spin.getItemAtPosition(position).toString()
                horatimer2 = SplitHora(horatimer2)
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        })
    }

    fun GetActualHour(): String{
        val currentDate = Date() //Fecha actual del celular y hora
        val hdf = SimpleDateFormat("HH:mm")
        val hour = hdf.format(currentDate)
        return hour
    }

    fun SetUpSpinnerAlert(mView: View){

        val options = arrayListOf<String>()
        options.add("Nunca")
        options.add("En el momento del evento")
        options.add("5 minutos antes")
        options.add("15 minutos antes")
        options.add("30 minutos antes")
        options.add("1 hora antes")
        options.add("2 horas antes")
        options.add("12 horas antes")
        options.add("1 dia antes")
        options.add("1 semana antes")
        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.spinner_alerts.adapter = dataAdapter

        mView.spinner_alerts.setOnItemSelectedListener(object: OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){

                val option = mView.spinner_alerts.getItemAtPosition(position).toString()
                when(option){
                    "Nunca" -> caseAlert = 0
                    "En el momento del evento" -> caseAlert = 1
                    "5 minutos antes" -> caseAlert = 2
                    "15 minutos antes" -> caseAlert = 3
                    "30 minutos antes" -> caseAlert = 4
                    "1 hora antes" -> caseAlert = 5
                    "2 horas antes" -> caseAlert = 6
                    "12 horas antes" -> caseAlert = 7
                    "1 dia antes" -> caseAlert = 8
                    "1 semana antes" -> caseAlert = 9
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        })
    }

    fun GetContacts(){
        presenter.GetContactos()
    }

    override fun SuccessContacts(response: ResponseContacts){
        respContacts = response
        misContactos = response.contacts //I use it in AgendaEventoView

        LY_AGENDA.visibility = View.VISIBLE
        LY_LOADER_AGENDA.visibility = View.GONE
    }

    override fun ErrorContacts(msj: String){
        toast(msj)
    }

    fun SetUpSpinnerContacts(mView: View){

        val adaptador_dropdownList: ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)

        respContacts!!.contacts.forEach{item->
            if(!item.nombre.isNullOrEmpty()) adaptador_dropdownList.add(item.nombre+" "+item.apellidos)
        }

        mView.autocomplete_contacts.setOnTouchListener(OnTouchListener {_, event->
            val DRAWABLE_LEFT = 0
            val DRAWABLE_TOP = 1
            val DRAWABLE_RIGHT = 2
            val DRAWABLE_BOTTOM = 3
            if (event.action == MotionEvent.ACTION_UP){

                if(event.rawX >= mView.autocomplete_contacts.right - mView.autocomplete_contacts.compoundDrawables[DRAWABLE_RIGHT].bounds.width()){
                    mView.autocomplete_contacts.text.clear()
                    return@OnTouchListener false
                }
            }
            false
        })

        mView.autocomplete_contacts!!.setThreshold(1)
        mView.autocomplete_contacts!!.setAdapter(adaptador_dropdownList)
        mView.autocomplete_contacts!!.addTextChangedListener(object: TextWatcher {

            override fun afterTextChanged(s: Editable?){}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int){

                if(mView.autocomplete_contacts!!.isPerformingCompletion){
                    GetContactID(mView.autocomplete_contacts!!.text.toString(), mView)
                }
            }
        })
        mView.autocomplete_contacts!!.setOnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_NEXT) mView.txt_location_E.requestFocus()
            true
        }
    }

    fun GetContactID(name: String, mView: View){

        var id: Int? = null
        respContacts!!.contacts.forEach{item->
            val fullName = item.nombre+" "+item.apellidos
            if(name == fullName) id = item.id
        }

        SetUpRecycler(mView, name, id!!)
    }

    fun SetUpRecycler(mView: View, name: String, id: Int){

        if(contactsAdded.isNullOrEmpty()){

            idsContacts.add(id)
            contactsAdded.add(name)
            mView.rclv_contacts.setHasFixedSize(true)
            mView.rclv_contacts.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
            arrayAdapter.ChipsContactoEventoNuevo(contactsAdded, this, idsContacts, -1)
            mView.rclv_contacts.adapter = arrayAdapter
        }
        else{
            if(contactsAdded.contains(name)) toast("You have added this contact before")
            else{
                idsContacts.add(id)
                contactsAdded.add(name)
                arrayAdapter.notifyDataSetChanged()
            }
        }
    }

    /*fun PickUpBeginTime(mView: View, title: String){

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog2: AlertDialog? = null
        val viewTime = layoutInflater.inflate(R.layout.dialog_change_hour_date, null)

        viewTime.txt_title_time.text = title

        viewTime.btn_save_hour.setOnClickListener{
            val hour: Int = viewTime.time_pciker.currentHour
            val min: Int = viewTime.time_pciker.currentMinute
            var minuto = min.toString()
            var hora = hour.toString()
            var aux = 0

            if(min in 0..9){
                aux = 1
                minuto = "0$minuto"
            }

            if (hour in 0..9) {
                hora = "0$hora"
            }

            val startHour: StringBuilder
            if(aux == 0) startHour = StringBuilder().append(hora).append(":").append(min.toString())
            else startHour = StringBuilder().append(hora).append(":").append(minuto)

            val hourActual = GetActualHour()
            val horaCurrent = hourActual.trim().split(":").toTypedArray()
            val horaBegin = startHour.trim().split(":").toTypedArray()

            if(horaBegin[0] == horaCurrent[0]){
                if(horaBegin[1] >= horaCurrent[1]){
                    dialog2!!.dismiss()
                    PickUpEndTime(startHour, mView, "My event ends at:")
                }
                else toast("You must select a valid hour")
            }
            else if(horaBegin[0] > horaCurrent[0]){
                dialog2!!.dismiss()
                PickUpEndTime(startHour, mView, "My event ends at:")
            }
            else toast("You must select a valid hour")
        }

        mBuilder.setView(viewTime)
        dialog2 = mBuilder.create()
        dialog2.show()
    }*/

    /*fun PickUpEndTime(startHour: StringBuilder, mView: View, title: String){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog2: AlertDialog? = null
        val viewTime = layoutInflater.inflate(R.layout.dialog_change_hour_date, null)

        viewTime.txt_title_time.text = title

        viewTime.btn_save_hour.setOnClickListener{
            val hour: Int = viewTime.time_pciker.currentHour
            val min: Int = viewTime.time_pciker.currentMinute
            var minuto = min.toString()
            var hora = hour.toString()
            var aux = 0

            if(min in 0..9){
                aux = 1
                minuto = "0$minuto"
            }

            if (hour in 0..9) {
                hora = "0$hora"
            }

            val endHour: StringBuilder
            if(aux == 0) endHour = StringBuilder().append(hora).append(":").append(min.toString())
            else endHour = StringBuilder().append(hora).append(":").append(minuto)

            val horaBegin = startHour.trim().split(":").toTypedArray()
            val horaEnd = endHour.trim().split(":").toTypedArray()

            /*if(horaEnd[0] >= horaBegin[0]){
                if(horaEnd[0] > horaBegin[0]){
                    mView.txt_time.text = "${startHour} - ${endHour}"
                    dialog2!!.dismiss()
                }
                else if(horaEnd[1] > horaBegin[1]){
                    mView.txt_time.text = "${startHour} - ${endHour}"
                    dialog2!!.dismiss()
                }else toast("Your end hour must be greater than your start hour")
            }else toast("Your end hour must be greater than your start hour")*/
        }

        mBuilder.setView(viewTime)
        dialog2 = mBuilder.create()
        dialog2.show()
    }*/

    override fun SuccessEventCreated(){
        LY_LOADER_MAKING_EVENT.visibility = View.GONE
        LY_AGENDA.visibility = View.VISIBLE
        val retrofit = RetrofitController(this)
        retrofit.AlertDialogShow("Event was created successfully", false, this)
        GoAgenda(dateSelected!!) //reloading data events
    }

    fun GoAgenda(dateselected: String){
        presenter.GetMyAgenda(dateselected)
    }

    override fun ErrorAgenda(resp: String){
        toast(resp)
    }

    override fun SuccessAgenda(response: ResponseAgenda){
        SetData(response)
    }

    fun SetData(datos: ResponseAgenda){

        if(datos.data.isNullOrEmpty()){
            LY_NODATA.visibility = View.VISIBLE
            LY_RECYCLER.visibility = View.GONE
        }
        else{
            LY_NODATA.visibility = View.GONE
            LY_RECYCLER.visibility = View.VISIBLE
            rclv_agenda.setHasFixedSize(true)
            rclv_agenda.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            val arrayAdapter = AgendaAdapter()
            arrayAdapter.AgendaAdapter(datos.data, this)
            rclv_agenda.adapter = arrayAdapter
        }
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }

    /*fun convertirPMoAM(){
        //WITH DATEPICKER FROM ANDROID
        var hour: Int = viewTime.time_pciker.currentHour
        val min: Int = viewTime.time_pciker.currentMinute
        var minuto = min.toString()
        var aux = 0
        var format = ""

        if(min in 0..9){
            aux = 1
            minuto = "0$minuto"
        }
        if (hour == 0) {
            hour += 12;
            format = "AM"
        }
        else if (hour == 12) format = "PM"
        else if (hour > 12) {
            hour -= 12
            format = "PM"
        }
        else format = "AM"

        var endHour: StringBuilder? = null
        if(aux == 0){
            endHour = StringBuilder().append(hour).append(":").append(min.toString())
                .append(" ").append(format)
        }
        else endHour = StringBuilder().append(hour).append(":").append(minuto).append(format)
    }*/
}