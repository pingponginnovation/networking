package com.example.appgenesys.Retrofit

import com.example.appgenesys.*
import com.example.appnetworking.*
import com.example.appnetworking.ModelsRetrofit.*
import retrofit2.Call
import retrofit2.http.*

interface Api{

    @POST("login")
    fun login(@Body requestLogin: RequestLogin): Call<ResponseLogin>

    @POST("usuarios/match/lista")
    fun getMatch(@Header("Authorization")token: String, @Body requestMatch: RequestMatch): Call<ResponseMatch>

    @POST("perfil/usuario")
    fun getMyProfile(@Header("Authorization")token: String): Call<ResponseProfile>

    @GET("empresa/programa/actividad")
    fun getActivities(@Header("Authorization")token: String): Call<ResponseActividades>

    @POST("agenda/datos")
    fun getAgenda(@Header("Authorization")token: String, @Body requestAgenda: RequestAgenda): Call<ResponseAgenda>

    @GET("ponentes/datos")
    fun getSpeakers(@Header("Authorization")token: String): Call<ResponseSpeakers>

    @POST("perfil/ponente")
    fun getProfilePonente(@Header("Authorization")token: String, @Body request: RetrofitController.RequestPonente): Call<ResponseProfilePonente>

    @POST("perfil/contacto")
    fun getProfileContacto(@Header("Authorization")token: String, @Body request: RequestPerfilContactoBuscado): Call<ResponsePerfilContacto>

    @POST("proyectos/datos")
    fun getProyectos(@Header("Authorization")token: String, @Body request: RequestProyectos): Call<ResponseProyectos>

    @POST("proyectos/votar")
    fun votaProyecto(@Header("Authorization")token: String, @Body request: RequestVotarProyecto): Call<ResponseVotarProyecto>

    @POST("token/device")
    fun saveToken(@Header("Authorization")token: String, @Body request: RequestSaveToken): Call<ResponseSaveToken>

    @POST("invitacion/accion")
    fun respuestaInvitacion(@Header("Authorization")token: String, @Body request: RequestToInvitacion): Call<ResponseInvitacion>

    @POST("match/accion")
    fun setLikeDislike(@Header("Authorization")token: String, @Body request: RequestLikeDislike): Call<ResponseLikeDislike>

    @GET("empresa/contacto")
    fun getInfoGenius(@Header("Authorization")token: String): Call<ResponseInfoGenius>

    @POST("perfil/modificar")
    fun editProfile(@Header("Authorization")token: String, @Body request: RequestEditProfile): Call<ResponseEditProfile>

    @POST("perfil/usuario/lista/contactos")
    fun getContacts(@Header("Authorization")token: String): Call<ResponseContacts>

    @POST("agenda/crear/cita")
    fun createEvent(@Header("Authorization")token: String, @Body request: RequestNewEvent): Call<ResponseNewEvent>

    @POST("agenda/modificar/cita")
    fun editCita(@Header("Authorization")token: String, @Body request: RequestEditCita): Call<ResponseEditCita>

    @POST("agenda/datos/xId")
    fun datosCitaInvitacion(@Header("Authorization")token: String, @Body request: RequestDatosCita): Call<ResponseDatosCita>

    @POST("agenda/respuesta/cita")
    fun responderCita(@Header("Authorization")token: String, @Body request: RequestRespuestaCita): Call<ResponseRespuestaCita>

    @POST("menu")
    fun getMenu(@Header("Authorization")token: String): Call<ResponseMenu>

    @POST("perfil/busqueda")
    fun getPersonas(@Header("Authorization")token: String, @Body request: RequestPersonasByName): Call<ResponsePersonasByName>

    @POST("notificacion/datos")
    fun getNotifications(@Header("Authorization")token: String): Call<ResponseNotificaciones>

    @POST("datetime")
    fun getDateTime(): Call<ResponseDateTimeOrder>

    @POST("notificacion/estatus")
    fun updateNotification(@Header("Authorization")token: String, @Body request: RequestUpdateNotification): Call<ResponseUpdateNotification>

    @POST("chat/mensajes")
    fun getMensajesChat(@Header("Authorization")token: String, @Body request: RequestChatThread): Call<ResponseChatThread>

    @POST("chat/mensaje/enviar")
    fun sendMessage(@Header("Authorization")token: String, @Body request: RequestEnviarMensaje): Call<ResponseEnviarMensaje>

    @POST("proyectos/lista")
    fun getAllProjects(@Header("Authorization")token: String, @Body req: RequestProyectosGeneral): Call<ResponseProyectosGeneral>

    @POST("proyectos/crear")
    fun crearProyecto(@Header("Authorization")token: String, @Body req: RequestCrearProyecto): Call<ResponseCrearProyecto>

    @POST("agenda/inscripcion/conferencia")
    fun suscribeConference(@Header("Authorization")token: String, @Body req: RequestSuscribeConference): Call<ResponseSuscribeConference>

    @POST("experiencia/crear")
    fun crearExperiencia(@Header("Authorization")token: String, @Body req: RequestCrearExperiencia): Call<ResponseCrearExperiencia>

    @POST("experiencia/modificar")
    fun editExperiencia(@Header("Authorization")token: String, @Body req: RequestModificarExperiencia): Call<ResponseEditExperiencia>

    @POST("proyectos/modificar")
    fun editProyecto(@Header("Authorization")token: String, @Body req: RequestModificarProyecto): Call<ResponseModificarProyecto>

    @POST("perfil/modificar/descripcion")
    fun editBiography(@Header("Authorization")token: String, @Body req: RequestEditBiography): Call<ResponseEditBiography>

    @POST("intereses/eliminar")
    fun deleteInteres(@Header("Authorization")token: String, @Body req: RequestDeleteInteres): Call<ResponseDeleteInteres>

    @POST("intereses/crear")
    fun crearInteres(@Header("Authorization")token: String, @Body req: RequestCrearInteres): Call<ResponseCrearInteres>

    @POST("habilidades/crear")
    fun crearHabilidad(@Header("Authorization")token: String, @Body req: RequestCrearSkill): Call<ResponseCrearSkill>

    @POST("habilidades/eliminar")
    fun deleteSkill(@Header("Authorization")token: String, @Body req: RequestDeleteSkill): Call<ResponseDeleteSkill>

    @POST("agenda/meeting/iniciar")
    fun StartOrJoinMeeting(@Header("Authorization")token: String, @Body req: RequestJoinStartMeeting): Call<ResponseJoinStartMeeting>
}