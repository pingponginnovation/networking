package com.example.appnetworking.RedireccionAgendaInvitacion

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.*
import com.example.appnetworking.Agenda.AgendaView
import com.example.appnetworking.AgendaEvento.AgendaEventoView
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.asistentes
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.asistentesMeeting
import com.example.appnetworking.ModelsRetrofit.ResponseDatosCitaOrder
import kotlinx.android.synthetic.main.redireccionamiento_agenda_invitacion.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class AgendaInvitacionView: AppCompatActivity(), AgendaInvitacion.View{

    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: AgendaInvitacion.Presenter
    private lateinit var response: ResponseDatosCita
    private var flag = -1

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.redireccionamiento_agenda_invitacion)

        controlAlert = ControllerAlertDialog(this)
        presenter = AgendaInvitacionPresenter(this, applicationContext)
        ConfigToolbar()

        val citaId = intent.getIntExtra("ID_CITA", -1)
        val perfilID = intent.getIntExtra("ID_PERFIL", -1)
        flag = intent.getIntExtra("FLAG", -1)

        presenter.GetInfoCita(RequestDatosCita(citaId, perfilID))

        btn_radio_si.setOnClickListener{
            controlAlert!!.ProgressBar()
            val retrofit = RetrofitController(this)
            retrofit.ResponderCita(RequestRespuestaCita(citaId, perfilID, 1), this)
        }

        btn_radio_no.setOnClickListener{
            showAlert(citaId, perfilID, this)
        }

        if(flag == 2) RADIOS.visibility = View.GONE //Es meeting general (informativo)
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    override fun ErrorCita(resp: String){
        controlAlert!!.ShowAlert(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        this.response = response
        SetData(response.data)
    }

    fun SetData(datos: ResponseDatosCitaOrder){
        txt_title_redir.text = datos.title
        txt_hora_redir.text = "${datos.fecha} at ${datos.hora_inicio} - ${datos.hora_fin}"
        txt_lugar_redir.text = datos.lugar
        txt_descripcion_redir.text = datos.descripcion
    }

    fun showAlert(citaId: Int, perfilID: Int, activity: AgendaInvitacionView){
        alert("Are you sure you won´t assist to the meeting?", "Warning"){
            positiveButton("Yes") {
                controlAlert!!.ProgressBar()
                val retrofit = RetrofitController(applicationContext)
                retrofit.ResponderCita(RequestRespuestaCita(citaId, perfilID, 0), activity)
            }
            negativeButton("Cancel"){}
        }.show()
    }

    fun UpdateView(){
        controlAlert!!.StopProgress()
        val intent = Intent(this, AgendaView::class.java)
        startActivity(intent)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}