package com.example.appnetworking.ContactosAndChats

import com.example.appgenesys.ResponseContacts

interface Contactos{

    interface View{
        fun ErrorContacts(resp: String)
        fun SuccessContacts(response: ResponseContacts)
    }

    interface Presenter{
        fun GetContacts()
        fun ErrorContacts(resp: String)
        fun SuccessContacts(response: ResponseContacts)
    }

    interface Model{
        fun GetContacts()
        fun ErrorContacts(resp: String)
        fun SuccessContacts(response: ResponseContacts)
    }
}