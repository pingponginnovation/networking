package com.example.appnetworking.ProyectosLista

import com.example.appnetworking.ResponseProfilePonente
import com.example.appnetworking.ResponseProyectos

interface ProyectosLista{

    interface View{
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectos)
    }

    interface Presenter{
        fun GetProyectos(idPonente: Int)
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectos)
    }

    interface Model{
        fun GetProyectos(idPonente: Int)
        fun ErrorProyectos(resp: String)
        fun SuccessProyectos(response: ResponseProyectos)
    }
}