package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseCrearSkill(
    @field:SerializedName("mensaje")
    val msj: String? = null,

    @field:SerializedName("skill_id")
    val skill_id: Int? = null,

    @field:SerializedName("skill")
    val skill: String? = null
)