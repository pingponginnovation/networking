package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfilePonenteOrder
import com.google.gson.annotations.SerializedName

data class ResponseProfilePonente(

    @SerializedName("data")
    val data: ResponseProfilePonenteOrder
)