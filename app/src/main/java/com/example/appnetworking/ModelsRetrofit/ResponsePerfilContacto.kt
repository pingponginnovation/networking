package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponsePerfilContactoOrder
import com.google.gson.annotations.SerializedName

data class ResponsePerfilContacto(

    @SerializedName("data")
    val data: ResponsePerfilContactoOrder
)