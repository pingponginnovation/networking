package com.example.appnetworking.Profile

import com.example.appgenesys.ResponseSuscribeConference
import com.example.appnetworking.RequestSuscribeConference
import com.example.appnetworking.ResponseProfilePonente

interface ProfileSpeaker{

    interface View{
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfilePonente)
        fun SuccessSuscribeConference(response: ResponseSuscribeConference)
        fun ErrorSuscribe(msj: String)
    }

    interface Presenter{
        fun GetProfile(idPonente: Int)
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfilePonente)
        fun SuscribeToConference(req: RequestSuscribeConference)
        fun SuccessSuscribeConference(response: ResponseSuscribeConference)
        fun ErrorSuscribe(msj: String)
    }

    interface Model{
        fun GetProfile(idPonente: Int)
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfilePonente)
        fun SuscribeToConference(req: RequestSuscribeConference)
        fun SuccessSuscribeConference(response: ResponseSuscribeConference)
        fun ErrorSuscribe(msj: String)
    }
}