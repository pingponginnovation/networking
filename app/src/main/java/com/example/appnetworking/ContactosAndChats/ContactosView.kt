package com.example.appnetworking.ContactosAndChats

import android.app.SearchManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.Adapters.ContactosAdapter
import com.example.appnetworking.FCMnotifications.Config
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.contactos_activity.*
import org.jetbrains.anko.toast
import java.util.*

    //*TODO. Por ahora esta clase no se utiliza. En lugar de ello se usa TabsViewpagerActivity

class ContactosView: AppCompatActivity(), Contactos.View{

    private lateinit var presenter: Contactos.Presenter
    private lateinit var preferences: PreferencesController
    private var filterList: ArrayList<ResponseSpeakersOrder>? = null
    private var adapterContacts: ContactosAdapter? = null
    private var searchView: SearchView? = null
    private var myBroadcastReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contactos_activity)

        /*ConfigToolbar()
        //RegisterBroadCast()
        filterList = ArrayList()
        preferences = PreferencesController(applicationContext)
        presenter = ContactosPresenter(this, applicationContext, preferences)
        GetContacts()*/
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back_black)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun RegisterBroadCast(){
        /*
         * Broadcast receiver calls in two scenarios
         * 1. gcm registration is completed
         * 2. when new push notification is received
         * */
        myBroadcastReceiver = object : BroadcastReceiver(){
            override fun onReceive(context: Context, intent: Intent){
                when (intent.action){
                    //Config.REGISTRATION_COMPLETE ->                         // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        //subscribeToGlobalTopic()
                    //Config.SENT_TOKEN_TO_SERVER ->                         // gcm registration id is stored in our server's MySQL
                        //Log.e(TAG, "GCM registration id is sent to our server")
                    Config.PUSH_NOTIFICATION ->{
                        //handlePushNotification(intent)
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu!!.findItem(R.id.menu_search_icon)

        if(searchItem != null){

            searchView = searchItem.actionView as SearchView
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

                override fun onQueryTextSubmit(query: String?): Boolean{
                    adapterContacts!!.getFilter().filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    adapterContacts!!.getFilter().filter(newText)
                    return false
                }

            })
        }

        return true
    }

    fun GetContacts(){
        presenter.GetContacts()
    }

    override fun ErrorContacts(resp: String){
        toast(resp)
    }

    override fun SuccessContacts(response: ResponseContacts){
        SetData(response)
    }

    fun SetData(datos: ResponseContacts){

        if(datos.contacts.isNullOrEmpty()){
            LY_LOADER_CONT.visibility = View.VISIBLE
            LY_CONTACTS.visibility = View.GONE
        }
        else{
            rclv_contactos.setHasFixedSize(true)
            rclv_contactos.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            adapterContacts = ContactosAdapter()
            adapterContacts!!.ContactosAdapter(datos.contacts, this)
            rclv_contactos.adapter = adapterContacts
        }
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}