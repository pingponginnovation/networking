package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.google.gson.annotations.SerializedName

data class ResponseMatch(

    @SerializedName("data")
    val data: ArrayList<ResponseMatchOrder>,

    @SerializedName("usuarios_busqueda")
    val usersRegistered: ArrayList<ResponseMatchUsersBusquedaOrder>
)