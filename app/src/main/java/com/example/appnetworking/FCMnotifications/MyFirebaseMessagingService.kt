package com.example.appnetworking.FCMnotifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.appnetworking.AgendaEvento.AgendaEventoView
import com.example.appnetworking.ContactoChat.ContactoChatView
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.R
import com.example.appnetworking.RedireccionAgendaInvitacion.AgendaInvitacionView
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyFirebaseMessagingService: FirebaseMessagingService(){

    var notificationIDPrefs: String? = null
    var notificationID: Int? = null
    var preferences: PreferencesController? = null

    lateinit var notificationChannel : NotificationChannel
    lateinit var notification: Notification
    lateinit var notificationSummary: Notification
    private val channelId = "com.example.vicky.notificationexample"
    private val description = "Test notification"
    val GROUP_NETWORKING = "NOTIFICACIONESPUSH"
    var body = ""
    var title = ""
    var id = -1
    var id_cita = -1
    var chat_id = -1
    var fecha_msj: String? = null
    var nombre = ""
    var puesto = ""
    var foto = ""
    var flagMeeting = -1
    private var idNotificacionParaLeer = -1

    override fun onMessageReceived(remoteMessage: RemoteMessage){

        preferences = PreferencesController(this)
        notificationIDPrefs = preferences!!.getIDnotification()!!
        if(notificationIDPrefs.isNullOrEmpty()) notificationID = 0
        else{
            var n: Int = notificationIDPrefs!!.toInt()
            n += 1
            notificationID = n
        }

        remoteMessage.data.isNotEmpty().let{

            Log.e("Message data payload: ", remoteMessage.data.toString())

            body = remoteMessage.data.get("body").toString()
            title = remoteMessage.data.get("title").toString()
            id = remoteMessage.data.get("perfil_id")!!.toInt()
            idNotificacionParaLeer = remoteMessage.data.get("notificacion_id")!!.toInt()
            val typeNotification = remoteMessage.data.get("type_notificacion").toString()

            if(typeNotification.equals("Chat")){
                nombre = remoteMessage.data.get("nombre")!!
                puesto = remoteMessage.data.get("puesto")!!
                foto = remoteMessage.data.get("imagen_perfil")!!
                chat_id = remoteMessage.data.get("canal_id")!!.toInt()
                fecha_msj = remoteMessage.data.get("fecha")!!

                ProcessMessageChat()

                if(preferences!!.getStateBroadcast()!!.equals("false")){
                    Log.e("false", "false")
                    GetReadyNotification(id, typeNotification, notificationID!!, idNotificacionParaLeer)
                }
            }
            else if(typeNotification.equals("CitaEnviada")){
                flagMeeting = remoteMessage.data.get("flag_meeting")!!.toInt()
                id_cita = remoteMessage.data.get("cita_id")!!.toInt()
            }

            if (/* Check if data needs to be processed by long running job */ true){
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }
        }
        remoteMessage.notification?.let {
            Log.e("Message Notification:", it.body!!)
        }
    }

    fun ProcessMessageChat(){
        val pushNotification = Intent(Config.PUSH_NOTIFICATION)
        pushNotification.putExtra("message", body)
        pushNotification.putExtra("chat_ID", chat_id)
        pushNotification.putExtra("perfil_id", id)
        pushNotification.putExtra("fecha", fecha_msj)
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)
    }

    /*fun isAppIsInBackground(): Boolean{

        var isInBackground = true
        val am = applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH){
            val runningProcesses = am.runningAppProcesses
            for (processInfo in runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (activeProcess in processInfo.pkgList) {
                        if (activeProcess == applicationContext.packageName) isInBackground = false
                    }
                }
            }
        }
        else{
            val taskInfo = am.getRunningTasks(1)
            val componentInfo = taskInfo[0].topActivity
            if (componentInfo!!.packageName == applicationContext.packageName) isInBackground = false
        }
        return isInBackground
    }*/

    fun GetReadyNotification(id: Int, type: String, notificationID: Int, idNotificacoinForRead: Int){

        var intent: Intent? = null

        if(type.equals("CitaEnviada")){

            if(flagMeeting == 0 || flagMeeting == 2){
                //Notificacion de invitacion a un meeting, para aceptar o rechazar
                intent = Intent(this, AgendaInvitacionView::class.java)
                intent.putExtra("ID_PERFIL", id)
                intent.putExtra("ID_CITA", id_cita)
                intent.putExtra("FLAG", flagMeeting)
                intent.putExtra("ID_NOTIFICACION", idNotificacoinForRead)
            }
            else if(flagMeeting == 1){
                //Notificacion para poder editar el meeting si asi lo requiere el creador.
                intent = Intent(this, AgendaEventoView::class.java)
                intent.putExtra("ID_PERFIL", id)
                intent.putExtra("ID_CITA", id_cita)
                intent.putExtra("ID_NOTIFICACION", idNotificacoinForRead)
            }
        }
        else if(type.equals("InvitacionFriend")){
                //Notificacion cuando alguien hizo match o agrega un contacto
            intent = Intent(this, PerfilContactoView::class.java)
            intent.putExtra("FROM", "Notification")
            intent.putExtra("MATCH", type)
            intent.putExtra("ID_PERFIL", id)
            intent.putExtra("ID_NOTIFICACION", idNotificacoinForRead)
        }
        else{
                //Notificacion de un chat
            intent = Intent(this, ContactoChatView::class.java)
            intent.putExtra("ID_HIS_PROFILE", id)
            intent.putExtra("NAME", nombre)
            intent.putExtra("PUESTO", puesto)
            intent.putExtra("IMG", foto)
            intent.putExtra("CHAT_ID", chat_id)
        }

        val pendingIntent = PendingIntent.getActivity(this, notificationID, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val alarmSound: Uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + this.packageName + "/raw/twitter")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){

            notificationChannel = NotificationChannel(channelId, description,
                NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            SetUpNotification(pendingIntent, alarmSound)
            with(NotificationManagerCompat.from(this)){
                notify(notificationID, notification)
            }
            //SendNotification()
        }
        else{
            SetUpNotification(pendingIntent, alarmSound)
            with(NotificationManagerCompat.from(this)){
                notify("ss",notificationID, notification)
            }

            /*if (Build.VERSION.SDK_INT >= 24) SendNotification()
            else{
                with(NotificationManagerCompat.from(this)){
                    notify(notificationID, notification)
                }
            }*/
        }
        preferences!!.SaveIDNotification(notificationID)
    }

    fun SetUpNotification(pendingIntent: PendingIntent, alarmSound: Uri){

        notification = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.genius)
            .setLargeIcon(BitmapFactory.decodeResource(this.resources,R.mipmap.genius))
            .setContentTitle(title)
            .setSound(alarmSound)
            .setContentText(body)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setGroup(GROUP_NETWORKING)
            .build()
    }

    fun SendNotification(){     //Para crear grupo de notificaciones en versiones >= 7
        notificationSummary = NotificationCompat.Builder(this, channelId)
            //set content text to support devices running API level < 24
            .setContentText("New messages")
            .setSmallIcon(R.mipmap.genius)
            .setAutoCancel(true)
            .setStyle(NotificationCompat.InboxStyle()
                .setSummaryText("New messages"))
            .setGroup(GROUP_NETWORKING)
            .setGroupSummary(true)
            .build()

        NotificationManagerCompat.from(this).apply{
            notify(notificationID!!, notification)
            notify(0, notificationSummary)
        }
    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")
        Log.e("NUEVOTOKEN", "SIIII")

        sendRegistrationToServer(token)
    }

    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    private fun sendRegistrationToServer(token: String?) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }
}
