package com.example.appnetworking.Adapters

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosCategoriasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosGeneralCategoriesOrder
import com.example.appnetworking.NewProject.NewProjectView
import com.example.appnetworking.Proyecto.EditProyectView
import com.example.appnetworking.R

class EditProjectCategoriesAdapter: RecyclerView.Adapter<EditProjectCategoriesAdapter.ViewHolder>(){

    //TODO. Recibo 2 arrays de categorias para 2 casos diferentes:
    //TODO. Cuando vengo desde la vista de "mis proyectos" y cuando vengo de "Proyectos generales"

    private var list: ArrayList<ResponseProyectosCategoriasOrder>? = null
    private var list2: ArrayList<ResponseProyectosGeneralCategoriesOrder>? = null
    private lateinit var actEditProject: EditProyectView
    private var category_name_passed = ""
    init {
        categoriaActivada = 0
    }
    companion object{
        var categoriaActivada = 0
        private var lastSelected: TextView? = null
        var id_categoria_selectedd = -1
        var category_namee = ""

        fun ClearCategoriesList(){
            if(lastSelected != null){
                lastSelected!!.setTextColor(Color.parseColor("#820A47"))
                lastSelected!!.setBackgroundResource(R.drawable.buttons_round_main)
                id_categoria_selectedd = -1
                category_namee = ""
                lastSelected = null
            }
        }
    }

    fun EditProjectCategoriesAdapter(categories: ArrayList<ResponseProyectosCategoriasOrder>,
                                     categories2: ArrayList<ResponseProyectosGeneralCategoriesOrder>,
                                     actEditProject: EditProyectView, categorianame: String){
        list = categories
        list2 = categories2
        this.actEditProject = actEditProject
        category_name_passed = categorianame
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val categorie = holder.itemView.findViewById(R.id.btn_categorie) as TextView

        if(list2!!.isEmpty()){
            categorie.text = list!![position].categoria!!
            if(list!![position].categoria.equals(category_name_passed, true)){
                categoriaActivada = 1
                lastSelected = categorie
                category_namee = list!![position].categoria!!
                id_categoria_selectedd = list!![position].id_categorie!!
                categorie.setTextColor(Color.WHITE)
                categorie.setBackgroundResource(R.drawable.buttons_round_main_selected)
            }
        }
        else{
            categorie.text = list2!![position].categoria!!
            if(list2!![position].categoria.equals(category_name_passed, true)){
                categoriaActivada = 1
                lastSelected = categorie
                category_namee = list2!![position].categoria!!
                id_categoria_selectedd = list2!![position].id_categorie!!
                categorie.setTextColor(Color.WHITE)
                categorie.setBackgroundResource(R.drawable.buttons_round_main_selected)
            }
        }

        if(position == 8) actEditProject.ConfigAutocomplete(list!!, list2!!) //se ejecuta hasta que carga el ultimo item

        categorie.setOnClickListener{view->

            actEditProject.Autocomplete(false)

            if(lastSelected != null){
                lastSelected!!.setTextColor(Color.parseColor("#820A47"))
                lastSelected!!.setBackgroundResource(R.drawable.buttons_round_main)
                lastSelected = categorie
            }
            else lastSelected = categorie

            if(list2!!.isEmpty()){
                category_namee = list!![position].categoria!!
                id_categoria_selectedd = list!![position].id_categorie!!
            }
            else{
                category_namee = list2!![position].categoria!!
                id_categoria_selectedd = list2!![position].id_categorie!!
            }

            categorie.setTextColor(Color.WHITE)
            categorie.setBackgroundResource(R.drawable.buttons_round_main_selected)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_categories_projects, parent, false))
    }

    override fun getItemCount(): Int{
        if(list2!!.isEmpty()){
            if(list!!.size > 9) return 9
            else return list!!.size
        }
        else{
            if(list2!!.size > 9) return 9
            else return list2!!.size
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}