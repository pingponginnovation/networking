package com.example.appnetworking.Models
import java.io.Serializable

data class SkillsForEdit(
    val name: String,
    val id: Int
): Serializable