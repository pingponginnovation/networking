package com.example.appnetworking.Profile

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.developer.kalert.KAlertDialog
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appnetworking.Adapters.ChipsExperienceProfilePonente
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.DecorationRecyclers.GridItemDecoration
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.R
import com.example.appnetworking.RequestSuscribeConference
import com.example.appnetworking.ResponseProfilePonente
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.speaker_profile.*
import org.jetbrains.anko.toast

class ProfileSpeakerView: AppCompatActivity(), ProfileSpeaker.View{

    private lateinit var presenter: ProfileSpeaker.Presenter
    private var controlAlert: ControllerAlertDialog? = null
    private var id_conferencia = ""

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.speaker_profile)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back)
        presenter = ProfileSpeakerPresenter(this, applicationContext)
        controlAlert = ControllerAlertDialog(this)
        val id_ponente = intent.getStringExtra("idponente")
        GetProfilePonente(id_ponente.toInt())

        btn_suscribe.setOnClickListener{
            controlAlert!!.ProgressBar()
            presenter.SuscribeToConference(RequestSuscribeConference(id_conferencia))
        }
    }

    override fun SuccessSuscribeConference(response: ResponseSuscribeConference){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(response.msj)
    }

    override fun ErrorSuscribe(msj: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(msj)
    }

    fun GetProfilePonente(id: Int){
        presenter.GetProfile(id)
    }

    override fun ErrorProfile(resp: String){
        toast(resp)
    }

    override fun SuccessProfile(response: ResponseProfilePonente){
        SetData(response.data)
        SetIntereses(response.data)
    }

    fun SetData(datos: ResponseProfilePonenteOrder){
        txt_name.text = datos.name+" "+datos.apellidos
        txt_ocupacion.text = (datos.ocupation)
        txt_residencia.text = (datos.address)
        txt_titulo.text = datos.conferencia.name
        if(datos.conferencia.place.isNullOrEmpty()) txt_lugar.text = "Place not assigned"
        else txt_lugar.text = datos.conferencia.place
        if(datos.conferencia.inscrito == 1){
            btn_suscribe.text = "Subscribed"
            btn_suscribe.isEnabled = false
        }
        else{
            btn_suscribe.text = "Add to agenda"
            btn_suscribe.isEnabled = true
        }
        txt_fecha_hora.text = datos.conferencia.fecha
        id_conferencia = datos.conferencia.id.toString()
        txt_descripcion.text = datos.description
        Picasso.get().load(datos.img_perfil).into(img_myphoto)
        if(datos.conferencia.descripcion.isNullOrEmpty()) txt_contenido.text = "No details"
        else txt_contenido.text = datos.conferencia.descripcion
    }

    fun SetIntereses(response: ResponseProfilePonenteOrder){

        rclv_experiences_ponente.setHasFixedSize(true)
        rclv_experiences_ponente.layoutManager = GridLayoutManager(this, 4)
        rclv_experiences_ponente.addItemDecoration(GridItemDecoration(10, 2))
        val arrayAdapter = ChipsExperienceProfilePonente(response.experiencias, this)
        rclv_experiences_ponente.adapter = arrayAdapter

        LY_SPEAKER_PROFILE.visibility = View.VISIBLE
        LY_LOADER_SPEAKER_PROFILE.visibility = View.GONE
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}