package com.example.appnetworking.PersonasByIntereses

import android.content.Context
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseMatch

class PersonasByInteresesModel(viewPresenter: PersonasByIntereses.Presenter, ctx: Context): PersonasByIntereses.Model{

    private val presenter = viewPresenter
    private var context: Context? = null

    init {
        context = ctx
    }

    override fun GetMatchesFiltro(filtro: Int, filtros: String){
        val retrofit = RetrofitController(context!!)
        retrofit.MatchesFilter(this, filtro, filtros)
    }

    override fun ErrorMatches(resp: String){
        presenter.ErrorMatches(resp)
    }

    override fun SuccessMatchFiltro(response: ResponseMatch){
        presenter.SuccessMatchFiltro(response)
    }
}