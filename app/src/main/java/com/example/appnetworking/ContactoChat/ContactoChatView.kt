package com.example.appnetworking.ContactoChat

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.RequestEnviarMensaje
import com.example.appgenesys.ResponseChatThread
import com.example.appnetworking.Adapters.ChatSpecificThreadAdapter
import com.example.appnetworking.FCMnotifications.Config
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.R
import com.example.appnetworking.ResponseEnviarMensaje
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.chat_contacto_activity.*
import org.jetbrains.anko.toast

class ContactoChatView: AppCompatActivity(), ContactoChat.View{

    data class ThreadChat(val id_perfil: Int, val mensaje: String, val date: String)

    private lateinit var presenter: ContactoChat.Presenter
    private lateinit var preferences: PreferencesController
    private var filterList: ArrayList<ResponseSpeakersOrder>? = null
    private var adapterChat: ChatSpecificThreadAdapter? = null
    private var myBroadcastReceiver: BroadcastReceiver? = null
    private var arrayMensajes: ArrayList<ThreadChat> = ArrayList()
    private var canal_id_chat = -1
    private var mensaje: String? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat_contacto_activity)

        ConfigToolbar()
        SetExtras()
        RegisterBroadCast()
        filterList = ArrayList()
        preferences = PreferencesController(applicationContext)
        presenter = ContactoChatPresenter(this, applicationContext, preferences)
        GetChat()

        fab_send.setOnClickListener{
            if(edt_msj.text.toString().isEmpty()) toast("Message can't be empty")
            else{
                mensaje = edt_msj.text.toString()
                val request = RequestEnviarMensaje(canal_id_chat, mensaje!!)
                presenter.EnviarMensaje(request)
                edt_msj.setText("")
            }
        }

        btn_show_profile.setOnClickListener{
            val intentt = Intent(this, PerfilContactoView::class.java)
            intentt.putExtra("ID_PERFIL", intent.getIntExtra("ID_HIS_PROFILE", -1))
            intentt.putExtra("FROM", "MY_CONTACT")
            startActivity(intentt)
        }

        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
    }

    fun SetExtras(){
        Picasso.get().load(intent.getStringExtra("IMG")).into(img_chat)
        txt_name_chat.text = intent.getStringExtra("NAME")
        txt_puesto_chat.text = intent.getStringExtra("PUESTO")
    }

    private fun RegisterBroadCast(){
        myBroadcastReceiver = object : BroadcastReceiver(){
            override fun onReceive(context: Context, intent: Intent){
                when (intent.action){
                    Config.PUSH_NOTIFICATION -> handlePushNotification(intent)
                }
            }
        }
    }

    private fun handlePushNotification(intent: Intent){
        val message = intent.getStringExtra("message")
        val id_profile = intent.getIntExtra("perfil_id", -1)
        val date = intent.getStringExtra("fecha")
        UpdateRow(id_profile, message!!, date!!)
        GetChat()//Para actulizar los mensajes que me manda la persona, mientras estamos dentro del chat
        //y marcarlo como leído.
    }

    private fun UpdateRow(mi_id: Int, msj: String, fecha: String){
        val newMsj = ThreadChat(mi_id, msj, fecha)
        arrayMensajes.add(newMsj)
        adapterChat!!.notifyDataSetChanged()
        if(adapterChat!!.itemCount > 1){
            rclv_chat.layoutManager!!.smoothScrollToPosition(rclv_chat, null, adapterChat!!.itemCount-1)
        }
    }

    private fun clearNotificationTray(){
        val notificationManager = this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()
    }

    fun GetChat(){
        canal_id_chat = intent.getIntExtra("CHAT_ID", -1)
        presenter.GetMensajes(canal_id_chat)
    }

    override fun ErrorMensajes(resp: String){
        toast(resp)
    }

    override fun SuccessMensajes(response: ResponseChatThread){
        SetData(response)
    }

    fun SetData(datos: ResponseChatThread){

        datos.data_mensajes.forEach{
            val chat = ThreadChat(it.perfil_id!!, it.msj!!, it.fecha!!)
            arrayMensajes.add(chat)
        }

        rclv_chat.setHasFixedSize(true)
        rclv_chat.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapterChat = ChatSpecificThreadAdapter()
        adapterChat!!.ChatSpecificThreadAdapter(arrayMensajes, this,
            preferences.getMyIDprofile()!!.toInt())
        rclv_chat.adapter = adapterChat

        if(adapterChat!!.itemCount > 1) rclv_chat.layoutManager!!
            .scrollToPosition(adapterChat!!.itemCount-1)
    }

    override fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje){
        UpdateRow(preferences.getMyIDprofile()!!.toInt(), resp.mi_msj!!, resp.date!!)
    }

    override fun ErrorEnviarMensaje(resp: String){
        toast(resp)
    }

    override fun onResume(){
        super.onResume()
        clearNotificationTray()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(myBroadcastReceiver!!, IntentFilter(Config.PUSH_NOTIFICATION))
        preferences.SaveStateBroadcast("true") //Is true when the user is inside the chat
        //and don't show the notification, only refresh the chat thread from the broadcast receiver
    }

    override fun onPause(){
        super.onPause()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myBroadcastReceiver!!)
        preferences.SaveStateBroadcast("false")
    }
}