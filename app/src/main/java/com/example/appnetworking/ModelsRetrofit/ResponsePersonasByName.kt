package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponsePerfilContactoOrder
import com.example.appnetworking.ModelsRetrofit.ResponsePersonasByNameOrder
import com.google.gson.annotations.SerializedName

data class ResponsePersonasByName(

    @SerializedName("data")
    val data: ArrayList<ResponsePersonasByNameOrder>
)