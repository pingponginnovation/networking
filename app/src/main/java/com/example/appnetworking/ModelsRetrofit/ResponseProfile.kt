//package com.example.appnetworking
package com.example.appgenesys.Retrofit

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileCatalogoEmpresasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileJornadasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileOrder
import com.google.gson.annotations.SerializedName

data class ResponseProfile(

    @SerializedName("data")
    val data: ResponseProfileOrder,

    @SerializedName("cat_empresas")
    val catalogo_empresas: ArrayList<ResponseProfileCatalogoEmpresasOrder>,

    @SerializedName("tipo_empleo")
    val jornadas: ArrayList<ResponseProfileJornadasOrder>
)