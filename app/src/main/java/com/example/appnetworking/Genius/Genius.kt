package com.example.appnetworking.Genius

import com.example.appgenesys.ResponseAgenda
import com.example.appnetworking.ResponseInfoGenius

interface Genius{

    interface View{
        fun ErrorGenius(resp: String)
        fun SuccessInfo(response: ResponseInfoGenius)
    }

    interface Presenter{
        fun GetInformacion()
        fun ErrorGenius(resp: String)
        fun SuccessInfo(response: ResponseInfoGenius)
    }

    interface Model{
        fun GetInformacion()
        fun ErrorGenius(resp: String)
        fun SuccessInfo(response: ResponseInfoGenius)
    }
}