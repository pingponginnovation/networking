package com.example.appnetworking

import com.google.gson.annotations.SerializedName

data class ResponseNotificacionesOrder(

    @SerializedName("notificacion_id")
    val id_notification: Int? = null,

    @SerializedName("titulo")
    val title: String? = null,

    @SerializedName("mensaje")
    val msj: String? = null,

    @SerializedName("fecha_creado")
    val fecha_created: String? = null,

    @SerializedName("img_perfil")
    val img: String? = null,

    @SerializedName("estatus")
    val status: Int? = null,

    @SerializedName("perfil_id")
    val profile_id: Int? = null,

    @SerializedName("cita_id")
    val cita_id: Int? = null,

    @SerializedName("proyecto_id")
    val id_proyecto: Int? = null,

    @SerializedName("match")
    val match_type: String? = null
)