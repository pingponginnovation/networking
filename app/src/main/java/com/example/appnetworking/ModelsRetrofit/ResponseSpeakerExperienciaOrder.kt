package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseSpeakerExperienciaOrder(

    @field:SerializedName("cargo")
    val cargo: String? = null,

    @field:SerializedName("empresa")
    val empresa: String? = null
)