package com.example.appnetworking.ProyectosLista

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseProfilePonente
import com.example.appnetworking.ResponseProyectos

class ProyectosListaModel(viewPresenter: ProyectosLista.Presenter, ctx: Context): ProyectosLista.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetProyectos(idPonente: Int){
        val retrofit = RetrofitController(context!!)
        retrofit.GetProyectos(this, idPonente)
    }

    override fun ErrorProyectos(resp: String){
        presenter.ErrorProyectos(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectos){
        presenter.SuccessProyectos(response)
    }
}