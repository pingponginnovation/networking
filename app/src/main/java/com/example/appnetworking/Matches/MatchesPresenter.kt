package com.example.appgenesys.Login

import android.content.Context
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePerfilContacto

class MatchesPresenter(viewMatch: Matches.View, ctx: Context): Matches.Presenter{

    private val view = viewMatch
    private val model = MatchesModel(this, ctx)

    //MODEL
    override fun GetMatches(filtro: Int, searchs: String){
        model.GetMatches(filtro, searchs)
    }

    override fun SearchContacto(id_perfil: Int){
        model.SearchContacto(id_perfil)
    }

    //VIEW
    override fun ErrorMatches(resp: String){
        view.ErrorMatches(resp)
    }

    override fun SuccessMatches(response: ResponseMatch){
        view.SuccessMatches(response)
    }

    override fun ContactoSuccess(response: ResponsePerfilContacto){
        view.ContactoSuccess(response)
    }
}