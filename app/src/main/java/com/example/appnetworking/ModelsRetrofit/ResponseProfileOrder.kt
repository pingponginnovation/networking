package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ResponseProfileOrder{

    @field:SerializedName("perfil_id")
    val id: Int? = null

    @field:SerializedName("nombre")
    val name: String? = null

    @field:SerializedName("apellidos")
    val apellidos: String? = null

    @field:SerializedName("genero")
    val genre: String? = null

    @field:SerializedName("fecha_nacimiento")
    val date_birth: String? = null

    @field:SerializedName("correo")
    val email: String? = null

    @field:SerializedName("celular")
    val cel: String? = null

    @field:SerializedName("telefono")
    val phone: String? = null

    @field:SerializedName("direccion")
    val address: String? = null

    @field:SerializedName("pagina_web")
    val web: String? = null

    @field:SerializedName("empresa")
    val empresa: String? = null

    @field:SerializedName("puesto")
    val ocupation: String? = null

    @field:SerializedName("sector")
    val sector: String? = null

    @field:SerializedName("sinopsis")
    val description: String? = null

    @field:SerializedName("imagen_perfil")
    val img_perfil: String? = null

    @SerializedName("intereses")
    val interest: ResponseProfileIntereses? = null

    @SerializedName("experiencias")
    val experiences: ResponseProfileExperiencias? = null

    @SerializedName("habilidades")
    val habilidades: ResponseProfileHabilidades? = null
}