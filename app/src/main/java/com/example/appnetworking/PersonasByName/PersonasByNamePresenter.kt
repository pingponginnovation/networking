package com.example.appnetworking.PersonasByName

import android.content.Context
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePersonasByName

class PersonasByNamePresenter(viewMatch: PersonasByName.View, ctx: Context): PersonasByName.Presenter{

    private val view = viewMatch
    private val model = PersonasByNameModel(this, ctx)

    //MODEL
    override fun GetPersonas(name: String){
        model.GetPersonas(name)
    }
    //VIEW
    override fun ErrorPersonas(resp: String){
        view.ErrorPersonas(resp)
    }

    override fun SuccessPersonas(response: ResponsePersonasByName){
        view.SuccessPersonas(response)
    }
}