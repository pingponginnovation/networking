package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfilePonenteOrder(

    @field:SerializedName("ponente_id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("apellidos")
    val apellidos: String? = null,

    @field:SerializedName("genero")
    val genre: String? = null,

    @field:SerializedName("fecha_nacimiento")
    val date_birth: String? = null,

    @field:SerializedName("correo")
    val email: String? = null,

    @field:SerializedName("celular")
    val cel: String? = null,

    @field:SerializedName("direccion")
    val address: String? = null,

    @field:SerializedName("empresa")
    val empresa: String? = null,

    @field:SerializedName("puesto")
    val ocupation: String? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("sinopsis")
    val description: String? = null,

    @field:SerializedName("imagen_perfil")
    val img_perfil: String? = null,

    @SerializedName("confencia")
    val conferencia: ResponseProfilePonenteConferenciaOrder,

    @SerializedName("areas_de_experiencias")
    val experiencias: ArrayList<ResponseProfilePonenteExperienciaOrder>
)