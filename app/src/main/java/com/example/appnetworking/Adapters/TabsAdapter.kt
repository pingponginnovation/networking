package com.example.appnetworking.Adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.appnetworking.ContactosAndChats.ChatsFragment
import com.example.appnetworking.ContactosAndChats.ContactosFragment

class TabsAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int): FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    override fun getItem(position: Int): Fragment{
        when (position){
            0 -> {
                return ContactosFragment()
            }
            1 -> {
                return ChatsFragment()
            }
            else->{throw IllegalStateException("$position is illegal") }
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }
}