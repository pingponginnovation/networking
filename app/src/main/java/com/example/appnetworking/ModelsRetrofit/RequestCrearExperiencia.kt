package com.example.appnetworking

data class RequestCrearExperiencia(
    val perfil_id: Int,
    val cargo: String,
    val tipo_empleo: Int,
    val empresa_flag: String,
    val empresa_id: Int,
    val empresa: String,
    val direccion: String,
    val actualmente: String,
    val fecha_inicio: String,
    val fecha_fin: String,
    val descripcion: String
)