package com.example.appnetworking

data class RequestEditBiography(
    val perfil_id: Int,
    val descripcion: String
)