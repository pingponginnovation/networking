package com.example.appnetworking.Menu

import com.example.appnetworking.ModelsRetrofit.ResponseMenuOrder
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePerfilContacto

interface MenuInterface{

    interface View{
        fun GetMenu()
        fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String)
    }

    interface Presenter{
        fun GetMenu()
        fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String)
    }

    interface Model{
        fun GetMenu()
        fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String)
    }
}