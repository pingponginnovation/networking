package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileJornadasOrder(

    @field:SerializedName("tipo_empleo_id")
    val id: Int? = null,

    @field:SerializedName("tipo_empleo")
    val tipo_jornada: String? = null
)