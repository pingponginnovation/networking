package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.Profile.ProfileSpeakerView
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_speakers.view.*

class SpeakersAdapter: RecyclerView.Adapter<SpeakersAdapter.ViewHolder>(){

    var list_: List<ResponseSpeakersOrder>? = null
    lateinit var contexto: Context

    fun SpeakersAdapter(lista: List<ResponseSpeakersOrder>, context: Context){
        list_ = lista
        this.contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val name = holder.itemView.findViewById(R.id.txt_name) as TextView
        val puesto = holder.itemView.findViewById(R.id.txt_puesto) as TextView
        val evento = holder.itemView.findViewById(R.id.txt_evento) as TextView
        val lugar = holder.itemView.findViewById(R.id.txt_place) as TextView
        val datetime = holder.itemView.findViewById(R.id.txt_date_time) as TextView

        name.text = list_!![position].name
        puesto.text = list_!![position].ocupation
        evento.text = list_!![position].conferencia.name
        lugar.text = list_!![position].conferencia.place
        datetime.text = "${list_!![position].conferencia.fecha}"
        Picasso.get().load(list_!![position].img_perfil).into(holder.itemView.img_speakers)

        holder.itemView.setOnClickListener{view ->
            val intent = Intent(contexto, ProfileSpeakerView::class.java)
            intent.putExtra("idponente", list_!![position].id.toString())
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_speakers, parent, false))
    }

    override fun getItemCount(): Int{
        return list_!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}