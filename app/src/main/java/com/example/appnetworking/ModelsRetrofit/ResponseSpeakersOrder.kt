package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseSpeakersOrder(

    @field:SerializedName("perfil_id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("apellidos")
    val apellidos: String? = null,

    @field:SerializedName("genero")
    val genre: String? = null,

    @field:SerializedName("direccion")
    val address: String? = null,

    @field:SerializedName("empresa")
    val empresa: String? = null,

    @field:SerializedName("puesto")
    val ocupation: String? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("imagen_perfil")
    val img_perfil: String? = null,

    @SerializedName("experiencia")
    val experiencia: ResponseSpeakerExperienciaOrder,

    @SerializedName("conferencia")
    val conferencia: ResponseSpeakerConferenciaOrder
)