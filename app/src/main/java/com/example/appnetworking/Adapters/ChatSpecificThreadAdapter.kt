package com.example.appnetworking.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ContactoChat.ContactoChatView
import com.example.appnetworking.ContactosAndChats.ChatsFragment
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import java.util.*

class ChatSpecificThreadAdapter: RecyclerView.Adapter<ChatSpecificThreadAdapter.ViewHolder>(), Filterable{

    var list: ArrayList<ContactoChatView.ThreadChat>? = null
    private var listFiltered: ArrayList<ContactoChatView.ThreadChat>? = null
    lateinit var contexto: Context
    private val SELF = 100
    private var my_id_profile = -1

    fun ChatSpecificThreadAdapter(lista: ArrayList<ContactoChatView.ThreadChat>, context: Context, id: Int){
        list = lista
        listFiltered = lista
        my_id_profile = id
        contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val msj = holder.itemView.findViewById(R.id.message) as TextView
        //val timestamp = holder.itemView.findViewById(R.id.timestamp) as TextView

        msj.text = listFiltered!![position].mensaje
        //timestamp.text = listFiltered!![position].date
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View
        if (viewType == SELF) itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_self, parent, false)
        else itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_other, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemViewType(position: Int): Int {
        val chat: ContactoChatView.ThreadChat = listFiltered!!.get(position)
        if(chat.id_perfil == my_id_profile) return SELF
        else return position
    }

    override fun getItemCount(): Int{
        return listFiltered!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {

                val charString = charSequence.toString()

                if (charString.isEmpty()) listFiltered = list
                else{

                    val filteredList = ArrayList<ContactoChatView.ThreadChat>()
                    for (row in list!!) {

                        if (row.mensaje.toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row)
                    }

                    listFiltered = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                listFiltered = filterResults.values as ArrayList<ContactoChatView.ThreadChat>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}