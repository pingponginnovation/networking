package com.example.appnetworking.Profile

import android.content.Context
import com.example.appgenesys.*
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestCrearExperiencia
import com.example.appnetworking.RequestEditBiography
import com.example.appnetworking.RequestModificarExperiencia

class ProfileModel(viewPresenter: Profile.Presenter, ctx: Context): Profile.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetProfile(){
        val retrofit = RetrofitController(context!!)
        retrofit.Profile(this)
    }

    override fun CrearExperiencia(req: RequestCrearExperiencia){
        val retrofit = RetrofitController(context!!)
        retrofit.CreateExperience(this, req)
    }

    override fun EditExperience(req: RequestModificarExperiencia){
        val retrofit = RetrofitController(context!!)
        retrofit.EditExperiencia(this, req)
    }

    override fun EditBiography(req: RequestEditBiography){
        val retrofit = RetrofitController(context!!)
        retrofit.EditBiography(this, req)
    }

    override fun DeleteInteres(req: RequestDeleteInteres){
        val retrofit = RetrofitController(context!!)
        retrofit.DeleteInteres(this, req)
    }

    override fun CreateInteres(req: RequestCrearInteres){
        val retrofit = RetrofitController(context!!)
        retrofit.CrearInteres(this, req)
    }

    override fun CreateSkill(req: RequestCrearSkill){
        val retrofit = RetrofitController(context!!)
        retrofit.CrearSkill(this, req)
    }

    override fun DeleteSkill(req: RequestDeleteSkill){
        val retrofit = RetrofitController(context!!)
        retrofit.DeleteSkill(this, req)
    }

    override fun ErrorProfile(resp: String){
        presenter.ErrorProfile(resp)
    }

    override fun SuccessProfile(response: ResponseProfile){
        presenter.SuccessProfile(response)
    }

    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia){
        presenter.SuccessCrearExperiencia(response)
    }

    override fun ErrorCrearExperiencia(msj: String){
        presenter.ErrorCrearExperiencia(msj)
    }

    override fun SucccessEdit(resp: ResponseEditExperiencia){
        presenter.SucccessEdit(resp)
    }

    override fun ErrorEdit(msj: String){
        presenter.ErrorEdit(msj)
    }

    override fun SuccessEditBiography(response: ResponseEditBiography){
        presenter.SuccessEditBiography(response)
    }

    override fun ErrorEditBiography(resp: String){
        presenter.ErrorEditBiography(resp)
    }

    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres){
        presenter.SuccessDeleteInteres(resp)
    }

    override fun ErrorDeleteInteres(resp: String){
        presenter.ErrorDeleteInteres(resp)
    }

    override fun SuccessInterest(response: ResponseCrearInteres){
        presenter.SuccessInterest(response)
    }

    override fun ErrorInteres(resp: String){
        presenter.ErrorInteres(resp)
    }

    override fun SuccessSkill(response: ResponseCrearSkill){
        presenter.SuccessSkill(response)
    }

    override fun ErrorSkill(resp: String){
        presenter.ErrorSkill(resp)
    }

    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill){
        presenter.SuccessDeleteSkill(resp)
    }

    override fun ErrorDeleteSkill(resp: String){
        presenter.ErrorDeleteSkill(resp)
    }
}