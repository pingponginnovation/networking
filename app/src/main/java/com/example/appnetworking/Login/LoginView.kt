package com.example.appnetworking.Login

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.appgenesys.Login.LoginPresenter
import com.example.appgenesys.Retrofit.Login
import com.example.appnetworking.CoreActivity
import com.example.appnetworking.CoreGenius
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class LoginView: AppCompatActivity(), Login.View{

    private lateinit var presenter: Login.Presenter
    var preferences: PreferencesController? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        preferences = PreferencesController(applicationContext)
        presenter = LoginPresenter(this, applicationContext, preferences!!)

        switch_url.setOnClickListener{
            if(switch_url.isChecked) switch_url.setText("Production")
            else switch_url.text = "Testing"
        }

        CheckSession()

        btn_signin.setOnClickListener{
            if(txt_email.text.isNullOrEmpty() || txt_pass.text.isNullOrEmpty()) toast("Complete all fields")
            else MakeLogin()
        }
    }

    fun CheckSession(){
        if(!preferences!!.getSession().isNullOrEmpty()){
            startActivity<CoreActivity>()
            finish()
        }
    }

    override fun MakeLogin(){
        progressBar.indeterminateDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        progressBar.visibility = View.VISIBLE
        preferences!!.SaveURLtype(switch_url.text.toString())
        presenter.MakeLogin(txt_email.text.toString(), txt_pass.text.toString())
    }

    override fun SuccessLogin(name: String){
        val intent = Intent(this, CoreGenius::class.java)
        intent.putExtra("NAME", name)
        startActivity(intent)
        finish()
    }

    override fun ErrorLogin(resp: String){
        progressBar.stopNestedScroll()
        progressBar.visibility = View.GONE
        toast(resp)
    }

    override fun ErrorServer(msj: String) {
        progressBar.stopNestedScroll()
        progressBar.visibility = View.GONE
        toast(msj)
    }
}
