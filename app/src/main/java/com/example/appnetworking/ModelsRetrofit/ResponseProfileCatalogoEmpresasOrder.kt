package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileCatalogoEmpresasOrder(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("empresa")
    val empresa: String? = null,

    @field:SerializedName("logo_empresa")
    val logoempresa: String? = null
)