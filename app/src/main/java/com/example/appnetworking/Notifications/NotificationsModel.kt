package com.example.appnetworking.Notifications

import android.content.Context
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseNotificaciones

class NotificationsModel(viewPresenter: Notifications.Presenter, ctx: Context): Notifications.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetNotifications(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetNotifications(this)
    }

    override fun ErrorNotifications(resp: String){
        presenter.ErrorNotifications(resp)
    }

    override fun SuccessNotifications(response: ResponseNotificaciones){
        presenter.SuccessNotifications(response)
    }
}