package com.example.appnetworking.Profile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.ExperiencesAdapter
import com.example.appnetworking.Profile.ProfileView.Companion.responseCatalogoEmpresas
import com.example.appnetworking.Profile.ProfileView.Companion.responseExperiences
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.activity_live_video_broadcaster.view.*
import kotlinx.android.synthetic.main.edit_experience_profile.*
import kotlinx.android.synthetic.main.search_company.*
import kotlinx.android.synthetic.main.show_experiences_toedit_profile.*
import kotlinx.android.synthetic.main.show_experiences_toedit_profile.back_button
import org.jetbrains.anko.toast

class SearchCompanyName: AppCompatActivity(){

    var adaptador_dropdownList: ArrayAdapter<String>? = null
    companion object{
        var name_company = ""
        var aux_catalogo = -1
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_company)

        autocomplete_company!!.addTextChangedListener(yourTextWatcher)
        adaptador_dropdownList = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)
        ConfigToolbar()
        SetUpSearchField()
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun SetUpSearchField(){

        if(!responseCatalogoEmpresas.isNullOrEmpty()){
            responseCatalogoEmpresas.forEach{
                adaptador_dropdownList!!.add(it.empresa)
            }
        }
        autocomplete_company!!.setThreshold(1)
        autocomplete_company!!.setAdapter(adaptador_dropdownList)
        autocomplete_company!!.setOnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                aux_catalogo = 0 //La empresa no está en el listado
                name_company = autocomplete_company!!.text.toString()
                finish()
            }
            true
        }
    }

    protected var yourTextWatcher: TextWatcher = object: TextWatcher{
        override fun afterTextChanged(s: Editable){}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int){}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int){

            if(autocomplete_company!!.isPerformingCompletion){
                aux_catalogo = 1 //Empresa que está en el listado de empresas
                name_company = autocomplete_company!!.text.toString()
                finish()
            }
        }
    }
}