package com.example.appnetworking

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.os.CountDownTimer
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.developer.kalert.KAlertDialog
import com.example.appnetworking.NewProject.NewProjectView
import com.example.appnetworking.Proyecto.EditProyectView
import kotlinx.android.synthetic.main.alertdialog_custom.view.*
import kotlinx.android.synthetic.main.alertdialog_custom.view.btn_closee
import kotlinx.android.synthetic.main.alertdialog_custom.view.txt_message
import kotlinx.android.synthetic.main.alertdialog_custom_expired.view.*
import java.text.SimpleDateFormat
import java.util.*

class ControllerAlertDialog(context: Context){

    var contex: Context? = null
    private lateinit var pDialog: KAlertDialog
    init {
        if(this.contex == null) this.contex = context
    }

    fun ShowAlert(msj: String){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(contex!!)
        var dialog: AlertDialog? = null
        val view = LayoutInflater.from(contex).inflate(R.layout.alertdialog_custom, null)

        view.txt_message.text = msj
        view.txt_message.setTextColor(Color.parseColor("#5D002F"))

        view.btn_closee.setOnClickListener{
            dialog!!.dismiss()
        }
        mBuilder.setView(view)
        dialog = mBuilder.create()
        dialog.show()

        StartRunnable(dialog)
    }

    fun StartRunnable(dialog: AlertDialog){
        object : CountDownTimer(3000, 3000){
            override fun onTick(millisUntilFinished: Long){
            }
            override fun onFinish(){
                dialog.dismiss()
            }
        }.start()
    }

    fun AlertSuccessEditProject(msj: String, actividad: EditProyectView){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(contex!!)
        var dialog: AlertDialog? = null
        val view = LayoutInflater.from(contex).inflate(R.layout.alertdialog_custom_expired, null)

        view.txt_message.text = msj
        view.txt_message.setTextColor(Color.parseColor("#5D002F"))

        view.btn_aceptar.setOnClickListener{
            dialog!!.dismiss()
            actividad.finish()
        }

        mBuilder.setView(view)
        dialog = mBuilder.create()
        dialog.setCancelable(false)
        dialog.show()
    }

    fun AlertSuccessCreatedProject(msj: String, actividad: NewProjectView){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(contex!!)
        var dialog: AlertDialog? = null
        val view = LayoutInflater.from(contex).inflate(R.layout.alertdialog_custom_expired, null)

        view.txt_message.text = msj
        view.txt_message.setTextColor(Color.parseColor("#5D002F"))

        view.btn_aceptar.setOnClickListener{
            dialog!!.dismiss()
            actividad.finish()
        }

        mBuilder.setView(view)
        dialog = mBuilder.create()
        dialog.setCancelable(false)
        dialog.show()
    }

    fun ProgressBar(){
        pDialog = KAlertDialog(contex, KAlertDialog.PROGRESS_TYPE)
        pDialog.progressHelper.barColor = Color.parseColor("#5D002F")
        pDialog.setCancelable(false)
        pDialog.setTitleText("Loading")
        pDialog.progressHelper.barWidth = 5
        pDialog.progressHelper.rimColor = Color.parseColor("#5D002F")
        pDialog.progressHelper.rimWidth = 2
        pDialog.progressHelper.spinSpeed = 1.5f
        pDialog.show()
    }

    fun StopProgress(){
        if(pDialog.progressHelper.isSpinning){
            pDialog.cancel()
            pDialog.dismissWithAnimation()
        }
    }
}