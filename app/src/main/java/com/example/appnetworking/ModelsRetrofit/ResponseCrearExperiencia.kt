package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseCrearExperiencia(

    @field:SerializedName("mensaje")
    val msj: String? = null
)