package com.example.appnetworking.Menu

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.ModelsRetrofit.ResponseMenuOrder

class MenuModel(viewPresenter: MenuInterface.Presenter, ctx: Context): MenuInterface.Model{

    private val presenter = viewPresenter
    private var context: Context? = null

    init {
        context = ctx
    }

    override fun GetMenu(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetMenu(this)
    }

    override fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String){
        presenter.SuccessMenu(resp, photo)
    }
}