package com.example.appnetworking.Genius

import android.content.Context
import com.example.appgenesys.ResponseAgenda
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseInfoGenius

class GeniusPresenter(viewMatch: Genius.View, ctx: Context): Genius.Presenter{

    private val view = viewMatch
    private val model = GeniusModel(this, ctx)

    //MODEL
    override fun GetInformacion(){
        model.GetInformacion()
    }

    //VIEW
    override fun ErrorGenius(resp: String) {
        view.ErrorGenius(resp)
    }

    override fun SuccessInfo(response: ResponseInfoGenius){
        view.SuccessInfo(response)
    }
}