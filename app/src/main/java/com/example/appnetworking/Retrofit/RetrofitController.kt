//package com.example.appnetworking
package com.example.appgenesys.Retrofit

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import com.example.appgenesys.*
import com.example.appgenesys.Login.Matches
import com.example.appnetworking.*
import com.example.appnetworking.ActivityProgram.ActivityProgram
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.AgendaEvento.AgendaEvento
import com.example.appnetworking.ContactoChat.ContactoChat
import com.example.appnetworking.ContactosAndChats.Contactos
import com.example.appnetworking.Genius.Genius
import com.example.appnetworking.Menu.MenuInterface
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.NewProject.NewProject
import com.example.appnetworking.Notifications.Notifications
import com.example.appnetworking.PersonasByIntereses.PersonasByIntereses
import com.example.appnetworking.PersonasByName.PersonasByName
import com.example.appnetworking.Ponentes.Ponentes
import com.example.appnetworking.Profile.PerfilContacto
import com.example.appnetworking.Profile.Profile
import com.example.appnetworking.Profile.ProfileSpeaker
import com.example.appnetworking.ProjectsGeneral.ProjectsGeneral
import com.example.appnetworking.Proyecto.Proyecto
import com.example.appnetworking.ProyectosLista.ProyectosLista
import com.example.appnetworking.RedireccionAgendaInvitacion.AgendaInvitacion
import com.example.appnetworking.RedireccionAgendaInvitacion.AgendaInvitacionView
import com.gitonway.lee.niftymodaldialogeffects.lib.NiftyDialogBuilder
import kotlinx.android.synthetic.main.alertdialog_custom.view.*
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.toast
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

class RetrofitController(context: Context){

    var URL_PRODUCTION = "http://networking.fasten.com.mx/api/"
    var URL_TESTING = "https://networkingtest.fasten.com.mx//api/"
    var api: Api? = null
    var preferences: PreferencesController? = null
    var context: Context? = null
    private lateinit var adapterRetrofit: Retrofit

    init {
        this.context = context
        preferences = PreferencesController(context)
        configurateRetrofit()
    }

    fun configurateRetrofit(){

        var url = ""
        if(preferences!!.getTypeURL().equals("Testing")) url = URL_TESTING
        else url = URL_PRODUCTION

        val httpClient = configurateClient()
        val client = httpClient.build()
        adapterRetrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        api = adapterRetrofit.create(Api::class.java)
    }

    private fun configurateClient(): OkHttpClient.Builder{
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor{chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .header("Content-Type", "application/json")
                .header("Accept", "application/json")
                .method(original.method(), original.body())
                .build()
            chain.proceed(request)
        }

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.writeTimeout(30, TimeUnit.SECONDS)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        return httpClient
    }

    fun AlertDialogShow(msj: String, close: Boolean, act: Context){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(act)
        var dialog: AlertDialog? = null
        val view = LayoutInflater.from(act).inflate(R.layout.alertdialog_custom, null)

        view.txt_message.text = msj
        view.txt_message.setTextColor(Color.parseColor("#5D002F"))

        view.btn_closee.setOnClickListener{
            dialog!!.dismiss()
        }

        mBuilder.setView(view)
        dialog = mBuilder.create()
        dialog.show()

        StartRunnable(dialog)
    }

    fun AlertDialogShowEdit(msj: String, close: Boolean, act: Activity){
        val dialogBuilder = NiftyDialogBuilder.getInstance(context)
        dialogBuilder
            .withMessage(msj)
            .withIcon(R.drawable.logo)
            .withDividerColor("#FFFFFF")
            .withDialogColor("#820A47")
            .withMessageColor("#FFFFFF")
            .withDuration(400)
            .isCancelableOnTouchOutside(false)
            .withButton1Text("Accept")
            .setButton1Click{
                if(close) act.finish()
            }
            .show()
    }

    fun StartRunnable(dialog: AlertDialog){
        object : CountDownTimer(3000, 3000){
            override fun onTick(millisUntilFinished: Long){
            }
            override fun onFinish(){
                dialog.dismiss()
            }
        }.start()
    }

    fun Login(model: Login.Model, user: String, password: String){

        val callLogin  = api!!.login(RequestLogin(user, password))
        callLogin.enqueue(object: Callback<ResponseLogin> {

            override fun onResponse(call: Call<ResponseLogin>, response: Response<ResponseLogin>){
                if (response.isSuccessful){
                    val respSuccess: ResponseLogin = response.body()!!
                    model.SuccessLogin(respSuccess)
                }
                else model.ErrorLogin("Credenciales inválidas")
            }
            override fun onFailure(call: Call<ResponseLogin>, t: Throwable){
                model.ErrorServer(t.message.toString())
            }
        })
    }

    fun Matches(model: Matches.Model, typeFiltro: Int, searches: String){

        val callMatches  = api!!.getMatch("Bearer ${preferences!!.getToken()}", RequestMatch(typeFiltro, searches))
        callMatches.enqueue(object: Callback<ResponseMatch> {

            override fun onResponse(call: Call<ResponseMatch>, response: Response<ResponseMatch>){

                if (response.isSuccessful){
                    val respSuccess: ResponseMatch = response.body()!!
                    model.SuccessMatches(respSuccess)
                }
                else model.ErrorMatches(response.message())
            }
            override fun onFailure(call: Call<ResponseMatch>, t: Throwable){
                Log.e("Server:", t.message!!)
                model.ErrorMatches(t.message.toString())
            }
        })
    }

    fun MatchesFilter(model: PersonasByIntereses.Model, typeFiltro: Int, searches: String){

        val callMatches = api!!.getMatch("Bearer ${preferences!!.getToken()}", RequestMatch(typeFiltro, searches))
        callMatches.enqueue(object: Callback<ResponseMatch> {

            override fun onResponse(call: Call<ResponseMatch>, response: Response<ResponseMatch>){

                if (response.isSuccessful){
                    val respSuccess: ResponseMatch = response.body()!!
                    model.SuccessMatchFiltro(respSuccess)
                }
                else model.ErrorMatches(response.message())
            }
            override fun onFailure(call: Call<ResponseMatch>, t: Throwable){
                Log.e("Server:", t.message!!)
                model.ErrorMatches(t.message.toString())
            }
        })
    }

    fun Profile(model: Profile.Model){

        val callLogin  = api!!.getMyProfile("Bearer ${preferences!!.getToken()}")
        callLogin.enqueue(object: Callback<ResponseProfile> {

            override fun onResponse(call: Call<ResponseProfile>, response: Response<ResponseProfile>){

                if (response.isSuccessful){

                    val respSuccess: ResponseProfile = response.body()!!
                    model.SuccessProfile(respSuccess)
                    //Log.e("Dentro:", respSuccess.toString())
                }
                else model.ErrorProfile(response.message())
            }

            override fun onFailure(call: Call<ResponseProfile>, t: Throwable){
                model.ErrorProfile(t.message.toString())
            }
        })
    }

    fun ListActivities(model: ActivityProgram.Model){

        val callLogin  = api!!.getActivities("Bearer ${preferences!!.getToken()}")
        callLogin.enqueue(object: Callback<ResponseActividades> {

            override fun onResponse(call: Call<ResponseActividades>, response: Response<ResponseActividades>){

                if (response.isSuccessful){

                    val respSuccess: ResponseActividades = response.body()!!
                    model.SuccessActivities(respSuccess)
                }
                else model.ErrorActivities(response.message())
            }
            override fun onFailure(call: Call<ResponseActividades>, t: Throwable){
                model.ErrorActivities(t.message.toString())
            }
        })
    }

    fun GetAgenda(model: Agenda.Model, idProfile: Int, dateSelected: String){

        val callLogin  = api!!.getAgenda("Bearer ${preferences!!.getToken()}",
            RequestAgenda(idProfile, dateSelected))
        callLogin.enqueue(object: Callback<ResponseAgenda> {

            override fun onResponse(call: Call<ResponseAgenda>, response: Response<ResponseAgenda>){

                if (response.isSuccessful){
                    val respSuccess: ResponseAgenda = response.body()!!
                    model.SuccessAgenda(respSuccess)
                }
                else model.ErrorAgenda(response.message())
            }
            override fun onFailure(call: Call<ResponseAgenda>, t: Throwable){
                model.ErrorAgenda(t.message.toString())
            }
        })
    }

    fun GetSpeakers(model: Ponentes.Model){

        val callLogin  = api!!.getSpeakers("Bearer ${preferences!!.getToken()}")
        callLogin.enqueue(object: Callback<ResponseSpeakers> {

            override fun onResponse(call: Call<ResponseSpeakers>, response: Response<ResponseSpeakers>){

                if (response.isSuccessful){
                    val respSuccess: ResponseSpeakers = response.body()!!
                    //Log.e("datos", respSuccess.toString())
                    model.SuccessPonentes(respSuccess)
                }
                else model.ErrorPonentes(response.message())
            }
            override fun onFailure(call: Call<ResponseSpeakers>, t: Throwable){
                model.ErrorPonentes(t.message.toString())
            }
        })
    }

    data class RequestPonente(val ponente_id: Int)
    fun GetPonente(model: ProfileSpeaker.Model, id: Int){

        val callLogin  = api!!.getProfilePonente("Bearer ${preferences!!.getToken()}", RequestPonente(id))
        callLogin.enqueue(object: Callback<ResponseProfilePonente> {

            override fun onResponse(call: Call<ResponseProfilePonente>, response: Response<ResponseProfilePonente>){

                if (response.isSuccessful){
                    val respSuccess: ResponseProfilePonente = response.body()!!
                    model.SuccessProfile(respSuccess)
                }
                else model.ErrorProfile(response.message())
            }
            override fun onFailure(call: Call<ResponseProfilePonente>, t: Throwable){
                model.ErrorProfile(t.message.toString())
            }
        })
    }

    fun SearchContacto(model: Matches.Model, id: Int){

        val callLogin  = api!!.getProfileContacto("Bearer ${preferences!!.getToken()}", RequestPerfilContactoBuscado(id))
        callLogin.enqueue(object: Callback<ResponsePerfilContacto> {

            override fun onResponse(call: Call<ResponsePerfilContacto>, response: Response<ResponsePerfilContacto>){

                if (response.isSuccessful){
                    val respSuccess: ResponsePerfilContacto = response.body()!!
                    model.ContactoSuccess(respSuccess)
                }
                else AlertDialogShow(response.message(), false, context!!)
            }
            override fun onFailure(call: Call<ResponsePerfilContacto>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetProyectos(model: ProyectosLista.Model, id: Int){

        val callLogin  = api!!.getProyectos("Bearer ${preferences!!.getToken()}", RequestProyectos(id))
        callLogin.enqueue(object: Callback<ResponseProyectos> {

            override fun onResponse(call: Call<ResponseProyectos>, response: Response<ResponseProyectos>){

                if (response.isSuccessful){
                    val respSuccess: ResponseProyectos = response.body()!!
                    model.SuccessProyectos(respSuccess)
                }
                else model.ErrorProyectos(response.message())
            }
            override fun onFailure(call: Call<ResponseProyectos>, t: Throwable){
                model.ErrorProyectos(t.message.toString())
            }
        })
    }

    fun VotaProyecto(model: Proyecto.Model, req: RequestVotarProyecto){

        val callLogin  = api!!.votaProyecto("Bearer ${preferences!!.getToken()}", req)
        callLogin.enqueue(object: Callback<ResponseVotarProyecto> {

            override fun onResponse(call: Call<ResponseVotarProyecto>, response: Response<ResponseVotarProyecto>){

                if (response.isSuccessful){
                    val respSuccess: ResponseVotarProyecto = response.body()!!
                    model.SuccessVotacion(respSuccess)
                }
                else model.ErrorVotacion(response.message())
            }
            override fun onFailure(call: Call<ResponseVotarProyecto>, t: Throwable){
                model.ErrorVotacion(t.message.toString())
            }
        })
    }

    fun SaveToken(context: Context, token: String, os: String){

        val callLogin  = api!!.saveToken("Bearer ${preferences!!.getToken()}", RequestSaveToken(token, os))

        callLogin.enqueue(object: Callback<ResponseSaveToken> {

            override fun onResponse(call: Call<ResponseSaveToken>, response: Response<ResponseSaveToken>){

                if (response.isSuccessful){
                    val respSuccess: ResponseSaveToken = response.body()!!
                    //context.toast("Token guardado")
                }
                else AlertDialogShow(response.message(), false, context)
            }
            override fun onFailure(call: Call<ResponseSaveToken>, t: Throwable){
                context.toast(t.message.toString())
            }
        })
    }

    fun Invitacion(context: Context, id_destino: String, tipoInvitacion: String){

        val callLogin  = api!!.respuestaInvitacion("Bearer ${preferences!!.getToken()}",
            RequestToInvitacion(id_destino, tipoInvitacion))

        callLogin.enqueue(object: Callback<ResponseInvitacion> {
            override fun onResponse(call: Call<ResponseInvitacion>, response: Response<ResponseInvitacion>){

                if (response.isSuccessful){
                    val respSuccess: ResponseInvitacion = response.body()!!
                    AlertDialogShow(respSuccess.msj, false, context)
                }
                else AlertDialogShow(response.message(), false, context)
            }
            override fun onFailure(call: Call<ResponseInvitacion>, t: Throwable){
                context.toast(t.message.toString())
            }
        })
    }

    fun SearchContactoNotification(model: PerfilContacto.Model, id: Int){

        Log.e("id", id.toString())

        val callLogin  = api!!.getProfileContacto("Bearer ${preferences!!.getToken()}", RequestPerfilContactoBuscado(id))
        callLogin.enqueue(object: Callback<ResponsePerfilContacto> {

            override fun onResponse(call: Call<ResponsePerfilContacto>, response: Response<ResponsePerfilContacto>){

                if (response.isSuccessful){
                    val respSuccess: ResponsePerfilContacto = response.body()!!
                    model.SuccessPerfil(respSuccess)
                }
                else{
                    Log.e("Error:", response.message())
                    context!!.toast(response.message())
                }
            }
            override fun onFailure(call: Call<ResponsePerfilContacto>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun SetLikeDislike(id: String, opcion: String){

        val callLogin  = api!!.setLikeDislike("Bearer ${preferences!!.getToken()}",
            RequestLikeDislike(id, opcion))

        callLogin.enqueue(object: Callback<ResponseLikeDislike> {

            override fun onResponse(call: Call<ResponseLikeDislike>, response: Response<ResponseLikeDislike>){

                if (response.isSuccessful){
                    val respSuccess: ResponseLikeDislike = response.body()!!
                    AlertDialogShow(respSuccess.msj!!, false, context!!)
                }
                else AlertDialogShow(response.message(), false, context!!)
            }
            override fun onFailure(call: Call<ResponseLikeDislike>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetInfoGenius(model: Genius.Model){

        val callLogin  = api!!.getInfoGenius("Bearer ${preferences!!.getToken()}")

        callLogin.enqueue(object: Callback<ResponseInfoGenius> {

            override fun onResponse(call: Call<ResponseInfoGenius>, response: Response<ResponseInfoGenius>){

                if (response.isSuccessful){
                    val respSuccess: ResponseInfoGenius = response.body()!!
                    model.SuccessInfo(respSuccess)
                }
                else model.ErrorGenius(response.toString())
            }
            override fun onFailure(call: Call<ResponseInfoGenius>, t: Throwable){
                model.ErrorGenius(t.message.toString())
            }
        })
    }

    fun EditProfile(id: Int, name: String, apaterno: String, act: Activity, corre: String,
    celular: String, direccion: String, web: String, empresa: String, puesto: String, sinopsis: String,
    img: String, flag: Int){

        val callLogin  = api!!.editProfile("Bearer ${preferences!!.getToken()}",
            RequestEditProfile(id, name, apaterno, corre, celular, direccion, web, empresa, puesto, sinopsis,
            img, flag))

        callLogin.enqueue(object: Callback<ResponseEditProfile> {
            override fun onResponse(call: Call<ResponseEditProfile>, response: Response<ResponseEditProfile>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEditProfile = response.body()!!
                    AlertDialogShowEdit("Profile updated successfully", true, act)
                }
                else AlertDialogShow(response.message(), false, act)
            }
            override fun onFailure(call: Call<ResponseEditProfile>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetContacts(model: Agenda.Model){

        val callLogin  = api!!.getContacts("Bearer ${preferences!!.getToken()}")

        callLogin.enqueue(object: Callback<ResponseContacts> {

            override fun onResponse(call: Call<ResponseContacts>, response: Response<ResponseContacts>){

                if (response.isSuccessful){
                    val respSuccess: ResponseContacts = response.body()!!
                    model.SuccessContacts(respSuccess)
                }
                else model.ErrorContacts(response.toString())
            }
            override fun onFailure(call: Call<ResponseContacts>, t: Throwable){
                model.ErrorContacts(t.message.toString())
            }
        })
    }

    fun CreateEvent(model: Agenda.Model, request: RequestNewEvent){

        val callLogin  = api!!.createEvent("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseNewEvent>{
            override fun onResponse(call: Call<ResponseNewEvent>, response: Response<ResponseNewEvent>){

                if (response.isSuccessful){
                    val respSuccess: ResponseNewEvent = response.body()!!
                    model.SuccessEventCreated()
                }
                else context!!.toast(response.message())
            }
            override fun onFailure(call: Call<ResponseNewEvent>, t: Throwable){
                context!!.toast(t.message.toString() + "aquiii")
            }
        })
    }

    fun EditCita(request: RequestEditCita){

        val callLogin  = api!!.editCita("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseEditCita> {
            override fun onResponse(call: Call<ResponseEditCita>, response: Response<ResponseEditCita>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEditCita = response.body()!!
                    AlertDialogShow(respSuccess.msj, false, context!!)
                }
                else AlertDialogShow(response.message(), false, context!!)
            }
            override fun onFailure(call: Call<ResponseEditCita>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetMenu(model: MenuInterface.Model){

        val callLogin  = api!!.getMenu("Bearer ${preferences!!.getToken()}")
        callLogin.enqueue(object: Callback<ResponseMenu> {
            override fun onResponse(call: Call<ResponseMenu>, response: Response<ResponseMenu>){

                if (response.isSuccessful){
                    val respSuccess: ResponseMenu = response.body()!!
                    model.SuccessMenu(respSuccess.data, respSuccess.photo!!)
                }
                else context!!.toast(response.message())
            }
            override fun onFailure(call: Call<ResponseMenu>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetListaContacts(model: Contactos.Model){

        val callLogin  = api!!.getContacts("Bearer ${preferences!!.getToken()}")

        callLogin.enqueue(object: Callback<ResponseContacts> {

            override fun onResponse(call: Call<ResponseContacts>, response: Response<ResponseContacts>){

                if (response.isSuccessful){
                    val respSuccess: ResponseContacts = response.body()!!
                    model.SuccessContacts(respSuccess)
                }
                else model.ErrorContacts(response.toString())
            }
            override fun onFailure(call: Call<ResponseContacts>, t: Throwable){
                model.ErrorContacts(t.message.toString())
            }
        })
    }

    fun FindPersonas(model: PersonasByName.Model, name: String){

        val callLogin = api!!.getPersonas("Bearer ${preferences!!.getToken()}",
            RequestPersonasByName(name))

        callLogin.enqueue(object: Callback<ResponsePersonasByName> {

            override fun onResponse(call: Call<ResponsePersonasByName>, response: Response<ResponsePersonasByName>){

                if (response.isSuccessful){
                    val respSuccess: ResponsePersonasByName = response.body()!!
                    model.SuccessPersonas(respSuccess)
                }
                else{

                    val error: ResponsePersonasByNameError
                    val converter: Converter<ResponseBody, ResponsePersonasByNameError> = adapterRetrofit.
                        responseBodyConverter(ResponsePersonasByNameError::class.java,
                            arrayOfNulls<Annotation>(0))

                    error = try{
                        converter.convert(response.errorBody()!!)
                    }catch(e: IOException){}!! as ResponsePersonasByNameError

                    model.ErrorPersonas(error.msjError!!)
                }
            }
            override fun onFailure(call: Call<ResponsePersonasByName>, t: Throwable){
                model.ErrorPersonas(t.message.toString())
            }
        })
    }

    fun GetNotifications(model: Notifications.Model){

        val callLogin = api!!.getNotifications("Bearer ${preferences!!.getToken()}")
        callLogin.enqueue(object: Callback<ResponseNotificaciones> {

            override fun onResponse(call: Call<ResponseNotificaciones>, response: Response<ResponseNotificaciones>){

                if (response.isSuccessful){
                    val respSuccess: ResponseNotificaciones = response.body()!!
                    model.SuccessNotifications(respSuccess)
                }
                else{

                    /*val error: ResponsePersonasByNameError
                    val converter: Converter<ResponseBody, ResponsePersonasByNameError> = adapterRetrofit.
                    responseBodyConverter(ResponsePersonasByNameError::class.java,
                        arrayOfNulls<Annotation>(0))

                    error = try{
                        converter.convert(response.errorBody()!!)
                    }catch(e: IOException){}!! as ResponsePersonasByNameError*/

                    model.ErrorNotifications(response.message())
                }
            }
            override fun onFailure(call: Call<ResponseNotificaciones>, t: Throwable){
                model.ErrorNotifications(t.message.toString())
            }
        })
    }

    fun GetDateAndTime(model: Agenda.Model){

        val callLogin = api!!.getDateTime()
        callLogin.enqueue(object: Callback<ResponseDateTimeOrder>{
            override fun onResponse(call: Call<ResponseDateTimeOrder>, response: Response<ResponseDateTimeOrder>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDateTimeOrder = response.body()!!
                    model.SuccessDateTime(respSuccess)
                }
                else{

                    /*val error: ResponsePersonasByNameError
                    val converter: Converter<ResponseBody, ResponsePersonasByNameError> = adapterRetrofit.
                    responseBodyConverter(ResponsePersonasByNameError::class.java,
                        arrayOfNulls<Annotation>(0))

                    error = try{
                        converter.convert(response.errorBody()!!)
                    }catch(e: IOException){}!! as ResponsePersonasByNameError*/

                    model.ErrorDateTime(response.message())
                }
            }
            override fun onFailure(call: Call<ResponseDateTimeOrder>, t: Throwable){
                model.ErrorDateTime(t.message.toString())
            }
        })
    }

    fun UpdateNotification(request: RequestUpdateNotification){

        val callLogin  = api!!.updateNotification("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseUpdateNotification> {
            override fun onResponse(call: Call<ResponseUpdateNotification>, response: Response<ResponseUpdateNotification>){

                if (response.isSuccessful){
                    val respSuccess: ResponseUpdateNotification = response.body()!!
                    context!!.toast("Notificacion vista")
                    //notificationAct.UpdateAdapterNotifications(respSuccess.idNotificationUpdated!!)
                }
                else context!!.toast(response.message())
            }
            override fun onFailure(call: Call<ResponseUpdateNotification>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetDatosCita(model: AgendaInvitacion.Model, request: RequestDatosCita){

        val callLogin  = api!!.datosCitaInvitacion("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseDatosCita> {
            override fun onResponse(call: Call<ResponseDatosCita>, response: Response<ResponseDatosCita>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDatosCita = response.body()!!
                    model.SuccessInfo(respSuccess)
                }
                else model.ErrorCita(response.message())
            }
            override fun onFailure(call: Call<ResponseDatosCita>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetDatosCita(model: AgendaEvento.Model, request: RequestDatosCita){

        val callLogin  = api!!.datosCitaInvitacion("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseDatosCita> {
            override fun onResponse(call: Call<ResponseDatosCita>, response: Response<ResponseDatosCita>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDatosCita = response.body()!!
                    model.SuccessInfo(respSuccess)
                }
                else model.ErrorCita(response.message())
            }
            override fun onFailure(call: Call<ResponseDatosCita>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun ResponderCita(request: RequestRespuestaCita, myView: AgendaInvitacionView){

        val callLogin  = api!!.responderCita("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseRespuestaCita> {
            override fun onResponse(call: Call<ResponseRespuestaCita>, response: Response<ResponseRespuestaCita>){

                if (response.isSuccessful){
                    val respSuccess: ResponseRespuestaCita = response.body()!!
                    myView.UpdateView()
                    context!!.toast(respSuccess.msj!!)
                }
                else{
                    myView.UpdateView()
                    context!!.toast(response.message())
                }
            }
            override fun onFailure(call: Call<ResponseRespuestaCita>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetMessagesChat(model: ContactoChat.Model, request: RequestChatThread){

        val callLogin  = api!!.getMensajesChat("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseChatThread> {
            override fun onResponse(call: Call<ResponseChatThread>, response: Response<ResponseChatThread>){

                if (response.isSuccessful){
                    val respSuccess: ResponseChatThread = response.body()!!
                    model.SuccessMensajes(respSuccess)
                }
                else model.ErrorMensajes(response.message())
            }
            override fun onFailure(call: Call<ResponseChatThread>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun SendMessage(model: ContactoChat.Model, request: RequestEnviarMensaje){

        val callLogin  = api!!.sendMessage("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseEnviarMensaje> {
            override fun onResponse(call: Call<ResponseEnviarMensaje>, response: Response<ResponseEnviarMensaje>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEnviarMensaje = response.body()!!
                    model.SuccessEnviarMensaje(respSuccess)
                }
                else model.ErrorEnviarMensaje(response.message())
            }
            override fun onFailure(call: Call<ResponseEnviarMensaje>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun GetAllProjects(model: ProjectsGeneral.Model, request: RequestProyectosGeneral){

        val callLogin  = api!!.getAllProjects("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseProyectosGeneral> {
            override fun onResponse(call: Call<ResponseProyectosGeneral>, response: Response<ResponseProyectosGeneral>){

                if (response.isSuccessful){
                    val respSuccess: ResponseProyectosGeneral = response.body()!!
                    model.SuccessProyectos(respSuccess)
                }
                else model.ErrorProyectos(response.message())
            }
            override fun onFailure(call: Call<ResponseProyectosGeneral>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun CreateProject(model: NewProject.Model, request: RequestCrearProyecto){

        val callLogin  = api!!.crearProyecto("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseCrearProyecto>{

            override fun onResponse(call: Call<ResponseCrearProyecto>, response: Response<ResponseCrearProyecto>){

                if (response.isSuccessful){
                    val respSuccess: ResponseCrearProyecto = response.body()!!
                    model.SuccessProyecto(respSuccess)
                }
                else model.ErrorProyecto(response.message())
            }
            override fun onFailure(call: Call<ResponseCrearProyecto>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun SuscribeConference(model: ProfileSpeaker.Model, request: RequestSuscribeConference){

        val callLogin  = api!!.suscribeConference("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseSuscribeConference>{

            override fun onResponse(call: Call<ResponseSuscribeConference>, response: Response<ResponseSuscribeConference>){

                if (response.isSuccessful){
                    val respSuccess: ResponseSuscribeConference = response.body()!!
                    model.SuccessSuscribeConference(respSuccess)
                }
                else model.ErrorSuscribe(response.message())
            }
            override fun onFailure(call: Call<ResponseSuscribeConference>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun SubscribeProgram(model: ActivityProgram.Model, request: RequestSuscribeConference){

        val callLogin  = api!!.suscribeConference("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseSuscribeConference>{

            override fun onResponse(call: Call<ResponseSuscribeConference>, response: Response<ResponseSuscribeConference>){

                if (response.isSuccessful){
                    val respSuccess: ResponseSuscribeConference = response.body()!!
                    model.SuccessSubscribed(respSuccess)
                }
                else model.ErrorSubscribed(response.message())
            }
            override fun onFailure(call: Call<ResponseSuscribeConference>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun CreateExperience(model: Profile.Model, request: RequestCrearExperiencia){

        Log.e("request", request.toString())

        val callLogin  = api!!.crearExperiencia("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseCrearExperiencia>{

            override fun onResponse(call: Call<ResponseCrearExperiencia>, response: Response<ResponseCrearExperiencia>){

                if (response.isSuccessful){
                    val respSuccess: ResponseCrearExperiencia = response.body()!!
                    model.SuccessCrearExperiencia(respSuccess)
                }
                else model.ErrorCrearExperiencia(response.message())
            }
            override fun onFailure(call: Call<ResponseCrearExperiencia>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun EditExperiencia(model: Profile.Model, request: RequestModificarExperiencia){

        val callLogin  = api!!.editExperiencia("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseEditExperiencia>{

            override fun onResponse(call: Call<ResponseEditExperiencia>, response: Response<ResponseEditExperiencia>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEditExperiencia = response.body()!!
                    model.SucccessEdit(respSuccess)
                }
                else model.ErrorEdit(response.message())
            }
            override fun onFailure(call: Call<ResponseEditExperiencia>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun EditProyecto(model: Proyecto.Model, request: RequestModificarProyecto){

        val callLogin  = api!!.editProyecto("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseModificarProyecto>{

            override fun onResponse(call: Call<ResponseModificarProyecto>, response: Response<ResponseModificarProyecto>){

                if (response.isSuccessful){
                    val respSuccess: ResponseModificarProyecto = response.body()!!
                    model.SuccessEdit(respSuccess)
                }
                else model.ErrorEdit(response.message())
            }
            override fun onFailure(call: Call<ResponseModificarProyecto>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun EditBiography(model: Profile.Model, request: RequestEditBiography){

        val callLogin  = api!!.editBiography("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseEditBiography>{

            override fun onResponse(call: Call<ResponseEditBiography>, response: Response<ResponseEditBiography>){

                if (response.isSuccessful){
                    val respSuccess: ResponseEditBiography = response.body()!!
                    model.SuccessEditBiography(respSuccess)
                }
                else model.ErrorEditBiography(response.message())
            }
            override fun onFailure(call: Call<ResponseEditBiography>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun DeleteInteres(model: Profile.Model, request: RequestDeleteInteres){

        val callLogin  = api!!.deleteInteres("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseDeleteInteres>{

            override fun onResponse(call: Call<ResponseDeleteInteres>, response: Response<ResponseDeleteInteres>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDeleteInteres = response.body()!!
                    model.SuccessDeleteInteres(respSuccess)
                }
                else model.ErrorDeleteInteres(response.message())
            }
            override fun onFailure(call: Call<ResponseDeleteInteres>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun CrearInteres(model: Profile.Model, request: RequestCrearInteres){

        val callLogin  = api!!.crearInteres("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseCrearInteres>{

            override fun onResponse(call: Call<ResponseCrearInteres>, response: Response<ResponseCrearInteres>){

                if (response.isSuccessful){
                    val respSuccess: ResponseCrearInteres = response.body()!!
                    model.SuccessInterest(respSuccess)
                }
                else model.ErrorInteres(response.message())
            }
            override fun onFailure(call: Call<ResponseCrearInteres>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun CrearSkill(model: Profile.Model, request: RequestCrearSkill){

        val callLogin  = api!!.crearHabilidad("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseCrearSkill>{

            override fun onResponse(call: Call<ResponseCrearSkill>, response: Response<ResponseCrearSkill>){

                if (response.isSuccessful){
                    val respSuccess: ResponseCrearSkill = response.body()!!
                    model.SuccessSkill(respSuccess)
                }
                else model.ErrorSkill(response.message())
            }
            override fun onFailure(call: Call<ResponseCrearSkill>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun DeleteSkill(model: Profile.Model, request: RequestDeleteSkill){

        val callLogin  = api!!.deleteSkill("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseDeleteSkill>{

            override fun onResponse(call: Call<ResponseDeleteSkill>, response: Response<ResponseDeleteSkill>){

                if (response.isSuccessful){
                    val respSuccess: ResponseDeleteSkill = response.body()!!
                    model.SuccessDeleteSkill(respSuccess)
                }
                else model.ErrorDeleteSkill(response.message())
            }
            override fun onFailure(call: Call<ResponseDeleteSkill>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }

    fun JoinStartMeeting(model: AgendaEvento.Model, request: RequestJoinStartMeeting){

        val callLogin  = api!!.StartOrJoinMeeting("Bearer ${preferences!!.getToken()}", request)
        callLogin.enqueue(object: Callback<ResponseJoinStartMeeting> {
            override fun onResponse(call: Call<ResponseJoinStartMeeting>, response: Response<ResponseJoinStartMeeting>){

                if (response.isSuccessful){
                    val respSuccess: ResponseJoinStartMeeting = response.body()!!
                    model.SuccessJoinStartMeeting(respSuccess)
                }
                else model.ErrorJoinStartMeeting(response.message())
            }
            override fun onFailure(call: Call<ResponseJoinStartMeeting>, t: Throwable){
                context!!.toast(t.message.toString())
            }
        })
    }
}