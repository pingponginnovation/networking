package com.example.appnetworking.RedireccionAgendaInvitacion

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseInfoGenius

class AgendaInvitacionModel(viewPresenter: AgendaInvitacion.Presenter, ctx: Context): AgendaInvitacion.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetInfoCita(request: RequestDatosCita){
        val retrofit = RetrofitController(context!!)
        retrofit.GetDatosCita(this, request)
    }

    override fun ErrorCita(resp: String){
        presenter.ErrorCita(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        presenter.SuccessInfo(response)
    }
}