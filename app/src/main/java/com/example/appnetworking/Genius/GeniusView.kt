package com.example.appnetworking.Genius

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.appnetworking.LiveStreaming.LiveVideoBroadcasterActivity
import com.example.appnetworking.R
import com.example.appnetworking.ResponseInfoGenius
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.genius_activity.*
import org.jetbrains.anko.toast

class GeniusView: AppCompatActivity(), Genius.View{

    private lateinit var presenter: Genius.Presenter

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.genius_activity)

        presenter = GeniusPresenter(this, applicationContext)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Information & services")

        presenter.GetInformacion()
        btn_broadcast.setOnClickListener{
            val i = Intent(this, LiveVideoBroadcasterActivity::class.java)
            startActivity(i)
        }
    }

    override fun ErrorGenius(resp: String) {
        toast(resp)
    }

    override fun SuccessInfo(response: ResponseInfoGenius){
        SetData(response)
    }

    fun SetData(datos: ResponseInfoGenius){
        Picasso.get().load(datos.data.logo).into(img_logo)
        txt_info_s.text = datos.data.sinopsis
        txt_cel_s.text = datos.data.cel
        txt_email_s.text = datos.data.email
        txt_web_s.text = datos.data.web

        LY_LOADER_GENIUS.visibility = View.GONE
        LY_GENIUS.visibility = View.VISIBLE
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}