package com.example.appnetworking.ProyectosLista

import android.content.Context
import com.example.appnetworking.ResponseProfilePonente
import com.example.appnetworking.ResponseProyectos

class ProyectosListaPresenter(viewMatch: ProyectosLista.View, ctx: Context): ProyectosLista.Presenter{

    private val view = viewMatch
    private val model = ProyectosListaModel(this, ctx)

    //MODEL
    override fun GetProyectos(idPonente: Int){
        model.GetProyectos(idPonente)
    }
    //VIEW
    override fun ErrorProyectos(resp: String){
        view.ErrorProyectos(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectos){
        view.SuccessProyectos(response)
    }
}