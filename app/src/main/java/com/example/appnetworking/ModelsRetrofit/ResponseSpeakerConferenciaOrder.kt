package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseSpeakerConferenciaOrder(

    @field:SerializedName("conferencia_id")
    val id: Int? = null,

    @field:SerializedName("nombre")
    val name: String? = null,

    @field:SerializedName("fecha")
    val fecha: String? = null,

    @field:SerializedName("horainicio")
    val start_hour: String? = null,

    @field:SerializedName("horafin")
    val end_hour: String? = null,

    @field:SerializedName("lugar")
    val place: String? = null,

    @field:SerializedName("descripcion")
    val descripcion: String? = null
)