package com.example.appnetworking.Profile

import com.example.appnetworking.ResponsePerfilContacto
import com.example.appnetworking.ResponseProyectos

interface PerfilContacto{

    interface View{
        fun SuccessPerfil(resp: ResponsePerfilContacto)
        fun GetDataPerfil(id_perfil: Int)
    }

    interface Presenter{
        fun SuccessPerfil(resp: ResponsePerfilContacto)
        fun GetDataPerfil(id_perfil: Int)
    }

    interface Model{
        fun SuccessPerfil(resp: ResponsePerfilContacto)
        fun GetDataPerfil(id_perfil: Int)
    }
}