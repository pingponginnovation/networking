package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileIntereses(
    @SerializedName("datos")
    val data: List<ResponseProfileInteresesOrder>
)