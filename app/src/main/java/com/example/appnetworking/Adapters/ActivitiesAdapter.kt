package com.example.appnetworking.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ActivityProgram.ActivityProgram
import com.example.appnetworking.ActivityProgram.ActivityProgramView
import com.example.appnetworking.ModelsRetrofit.ResponseActividadesOrder
import com.example.appnetworking.R

class ActivitiesAdapter: RecyclerView.Adapter<ActivitiesAdapter.ViewHolder>(){

    var list: List<ResponseActividadesOrder>? = null
    lateinit var act: ActivityProgramView

    fun ActivitiesAdapter(lista: List<ResponseActividadesOrder>, act: ActivityProgramView){

        list = lista
        this.act = act
        Log.e("list", list.toString())
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val hora = holder.itemView.findViewById(R.id.txt_hora) as TextView
        val name = holder.itemView.findViewById(R.id.txt_name) as TextView
        val btn_add = holder.itemView.findViewById(R.id.btn_add_program) as Button

        hora.text = list!![position].start_hour
        name.text = list!![position].name

        if(list!![position].flag_show_btn == 0) btn_add.visibility = View.GONE
        else btn_add.visibility = View.VISIBLE

        if(list!![position].inscrito == 1) btn_add.text = "Subscribed"
        else btn_add.text = "Add to agenda"

        btn_add.setOnClickListener{
            act.SubscribeProgram(list!![position].id_conference!!, btn_add)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_programs, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}