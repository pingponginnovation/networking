package com.example.appnetworking

data class RequestEditProfile(
    val perfil_id: Int,
    val nombre: String,
    val apellidos: String,
    val correo: String,
    val celular: String,
    val direccion: String,
    val pagina_web: String,
    val empresa: String,
    val puesto: String,
    val sinopsis: String,
    val imagen_perfil: String,
    val flag_change_img: Int
)