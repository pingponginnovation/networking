package com.example.appnetworking.Adapters

import android.content.Context
import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.CoreActivity
import com.example.appnetworking.DecorationRecyclers.GridItemDecoration
import com.example.appnetworking.Matches.MatchesView
import com.example.appnetworking.Matches.MatchesView.Companion.flingContainer
import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.R
import com.google.android.material.internal.ViewUtils.dpToPx
import com.squareup.picasso.Picasso
import io.cabriole.decorator.ColumnProvider
import io.cabriole.decorator.GridMarginDecoration
import kotlinx.android.synthetic.main.item_card_matches_new.view.*
import org.jetbrains.anko.toast

open class CardsMatchAdapter(context: Context?, list: List<ResponseMatchOrder>, core: CoreActivity):
    ArrayAdapter<ResponseMatchOrder>(context!!, 0, list){
    companion object{
        var auxiliar = 0
    }
    val coreA = core
    var interestAdapter: ChipsInterestAdapter? = null
    var vi: LayoutInflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View{

        val cardMatch = getItem(position)!!
        val retView: View

        if(convertView == null) retView = vi.inflate(R.layout.item_card_matches_new, parent,false)
        else retView = convertView

        retView.txt_name.text = cardMatch.name + " " + cardMatch.apellidos
        retView.txt_ocupacion.text = cardMatch.ocupation
        retView.txt_residencia.text = cardMatch.address
        retView.txt_about.text = cardMatch.description
        Picasso.get().load(cardMatch.img_perfil).into(retView.img_match)

        SetUpRecycler(retView, cardMatch)

        retView.btn_like.setOnClickListener{
            auxiliar = 1
            coreA.SetLikeDislike(cardMatch.id.toString(), "1")
            flingContainer.topCardListener.selectRight()
        }
        retView.btn_dislike.setOnClickListener{
            auxiliar = 1
            coreA.SetLikeDislike(cardMatch.id.toString(), "0")
            flingContainer.topCardListener.selectLeft()
        }
        return retView
    }

    fun SetUpRecycler(view: View, cardMatch: ResponseMatchOrder){
        view.rclv_interest.setHasFixedSize(true)
        view.rclv_interest.layoutManager = GridLayoutManager(context, 4)
        //view.rclv_interest.addItemDecoration(GridItemDecoration(10, 2))
        interestAdapter = ChipsInterestAdapter(cardMatch.interest, context)
        view.rclv_interest.adapter = interestAdapter
    }

    internal class ViewHolder{
        var image: ImageView? = null
    }
}