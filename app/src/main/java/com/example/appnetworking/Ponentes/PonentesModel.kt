package com.example.appnetworking.Ponentes

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseSpeakers

class PonentesModel(viewPresenter: Ponentes.Presenter, ctx: Context, prefs: PreferencesController): Ponentes.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
        prefes = prefs
    }

    override fun GetSpeakers(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetSpeakers(this)
    }

    override fun ErrorPonentes(resp: String){
        presenter.ErrorPonentes(resp)
    }

    override fun SuccessPonentes(response: ResponseSpeakers){
        presenter.SuccessPonentes(response)
    }
}