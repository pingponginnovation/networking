package com.example.appnetworking.ContactoChat

import android.content.Context
import com.example.appgenesys.RequestEnviarMensaje
import com.example.appgenesys.ResponseChatThread
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseEnviarMensaje

class ContactoChatPresenter(viewMatch: ContactoChat.View, ctx: Context, prefs: PreferencesController): ContactoChat.Presenter{

    private val view = viewMatch
    private val model = ContactoChatModel(this, ctx, prefs)

    //MODEL
    override fun GetMensajes(id: Int){
        model.GetMensajes(id)
    }

    override fun EnviarMensaje(request: RequestEnviarMensaje){
        model.EnviarMensaje(request)
    }
    //VIEW
    override fun ErrorMensajes(resp: String){
        view.ErrorMensajes(resp)
    }

    override fun SuccessMensajes(response: ResponseChatThread){
        view.SuccessMensajes(response)
    }

    override fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje){
        view.SuccessEnviarMensaje(resp)
    }

    override fun ErrorEnviarMensaje(resp: String){
        view.ErrorEnviarMensaje(resp)
    }
}