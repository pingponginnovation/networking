package com.example.appnetworking.PersonasByName

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.*
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.R
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePersonasByName
import kotlinx.android.synthetic.main.personas_by_intereses_activity.*
import kotlinx.android.synthetic.main.personas_by_name.*
import org.jetbrains.anko.toast

class PersonasByNameView: AppCompatActivity(), PersonasByName.View{

    private lateinit var presenter: PersonasByName.Presenter
    private var arrayAdapter: PeopleByNameAdapter? = null
    private var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personas_by_name)

        SetUpToolbar()
        presenter = PersonasByNamePresenter(this, applicationContext)

        val name = intent.getStringExtra("NOMBRE")!!
        GetPersonas(name)
    }

    fun SetUpToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back_black)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu!!.findItem(R.id.menu_search_icon)
        if(searchItem != null){

            searchView = searchItem.actionView as SearchView
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

                override fun onQueryTextSubmit(query: String?): Boolean{
                    arrayAdapter!!.getFilter().filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    arrayAdapter!!.getFilter().filter(newText)
                    return false
                }

            })
        }
        return true
    }

    fun GetPersonas(name: String){
        presenter.GetPersonas(name)
    }

    override fun ErrorPersonas(resp: String){
        txt_total_results.text = resp
    }

    override fun SuccessPersonas(response: ResponsePersonasByName){
        if(!response.data.isNullOrEmpty()){
            LY_NORESULTS_BYNAME.visibility = View.GONE
            LY_PEOPLE_BYNAME.visibility = View.VISIBLE
            txt_total_results.text = "Results: ${response.data.size}"
            SetData(response.data)
        }
        else{
            LY_NORESULTS_BYNAME.visibility = View.VISIBLE
            LY_PEOPLE_BYNAME.visibility = View.GONE
            SetData(response.data)
        }
    }

    fun SetData(datos: ArrayList<ResponsePersonasByNameOrder>){
        rclv_people_byname.setHasFixedSize(true)
        rclv_people_byname.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        arrayAdapter = PeopleByNameAdapter()
        arrayAdapter!!.PeopleByNameAdapter(datos, this@PersonasByNameView)
        rclv_people_byname.adapter = arrayAdapter
    }

    override fun onResume(){
        super.onResume()
    }

    override fun onPause(){
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}