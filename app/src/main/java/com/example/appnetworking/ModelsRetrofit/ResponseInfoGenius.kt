package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseInfoGeniusOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.google.gson.annotations.SerializedName

data class ResponseInfoGenius(

    @SerializedName("data")
    val data: ResponseInfoGeniusOrder
)