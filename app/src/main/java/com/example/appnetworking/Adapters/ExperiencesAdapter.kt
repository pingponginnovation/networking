package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.Profile.EditExperienceView
import com.example.appnetworking.R
import com.squareup.picasso.Picasso

class ExperiencesAdapter: RecyclerView.Adapter<ExperiencesAdapter.ViewHolder>(){

    private var list_expe: List<ResponseProfileExperienciasOrder>? = null
    private lateinit var contexto: Context
    private var type: String? = null

    fun ExperiencesAdapter(lista: List<ResponseProfileExperienciasOrder>, context: Context, type: String){
        list_expe = lista.reversed()
        this.contexto = context
        this.type = type
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_empresa) as TextView
        val cargo = holder.itemView.findViewById(R.id.txt_cargo) as TextView
        val fecha = holder.itemView.findViewById(R.id.txt_fecha) as TextView
        val ubicacion = holder.itemView.findViewById(R.id.txt_ubicacion) as TextView
        val yearsWork = holder.itemView.findViewById(R.id.txt_years_woking) as TextView
        val logo = holder.itemView.findViewById(R.id.img_experience) as ImageView

        titulo.text = list_expe!![position].empresa
        cargo.text = list_expe!![position].cargo
        yearsWork.text = list_expe!![position].year_expe

        if(list_expe!![position].actualmente == 1) fecha.text = "${list_expe!![position].date_start} - Presente"
        else fecha.text = "${list_expe!![position].date_start} - ${list_expe!![position].date_end}"

        ubicacion.text = list_expe!![position].location
        Picasso.get().load(list_expe!![position].logo).into(logo)

        holder.itemView.setOnClickListener{view ->

            if(!type.equals("Profile")){

                val intent = Intent(contexto, EditExperienceView::class.java)
                intent.putExtra("COMPANY", list_expe!![position].empresa)
                intent.putExtra("JOB", list_expe!![position].cargo)
                intent.putExtra("DATE_INICIO", list_expe!![position].date_start_numbers)
                intent.putExtra("DATE_END", list_expe!![position].date_end_numbers)
                intent.putExtra("DESCRIPTION", list_expe!![position].description)
                intent.putExtra("EXPERIENCIA_ID", list_expe!![position].id)
                intent.putExtra("DIRECCION", list_expe!![position].location)
                contexto.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_card_experiences, parent, false))
    }

    override fun getItemCount():Int{
        if(type.equals("Profile")) return list_expe!!.size - 2
        else return list_expe!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}