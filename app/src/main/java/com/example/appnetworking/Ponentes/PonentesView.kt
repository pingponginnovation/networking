package com.example.appnetworking.Ponentes

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.contentValuesOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.SpeakersAdapter
import com.example.appnetworking.CoreActivity
import com.example.appnetworking.Matches.MatchesView.Companion.type
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.Profile.ProfileSpeakerView
import com.example.appnetworking.R
import com.example.appnetworking.ResponseSpeakers
import kotlinx.android.synthetic.main.speakers_activity.*
import kotlinx.coroutines.withContext
import org.jetbrains.anko.toast
import java.util.*

class PonentesView(autocomplete: AutoCompleteTextView): Fragment(), Ponentes.View{

    private var speakerAutocomplete = autocomplete
    private lateinit var presenter: Ponentes.Presenter
    private lateinit var preferences: PreferencesController
    var adaptador_dropdownList: ArrayAdapter<String>? = null
    var filterList: ArrayList<ResponseSpeakersOrder>? = null
    var arrayAdapterSpeaker: SpeakersAdapter? = null
    var rclv: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        val v: View = inflater.inflate(R.layout.speakers_activity, container, false)

        ctx = context!!
        rclv = v.findViewById(R.id.rclv_speakers)
        filterList = ArrayList()
        adaptador_dropdownList = ArrayAdapter(context!!, android.R.layout.simple_dropdown_item_1line)
        preferences = PreferencesController(context!!)
        presenter = PonentesPresenter(this, context!!, preferences)
        GetSpeakers()
        return v
    }

    fun GetSpeakers(){
        presenter.GetSpeakers()
    }

    fun ConfigAutoComplete(){
        speakerAutocomplete.setHint("Search speaker")
        speakerAutocomplete.setThreshold(1)
        speakerAutocomplete.setAdapter(adaptador_dropdownList)
    }

    override fun ErrorPonentes(resp: String){
        context!!.toast(resp)
    }

    override fun SuccessPonentes(response: ResponseSpeakers){
        SetData(response)
        response.data.forEach{item->
            adaptador_dropdownList!!.add(item.name)
        }
        list = response.data
        ConfigAutoComplete()
    }

    fun HideKeyboar(){
        val activity = context as Activity
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(activity.currentFocus == null) return
        if(activity.currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }

    fun SetData(datos: ResponseSpeakers){
        if(datos.data.isNullOrEmpty()){
            //LY_NODATA.visibility = View.VISIBLE
            //LY_RECYCLER.visibility = View.GONE
        }
        else{
            rclv!!.setHasFixedSize(true)
            rclv_speakers.layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            arrayAdapterSpeaker = SpeakersAdapter()
            arrayAdapterSpeaker!!.SpeakersAdapter(datos.data, context!!)
            rclv_speakers.adapter = arrayAdapterSpeaker
        }
        LY_LOADER_SPEAKERS.visibility = View.GONE
        LY_SPEAKERS.visibility = View.VISIBLE
    }

    companion object{

        var ctx: Context? = null
        var list: List<ResponseSpeakersOrder>? = null

        fun ShowSpeaker(nameSpeaker: String){
            var id: Int? = null
            list!!.forEach{item->
                val fullName = item.name
                if(nameSpeaker == fullName)id = item.id
            }

            val intent = Intent(ctx, ProfileSpeakerView::class.java)
            intent.putExtra("idponente", id.toString())
            ctx!!.startActivity(intent)
        }
    }

    override fun onResume(){
        super.onResume()
        type = "speakers"
    }

    override fun onPause(){
        super.onPause()
    }
}