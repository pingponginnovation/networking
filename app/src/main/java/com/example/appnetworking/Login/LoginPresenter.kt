package com.example.appgenesys.Login

import android.content.Context
import com.example.appgenesys.Retrofit.Login
import com.example.appnetworking.LoginModel
import com.example.appnetworking.PreferencesController

class LoginPresenter(viewLogin: Login.View, ctx: Context, preferences: PreferencesController): Login.Presenter{

    private val view = viewLogin
    private val model = LoginModel(this, ctx, preferences)

    //MODEL
    override fun MakeLogin(user: String, password: String){
        model.MakeLogin(user, password)
    }

    //VIEW
    override fun SuccessLogin(name: String){
        view.SuccessLogin(name)
    }

    override fun ErrorLogin(resp: String) {
        view.ErrorLogin(resp)
    }

    override fun ErrorServer(msj: String) {
        view.ErrorServer(msj)
    }
}