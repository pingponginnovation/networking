package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseDatosCitaOrder(

    @field:SerializedName("cita_id")
    val id_cita: Int? = null,

    @field:SerializedName("titulo")
    val title: String? = null,

    @field:SerializedName("fecha_cita")
    val fecha: String? = null,

    @field:SerializedName("hora_inicio")
    val hora_inicio: String? = null,

    @field:SerializedName("hora_fin")
    val hora_fin: String? = null,

    @field:SerializedName("asunto")
    val descripcion: String? = null,

    @field:SerializedName("ubicacion")
    val lugar: String? = null,

    @field:SerializedName("tiempo_alerta")
    val alerta: String? = null,

    @field:SerializedName("flag_editar_cancelar")
    val flag_editar: Int? = null,

    @field:SerializedName("asistentes")
    val asistentes: ArrayList<ResponseDatosCitaAsistentesOrder>,

    @field:SerializedName("zoom")
    val zoom: ResponseDatosCitaZoomOrder
)