package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseLoginData
import com.google.gson.annotations.SerializedName

data class ResponseLogin(

    @SerializedName("success")
    val state: Boolean? = null,
    @SerializedName("mensaje")
    val msj: String? = null,
    @SerializedName("data")
    val datos: ResponseLoginData? = null
)
