package com.example.appgenesys.Login

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePerfilContacto

class MatchesModel(viewPresenter: Matches.Presenter, ctx: Context): Matches.Model{

    private val presenter = viewPresenter
    private var context: Context? = null

    init {
        context = ctx
    }

    override fun GetMatches(filtro: Int, searchs: String){
        val retrofit = RetrofitController(context!!)
        retrofit.Matches(this, filtro, searchs)
    }

    override fun ErrorMatches(resp: String){
        presenter.ErrorMatches(resp)
    }

    override fun SuccessMatches(response: ResponseMatch){
        presenter.SuccessMatches(response)
    }

    override fun SearchContacto(id_perfil: Int){
        val retrofit = RetrofitController(context!!)
        retrofit.SearchContacto(this, id_perfil)
    }

    override fun ContactoSuccess(response: ResponsePerfilContacto){
        presenter.ContactoSuccess(response)
    }
}