package com.example.appnetworking.Genius

import android.content.Context
import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseInfoGenius

class GeniusModel(viewPresenter: Genius.Presenter, ctx: Context): Genius.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetInformacion(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetInfoGenius(this)
    }

    override fun ErrorGenius(resp: String) {
        presenter.ErrorGenius(resp)
    }

    override fun SuccessInfo(response: ResponseInfoGenius) {
        presenter.SuccessInfo(response)
    }
}