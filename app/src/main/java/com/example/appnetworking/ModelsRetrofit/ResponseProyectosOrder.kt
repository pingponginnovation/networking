package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseProyectosOrder(

    @field:SerializedName("perfil_id")
    val id_profile_owner: Int? = null,

    @field:SerializedName("proyecto_id")
    val id: Int? = null,

    @field:SerializedName("titulo")
    val titulo: String? = null,

    @field:SerializedName("categoria_id")
    val id_categoria: Int? = null,

    @field:SerializedName("categoria")
    val categoria_name: String? = null,

    @field:SerializedName("descripcion")
    val description: String? = null,

    @field:SerializedName("contenido_img")
    val img: String? = null,

    @field:SerializedName("contenido_video")
    val video: String? = null,

    @field:SerializedName("opcion_seleccionada")
    val optionVoted: String? = null,

    @field:SerializedName("flag_editar")
    val flag_editar: Boolean? = null,

    @field:SerializedName("estatus")
    val status: Int? = null
)