package com.example.appnetworking.Agenda

import android.content.Context
import com.example.appgenesys.ResponseAgenda
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.ResponseDateTimeOrder
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestNewEvent

class AgendaModel(viewPresenter: Agenda.Presenter, ctx: Context, prefs: PreferencesController): Agenda.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
        prefes = prefs
    }

    override fun GetMyAgenda(date: String){
        val retrofit = RetrofitController(context!!)
        retrofit.GetAgenda(this, prefes!!.getMyIDprofile()!!.toInt(), date)
    }

    override fun GetContactos(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetContacts(this)
    }

    override fun CreateEvent(request: RequestNewEvent) {
        val retrofit = RetrofitController(context!!)
        retrofit.CreateEvent(this, request)
    }

    override fun GetDateTime(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetDateAndTime(this)
    }

    override fun SuccessContacts(response: ResponseContacts){
        presenter.SuccessContacts(response)
    }

    override fun ErrorContacts(msj: String) {
        presenter.ErrorContacts(msj)
    }

    override fun ErrorAgenda(resp: String){
        presenter.ErrorAgenda(resp)
    }

    override fun SuccessAgenda(response: ResponseAgenda){
        presenter.SuccessAgenda(response)
    }

    override fun SuccessEventCreated(){
        presenter.SuccessEventCreated()
    }

    override fun SuccessDateTime(response: ResponseDateTimeOrder){
        presenter.SuccessDateTime(response)
    }

    override fun ErrorDateTime(resp: String){
        presenter.ErrorDateTime(resp)
    }
}