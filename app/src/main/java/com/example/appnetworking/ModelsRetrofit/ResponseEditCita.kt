package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosOrder
import com.google.gson.annotations.SerializedName

data class ResponseEditCita(
    @SerializedName("mensaje")
    val msj: String
)