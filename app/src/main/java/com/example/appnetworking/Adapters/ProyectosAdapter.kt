package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.ResponseActividadesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseAgendaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosOrder
import com.example.appnetworking.Profile.ProfileSpeakerView
import com.example.appnetworking.Profile.ProyectoView
import com.example.appnetworking.R
import com.squareup.picasso.Picasso

class ProyectosAdapter: RecyclerView.Adapter<ProyectosAdapter.ViewHolder>(){

    var list_expe: List<ResponseProyectosOrder>? = null
    lateinit var contexto: Context
    var name = ""
    var puesto = ""
    var direcc = ""
    var foto = ""

    fun ProyectosAdapter(lista: List<ResponseProyectosOrder>, context: Context, name: String,
    puesto: String, addres: String, img: String){

        this.name = name
        this.puesto = puesto
        direcc = addres
        list_expe = lista
        this.contexto = context
        foto = img
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_titulo_p) as TextView
        val descripcion = holder.itemView.findViewById(R.id.txt_desc_p) as TextView
        val img = holder.itemView.findViewById(R.id.img_logo_proyecto) as ImageView

        titulo.text = list_expe!![position].titulo
        descripcion.text = list_expe!![position].description
        Picasso.get().load(list_expe!![position].img).into(img)

        holder.itemView.setOnClickListener{view ->

            val intent = Intent(contexto, ProyectoView::class.java)
            intent.putExtra("ID_PROYECTO", list_expe!![position].id)
            intent.putExtra("ID_OWNER", list_expe!![position].id_profile_owner)
            intent.putExtra("TITULO", list_expe!![position].titulo)
            intent.putExtra("DESCRIPCION", list_expe!![position].description)
            intent.putExtra("VIDEO", list_expe!![position].video)
            intent.putExtra("FOTO_PROYECTO", list_expe!![position].img)
            intent.putExtra("ID_CATEGORIA", list_expe!![position].id_categoria)
            intent.putExtra("CATEGORIA", list_expe!![position].categoria_name)
            intent.putExtra("NAME", name)
            intent.putExtra("PUESTO", puesto)
            intent.putExtra("DIRECC", direcc)
            intent.putExtra("IMG", foto)
            intent.putExtra("OPTION_VOTED", list_expe!![position].optionVoted)
            intent.putExtra("FLAG_EDITAR", list_expe!![position].flag_editar)
            intent.putExtra("ESTATUS", list_expe!![position].status)
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_proyectos, parent, false))
    }

    override fun getItemCount(): Int {
        return list_expe!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}