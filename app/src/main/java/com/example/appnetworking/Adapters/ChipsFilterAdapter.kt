package com.example.appnetworking.Adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.R

class ChipsFilterAdapter: RecyclerView.Adapter<ChipsFilterAdapter.ViewHolder>(){

    var chips = arrayListOf<String>()
    lateinit var contexto: Context

    fun ChipsFilterAdapter(items: ArrayList<String>, context: Context){

        chips = items
        this.contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val chip = holder.itemView.findViewById(R.id.txt_chip) as TextView
        val close = holder.itemView.findViewById(R.id.id_chip_close) as ImageButton

        chip.text = chips[position]
        close.setOnClickListener{
            chips.remove(chips[position])
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_chips_filters, parent, false))
    }

    override fun getItemCount(): Int {
        return chips.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}