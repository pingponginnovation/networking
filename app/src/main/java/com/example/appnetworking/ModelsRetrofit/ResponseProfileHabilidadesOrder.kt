package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileHabilidadesOrder(

    @field:SerializedName("habilidad_id")
    val id: Int? = null,

    @field:SerializedName("habilidad")
    val skill: String? = null
)