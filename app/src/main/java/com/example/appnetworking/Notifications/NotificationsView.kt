package com.example.appnetworking.Notifications

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.NotificationsAdapter
import com.example.appnetworking.CoreActivity
import com.example.appnetworking.R
import com.example.appnetworking.ResponseNotificaciones
import com.example.appnetworking.ResponseNotificacionesOrder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.app_bar_menu.*
import kotlinx.android.synthetic.main.core_activity.*
import kotlinx.android.synthetic.main.core_activity.view.*
import kotlinx.android.synthetic.main.notifications_lista_activity.*
import org.jetbrains.anko.toast
import kotlin.collections.ArrayList

class NotificationsView(actividad: CoreActivity): Fragment(), Notifications.View{

    private var myActivity: CoreActivity = actividad
    private lateinit var presenter: Notifications.Presenter
    private var adapter: NotificationsAdapter? = null
    private var datos: ArrayList<ResponseNotificacionesOrder>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        val v: View = inflater.inflate(R.layout.notifications_lista_activity, container, false)

        presenter = NotificationsPresenter(this, context!!)
        GetNotifications()
        myActivity.back_button_core.setOnClickListener{
            myActivity.NetworkingActivity()
        }
        return v
    }

    fun GetNotifications(){
        presenter.GetNotifications()
    }

    override fun ErrorNotifications(resp: String){
        context!!.toast(resp)
    }

    override fun SuccessNotifications(response: ResponseNotificaciones){
        datos = response.data
        SetData()
    }

    fun SetData(){

        if(!datos.isNullOrEmpty()){

            //id_badge.setNumber(intent.getIntExtra("NOTIFS_PORLEER", -1))
            NO_NOTIS_LY.visibility = View.GONE
            NOTIFICATIONS_LY.visibility = View.VISIBLE

            rclv_notifications.setHasFixedSize(true)
            rclv_notifications.layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
            adapter = NotificationsAdapter()
            adapter!!.NotificationsAdapter(datos!!, context!!)
            rclv_notifications.adapter = adapter
        }
    }

    fun UpdateAdapterNotifications(idNotificationRead: Int){

        var forDelete: ResponseNotificacionesOrder? = null
        datos!!.forEach{item->
            if(item.id_notification == idNotificationRead) forDelete = item
        }
        datos!!.remove(forDelete)
        adapter!!.notifyDataSetChanged()
    }
}