package com.example.appnetworking

import android.content.Context
import com.example.appgenesys.Retrofit.Login
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController

class LoginModel(viewPresenter: Login.Presenter, ctx: Context, preferences: PreferencesController): Login.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
        prefes = preferences
    }

    override fun MakeLogin(user: String, password: String){
        val retrofit = RetrofitController(context!!)
        retrofit.Login(this, user, password)
    }

    override fun SuccessLogin(response: ResponseLogin){

        prefes!!.saveToken(response.datos!!.accessToken)
        prefes!!.SaveIDProfile(response.datos.id_user)
        prefes!!.SaveNameProfile(response.datos.name)
        prefes!!.SavePuesto(response.datos.puesto_user)
        presenter.SuccessLogin(response.datos.name)
    }

    override fun ErrorLogin(resp: String){
        presenter.ErrorLogin(resp)
    }

    override fun ErrorServer(msj: String) {
        presenter.ErrorServer(msj)
    }
}