package com.example.appnetworking.FCMnotifications

class Config{
    companion object{
        // global topic to receive app wide push notifications
        val TOPIC_GLOBAL = "global"
        // broadcast receiver intent filters
        val REGISTRATION_COMPLETE = "registrationComplete"
        var PUSH_NOTIFICATION = "pushNotification"
        // id to handle the notification in the notification tray
        val NOTIFICATION_ID = 100
        val NOTIFICATION_ID_BIG_IMAGE = 101
    }
}