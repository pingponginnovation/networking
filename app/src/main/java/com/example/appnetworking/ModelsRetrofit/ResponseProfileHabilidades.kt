package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileHabilidades(
    @SerializedName("datos")
    val data: List<ResponseProfileHabilidadesOrder>
)