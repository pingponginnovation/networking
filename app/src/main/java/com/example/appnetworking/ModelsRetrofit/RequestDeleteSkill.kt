//package com.example.appnetworking
package com.example.appgenesys

data class RequestDeleteSkill(val perfil_id: Int, val skill_id: Int)