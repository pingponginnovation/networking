package com.example.appnetworking.PersonasByName

import android.content.Context
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePersonasByName

class PersonasByNameModel(viewPresenter: PersonasByName.Presenter, ctx: Context): PersonasByName.Model{

    private val presenter = viewPresenter
    private var context: Context? = null

    init {
        context = ctx
    }

    override fun GetPersonas(name: String){
        val retrofit = RetrofitController(context!!)
        retrofit.FindPersonas(this, name)
    }

    override fun ErrorPersonas(resp: String){
        presenter.ErrorPersonas(resp)
    }

    override fun SuccessPersonas(response: ResponsePersonasByName){
        presenter.SuccessPersonas(response)
    }
}