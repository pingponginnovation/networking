package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.Profile.ProfileSpeakerView
import com.example.appnetworking.Profile.ProyectoView
import com.example.appnetworking.R
import com.example.appnetworking.ResponseProyectosGeneral
import com.squareup.picasso.Picasso

class ProyectosGeneralAdapter: RecyclerView.Adapter<ProyectosGeneralAdapter.ViewHolder>(){

    var list: List<ResponseProyectosGeneralOrder>? = null
    lateinit var contexto: Context

    fun ProyectosGeneralAdapter(lista: List<ResponseProyectosGeneralOrder>, context: Context){
        list = lista
        this.contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.txt_titulo_p) as TextView
        val descripcion = holder.itemView.findViewById(R.id.txt_desc_p) as TextView
        val img = holder.itemView.findViewById(R.id.img_logo_proyecto) as ImageView

        titulo.text = list!![position].titulo
        descripcion.text = list!![position].description
        if(!list!![position].img.isNullOrEmpty()) Picasso.get().load(list!![position].img).into(img)

        holder.itemView.setOnClickListener{view ->

            val intent = Intent(contexto, ProyectoView::class.java)
            intent.putExtra("ID_PROYECTO", list!![position].id)
            intent.putExtra("ID_OWNER", list!![position].id_profile_owner)
            intent.putExtra("TITULO", list!![position].titulo)
            intent.putExtra("DESCRIPCION", list!![position].description)
            Log.e("IMG", list!![position].video.toString())
            intent.putExtra("VIDEO", list!![position].video)
            Log.e("IMG", list!![position].img.toString())
            intent.putExtra("FOTO_PROYECTO", list!![position].img)
            intent.putExtra("ID_CATEGORIA", list!![position].categoriaID)
            intent.putExtra("CATEGORIA", list!![position].categoria_name)
            intent.putExtra("NAME", list!![position].name_owner)
            intent.putExtra("PUESTO", list!![position].puesto_owner)
            intent.putExtra("DIRECC", list!![position].direccion_owner)
            intent.putExtra("IMG", list!![position].img_owner)
            intent.putExtra("OPTION_VOTED", list!![position].optionVoted)
            intent.putExtra("FLAG_EDITAR", list!![position].falg_editar)
            intent.putExtra("ESTATUS", list!![position].estatus)
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_proyectos, parent, false))
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}