//package com.example.appnetworking
package com.example.appgenesys

import com.google.gson.annotations.SerializedName

data class ResponseDeleteInteres(
    @SerializedName("mensaje")
    val msj: String
)