package com.example.appnetworking.Profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.*
import com.example.appnetworking.Adapters.ExperiencesContactoAdapter
import com.example.appnetworking.Adapters.InterestsAdapter
import com.example.appnetworking.Adapters.SkillsAdapter
import com.example.appnetworking.DecorationRecyclers.GridItemDecoration
import com.example.appnetworking.Notifications.NotificationsView
import com.example.appnetworking.ProyectosLista.ProyectosListaView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_myprofile_new.*
import kotlinx.android.synthetic.main.profile_user_busqueda.*
import kotlinx.android.synthetic.main.profile_user_busqueda.img_myphoto
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_about
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_email
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_name
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_ocupacion
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_phone
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_residencia
import kotlinx.android.synthetic.main.profile_user_busqueda.txt_web_n

class PerfilContactoView: AppCompatActivity(), PerfilContacto.View{

    private lateinit var presenter: PerfilContacto.Presenter
    companion object{
        lateinit var responseContacto: ResponsePerfilContacto
        var idperfil: Int? = null
    }
    private var type = ""

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_user_busqueda)

        presenter = PerfilContactoPresenter(this, applicationContext)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back)

        GetExtras()
        if(intent.getStringExtra("FROM").equals("Notification", false)
            || intent.getStringExtra("FROM").equals("MY_CONTACT", false)){

            type = intent.getStringExtra("FROM")!!
            GetDataPerfil(intent.getIntExtra("ID_PERFIL", -1))
        }
        else{
            //Log.e("DATA", responseContacto.toString())
            SetDataPerfil()
            SetExperiences()
            SetInterest()
            SetSkills()
        }

        btn_projects.setOnClickListener{

            val intent = Intent(this, ProyectosListaView::class.java)
            intent.putExtra("Nombre", responseContacto.data.name)
            intent.putExtra("APELLIDOS", responseContacto.data.apellidos)
            intent.putExtra("Cargo", responseContacto.data.ocupation)
            intent.putExtra("Direccion", responseContacto.data.address)
            intent.putExtra("Foto", responseContacto.data.img_perfil)
            intent.putExtra("ID_PROFILE", idperfil.toString())
            startActivity(intent)
        }

        btn_add_contact.setOnClickListener{

            val retrofit = RetrofitController(this)

            if(intent.getStringExtra("FROM").equals("Notification", false)){

                val requestUpdate = RequestUpdateNotification(intent.getIntExtra("ID_NOTIFICACION",
                    -1), 1)

                //Log.e("es", requestUpdate.toString())

                retrofit.UpdateNotification(requestUpdate)
                retrofit.Invitacion(this, idperfil.toString(), "1")
                BUTTON_LY.visibility = View.GONE
            }
            else retrofit.Invitacion(this, idperfil.toString(), "2")
        }

        btn_cancel_contact.setOnClickListener{
            val retrofit = RetrofitController(this)
            retrofit.Invitacion(this, idperfil.toString(), "0")
            BUTTON_LY.visibility = View.GONE
        }
    }

    fun GetExtras(){
        if(intent.getStringExtra("FROM").equals("Notification", false)){

            if(intent.getStringExtra("MATCH").equals("InvitacionFriend", false)){
                    //Entra aqui cuando le doy click a la notificacion de amistad
                    //AQUI ES CUANDO ME LLEGA NOTIFICACION DE ALGUIEN QUE ME QUIERE AGREGAR
                idperfil = intent.getIntExtra("ID_PERFIL", -1)
                btn_add_contact.text = "Confirm"
                btn_cancel_contact.visibility = View.VISIBLE
            }
            else if(intent.getStringExtra("MATCH").equals("Pendiente", false)){
                //Entra aqui cuando doy click desde la lista de notificaciones
                //AQUI ES CUANDO ME LLEGA NOTIFICACION DE ALGUIEN QUE ME QUIERE AGREGAR
                idperfil = intent.getIntExtra("ID_PERFIL", -1)
                btn_add_contact.text = "Confirm"
                btn_cancel_contact.visibility = View.VISIBLE
            }
            else{
                //Aqui es cuando alguien me aceptó de amigos.
                btn_cancel_contact.visibility = View.GONE
                btn_add_contact.text = "In contacts"
                btn_add_contact.isEnabled = false
            }
        }
    }

    override fun GetDataPerfil(id_perfil: Int){
        presenter.GetDataPerfil(id_perfil)
    }

    override fun SuccessPerfil(resp: ResponsePerfilContacto){
            //TODO. Se llena la info from request friend or from contact list
        responseContacto = resp
        SetDataPerfil()
        SetExperiences()
        SetInterest()
        SetSkills()
    }

    fun SetDataPerfil(){

        if(responseContacto.data.friend == 1){
            btn_cancel_contact.visibility = View.GONE
            btn_add_contact.text = "In contacts"
            btn_add_contact.isEnabled = false
        }
        if(responseContacto.data.friend == 2){
            btn_add_contact.text = "Sent request"
            btn_add_contact.isEnabled = false
        }

        txt_name.text = responseContacto.data.name + " " + responseContacto.data.apellidos
        txt_ocupacion.text = responseContacto.data.ocupation
        txt_residencia.text = responseContacto.data.address
        Picasso.get().load(responseContacto.data.img_perfil).into(img_myphoto)
        txt_phone.text = responseContacto.data.cel
        txt_email.text = responseContacto.data.email
        txt_web_n.text = responseContacto.data.web
        txt_about.text = responseContacto.data.description
    }

    fun SetExperiences(){
        rclv_experiences_.setHasFixedSize(true)
        rclv_experiences_.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val arrayAdapter = ExperiencesContactoAdapter(responseContacto.data.experiences.data, this)
        rclv_experiences_.adapter = arrayAdapter
    }

    fun SetInterest(){
        rclv_interests_.setHasFixedSize(true)
        rclv_interests_.layoutManager = GridLayoutManager(this, 4)
        rclv_interests_.addItemDecoration(GridItemDecoration(10, 2))
        val arrayAdapter = InterestsAdapter()
        arrayAdapter.InterestsAdapter(responseContacto.data.interest.data, this)
        rclv_interests_.adapter = arrayAdapter
    }

    fun SetSkills(){
        rclv_skills_.setHasFixedSize(true)
        rclv_skills_.layoutManager = GridLayoutManager(this, 4)
        rclv_skills_.addItemDecoration(GridItemDecoration(10, 2))
        val arrayAdapter = SkillsAdapter()
        arrayAdapter.SkillsAdapter(responseContacto.data.habilidades.data, this)
        rclv_skills_.adapter = arrayAdapter

        LY_USER_FOUND.visibility = View.VISIBLE
        LY_LOADER_FOUND.visibility = View.GONE
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}