package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.Profile.ProfileSpeakerView
import com.example.appnetworking.Profile.ProyectoView
import com.example.appnetworking.ProjectsGeneral.ProjectsGeneral
import com.example.appnetworking.ProjectsGeneral.ProjectsGeneralView.Companion.categoriesOn
import com.example.appnetworking.R
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProyectosGeneral
import com.google.android.exoplayer2.util.TraceUtil
import com.squareup.picasso.Picasso

class ProyectosGeneralCategoriasAdapter: RecyclerView.Adapter<ProyectosGeneralCategoriasAdapter.ViewHolder>(){

    private var list: List<ResponseProyectosGeneralCategoriesOrder>? = null
    private lateinit var contexto: Context

    fun ProyectosGeneralCategoriasAdapter(lista: List<ResponseProyectosGeneralCategoriesOrder>, context: Context,
    presenter: ProjectsGeneral.Presenter){
        list = lista
        contexto = context
        Companion.presenter = presenter
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val categorie = holder.itemView.findViewById(R.id.btn_categorie) as TextView
        categorie.text = list!![position].categoria

        holder.itemView.setOnClickListener{view ->

            categoriesOn = true

            if(list!![position].selected){
                list!![position].selected = false
                selectedCategories.remove(list!![position].id_categorie!!)
                categorie.setTextColor(Color.parseColor("#820A47"))
                categorie.setBackgroundResource(R.drawable.buttons_round_main)

                if(selectedCategories.isEmpty()) presenter!!.GetProyectos(RequestProyectosGeneral(0, "", selectedCategories))
                else presenter!!.GetProyectos(RequestProyectosGeneral(1, list!![position].categoria!!, selectedCategories))
            }
            else{
                categorie.setTextColor(Color.WHITE)
                categorie.setBackgroundResource(R.drawable.buttons_round_main_selected)
                list!![position].selected = true
                selectedCategories.add(list!![position].id_categorie!!)

                presenter!!.GetProyectos(RequestProyectosGeneral(1, list!![position].categoria!!,
                    selectedCategories))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_categories_projects, parent, false))
    }

    override fun getItemCount(): Int{
        if(list!!.size > 9) return 9
        else return list!!.size
    }

    companion object{

        private var presenter: ProjectsGeneral.Presenter? = null
        var selectedCategories = arrayListOf<Int>()

        fun MakeSearch(name: String){
            presenter!!.GetProyectos(RequestProyectosGeneral(1, name, selectedCategories))
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}