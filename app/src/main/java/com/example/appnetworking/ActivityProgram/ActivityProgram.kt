package com.example.appnetworking.ActivityProgram

import android.widget.Button
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appgenesys.Retrofit.ResponseActividades
import com.example.appnetworking.RequestSuscribeConference

interface ActivityProgram{

    interface View{
        fun ErrorActivities(resp: String)
        fun SuccessActivities(response: ResponseActividades)

        fun SubscribeProgram(id: Int, btn: Button)
        fun SuccessSubscribed(resp: ResponseSuscribeConference)
        fun ErrorSubscribed(resp: String)
    }

    interface Presenter{
        fun GetListActivities()
        fun ErrorActivities(resp: String)
        fun SuccessActivities(response: ResponseActividades)

        fun SubscribeProgram(req: RequestSuscribeConference)
        fun SuccessSubscribed(resp: ResponseSuscribeConference)
        fun ErrorSubscribed(resp: String)
    }

    interface Model{
        fun GetListActivities()
        fun ErrorActivities(resp: String)
        fun SuccessActivities(response: ResponseActividades)

        fun SubscribeProgram(req: RequestSuscribeConference)
        fun SuccessSubscribed(resp: ResponseSuscribeConference)
        fun ErrorSubscribed(resp: String)
    }
}