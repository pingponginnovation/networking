package com.example.appnetworking.ActivityProgram

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appgenesys.Retrofit.ResponseActividades
import com.example.appnetworking.Adapters.ActivitiesAdapter
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.R
import com.example.appnetworking.RequestSuscribeConference
import kotlinx.android.synthetic.main.edit_skills.*
import kotlinx.android.synthetic.main.programs_activity.*

class ActivityProgramView: AppCompatActivity(), ActivityProgram.View{

    private lateinit var presenter: ActivityProgram.Presenter
    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var btnadd: Button

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.programs_activity)

        controlAlert = ControllerAlertDialog(this)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Activity program")
        presenter = ActivityProgramPresenter(this, applicationContext)
        ListActivities()
    }

    fun ListActivities(){
        controlAlert!!.ProgressBar()
        presenter.GetListActivities()
    }

    override fun SuccessActivities(response: ResponseActividades){
        controlAlert!!.StopProgress()
        SetData(response)
    }

    override fun ErrorActivities(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    fun SetData(datos: ResponseActividades){
        txt_date.text = datos.fechaevento
        rclv_programs.setHasFixedSize(true)
        rclv_programs.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val arrayAdapter = ActivitiesAdapter()
        arrayAdapter.ActivitiesAdapter(datos.data, this)
        rclv_programs.adapter = arrayAdapter

        LY_PROGRAMS.visibility = View.GONE
    }

    override fun SubscribeProgram(id: Int, btn: Button){
        btnadd = btn
        controlAlert!!.ProgressBar()
        presenter.SubscribeProgram(RequestSuscribeConference(id.toString()))
    }

    override fun SuccessSubscribed(resp: ResponseSuscribeConference){
        btnadd.text = "Subscribed"
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp.msj)
    }

    override fun ErrorSubscribed(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}