package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.ResponseMatchIntereses
import com.example.appnetworking.ModelsRetrofit.ResponseProfilePonenteExperienciaOrder
import com.example.appnetworking.R

class ChipsExperienceProfilePonente(items: List<ResponseProfilePonenteExperienciaOrder>, context: Context): RecyclerView.Adapter<ChipsExperienceProfilePonente.ViewHolder>(){

    var chips: List<ResponseProfilePonenteExperienciaOrder> = items
    var contexto: Context = context

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val titulo = holder.itemView.findViewById(R.id.chips_id) as TextView
        titulo.text = chips[position].name_experiencia
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_chips_interests, parent, false))
    }

    override fun getItemCount(): Int {
        if(chips.size > 8) return 8
        else return chips.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

    }
}