package com.example.appnetworking.Adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Models.InteresesForEdit
import com.example.appnetworking.Profile.EditIntereses
import com.example.appnetworking.R
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class InterestsToEditAdapter: RecyclerView.Adapter<InterestsToEditAdapter.ViewHolder>(){

    private var list: ArrayList<InteresesForEdit>? = null
    private var listIntereses: ArrayList<String>? = null
    lateinit var contexto: EditIntereses

    fun InterestsToEditAdapter(listIntereses: ArrayList<String>, lista: ArrayList<InteresesForEdit>,
                               context: EditIntereses){
        list = lista
        this.contexto = context
        this.listIntereses = listIntereses
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val titulo = holder.itemView.findViewById(R.id.txt_chip) as TextView
        val btn_close = holder.itemView.findViewById(R.id.delete_interest) as ImageButton
        titulo.text = listIntereses!![position]

        holder.itemView.setOnClickListener{
            contexto.toast(listIntereses!![position])
        }

        btn_close.setOnClickListener{
            contexto.alert("Are you sure?", "Warning"){
                positiveButton("Yes"){
                    contexto.DeleteInteres(list!![position].id, listIntereses!![position])
                }
                negativeButton("Cancel"){}
            }.show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_chips_intereses_edit, parent, false))
    }

    override fun getItemCount():Int{
        return listIntereses!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}