package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.google.gson.annotations.SerializedName

data class ResponseEnviarMensaje(
    @SerializedName("auto_mensaje")
    val mi_msj: String? = null,

    @SerializedName("fecha")
    val date: String? = null
)