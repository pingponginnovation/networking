package com.example.appnetworking

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.Login.Matches
import com.example.appgenesys.Login.MatchesPresenter
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.ActivityProgram.ActivityProgramView
import com.example.appnetworking.Adapters.ChipsFilterAdapter
import com.example.appnetworking.Adapters.ProyectosGeneralCategoriasAdapter.Companion.MakeSearch
import com.example.appnetworking.Agenda.AgendaView
import com.example.appnetworking.ContactosAndChats.TabsViewpagerActivity
import com.example.appnetworking.Genius.GeniusView
import com.example.appnetworking.Login.LoginView
import com.example.appnetworking.Matches.MatchesView
import com.example.appnetworking.Matches.MatchesView.Companion.arrayInfoMatches
import com.example.appnetworking.Matches.MatchesView.Companion.type
import com.example.appnetworking.Menu.MenuInterface
import com.example.appnetworking.Menu.MenuPresenter
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMenuOrder
import com.example.appnetworking.Notifications.NotificationsView
import com.example.appnetworking.PersonasByIntereses.PersonasByInteresesView
import com.example.appnetworking.PersonasByName.PersonasByNameView
import com.example.appnetworking.Ponentes.PonentesView
import com.example.appnetworking.Ponentes.PonentesView.Companion.ShowSpeaker
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.Profile.PerfilContactoView.Companion.idperfil
import com.example.appnetworking.Profile.PerfilContactoView.Companion.responseContacto
import com.example.appnetworking.Profile.ProfileView
import com.example.appnetworking.ProjectsGeneral.ProjectsGeneralView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.alertdialog_custom_expired.view.*
import kotlinx.android.synthetic.main.app_bar_menu.*
import kotlinx.android.synthetic.main.app_bar_menu.my_img
import kotlinx.android.synthetic.main.content_menu.*
import kotlinx.android.synthetic.main.core_activity.*
import kotlinx.android.synthetic.main.dialog_filter.view.*
import kotlinx.android.synthetic.main.nav_header_navigation_drawer.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class CoreActivity(): AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, Matches.View,
    MenuInterface.View{

    private lateinit var presenter: Matches.Presenter
    private lateinit var presenterMenu: MenuInterface.Presenter
    private lateinit var preferences: PreferencesController
    lateinit var matView: MatchesView
    var fragment: Fragment? = null
    var menu: Menu? = null
    var array_filters = arrayListOf<String>()
    var filtros: String = "" //Este string de filtros se manda en el servicio
    var adapterChips: ChipsFilterAdapter = ChipsFilterAdapter()
    var adaptador_dropdownList: ArrayAdapter<String>? = null
    var filtroActivo = 0
    var auxiliar = "no pause"
    private var menuAlreadyExist = false
    private var totalNotifsPorLeer = 0
    var autocompleteGeneral: AutoCompleteTextView? = null
    private var contactosNetworking: ArrayList<String> = ArrayList() //Para usarlo al regresar de otra view

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.core_activity)

        autocompleteGeneral = autocomplete_users!!
        autocompleteGeneral!!.addTextChangedListener(yourTextWatcher)

        preferences = PreferencesController(this)
        preferences.SetPausa("noPause")

        matView = MatchesView(this) //Esta variable no la uso, pero la tengo que instanciar para poder
                                //Pasarle este contexto a MatchesView y desde allá pueda usar los métodos de aqui
        SetUpButtons()
        adaptador_dropdownList = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)
        presenter = MatchesPresenter(this, applicationContext)
        presenterMenu = MenuPresenter(this, applicationContext)
        ConfigNotificationToken()
        GetMenu()
        ConfigFiltroMatches()
        setToolbar()
        GetMatches(filtroActivo, "")

        btn_clear_autocomplete.setOnClickListener{
            autocompleteGeneral!!.setText("")
        }
    }

    fun SetUpButtons(){
        btn_netorking.setOnClickListener{
            NetworkingActivity()
        }
        btn_projects.setOnClickListener{
            ProjectsFragment()
        }
        btn_speakers.setOnClickListener{
            SpeakersFragment()
        }
    }

    fun ProjectsFragment(){

        autocomplete_users.visibility = View.VISIBLE
        btn_clear_autocomplete.visibility = View.VISIBLE
        filter_by.visibility = View.VISIBLE
        TITLE_BACKBUTTON.visibility = View.GONE

        filter_by.isEnabled = false
        btn_netorking.setTextColor(Color.parseColor("#9e9e9e"))
        btn_speakers.setTextColor(Color.parseColor("#9e9e9e"))
        btn_projects.setTextColor(Color.parseColor("#820A47"))

        fragment = ProjectsGeneralView(autocompleteGeneral!!)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, fragment as ProjectsGeneralView).commit()
    }

    fun SpeakersFragment(){

        autocomplete_users.visibility = View.VISIBLE
        btn_clear_autocomplete.visibility = View.VISIBLE
        filter_by.visibility = View.VISIBLE
        TITLE_BACKBUTTON.visibility = View.GONE

        filter_by.isEnabled = false
        btn_netorking.setTextColor(Color.parseColor("#9e9e9e"))
        btn_speakers.setTextColor(Color.parseColor("#820A47"))
        btn_projects.setTextColor(Color.parseColor("#9e9e9e"))

        fragment = PonentesView(autocompleteGeneral!!)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, fragment as PonentesView).commit()
    }

    fun NetworkingActivity(){

        txt_general.visibility = View.GONE
        btn_netorking.visibility = View.VISIBLE
        btn_projects.visibility = View.VISIBLE
        btn_speakers.visibility = View.VISIBLE

        autocomplete_users.visibility = View.VISIBLE
        btn_clear_autocomplete.visibility = View.VISIBLE
        filter_by.visibility = View.VISIBLE
        TITLE_BACKBUTTON.visibility = View.GONE

        adaptador_dropdownList!!.clear()
        contactosNetworking.forEach{
            adaptador_dropdownList!!.add(it)
        }
        SetUpSearchField()

        filter_by.isEnabled = true
        btn_netorking.setTextColor(Color.parseColor("#820A47"))
        btn_speakers.setTextColor(Color.parseColor("#9e9e9e"))
        btn_projects.setTextColor(Color.parseColor("#9e9e9e"))

        fragment = MatchesView(this)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, fragment as MatchesView).commit()
    }

    fun NotificationsFragment(){

        filter_by.visibility = View.GONE
        autocomplete_users.visibility = View.GONE
        btn_netorking.visibility = View.GONE
        btn_speakers.visibility = View.GONE
        btn_projects.visibility = View.GONE
        btn_clear_autocomplete.visibility = View.GONE

        TITLE_BACKBUTTON.visibility = View.VISIBLE

        fragment = NotificationsView(this)
        val manager = supportFragmentManager
        manager.beginTransaction().replace(R.id.main_content, fragment as NotificationsView).commit()
    }

    fun SetLikeDislike(id: String, accion: String){
        val retrofit = RetrofitController(this)
        retrofit.SetLikeDislike(id, accion)
    }

    fun ConfigNotificationToken(){

        intent.extras?.let{

            for (key in it.keySet()){
                val value = intent.extras?.get(key)
                Log.e("DataExtra", value.toString())
            }
        }
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.e("InstanceID failed", task.exception.toString())
                    return@OnCompleteListener
                }

                val token = task.result?.token
                SaveTokenOnServer(token.toString(), "android")
            })
    }

    fun SetMatchInfo(){

        if(!preferences.getPause().equals("pause")){
            fragment = MatchesView(this)
            val manager = supportFragmentManager
            manager.beginTransaction().replace(R.id.main_content, fragment as MatchesView).commit()
        }
        else preferences.SetPausa("noPause")
    }

    fun ConfigNavDrawer(response: ArrayList<ResponseMenuOrder>){

        drawer_button.setOnClickListener{
            if (drawer_layout.isDrawerOpen(GravityCompat.END)) drawer_layout.closeDrawer(GravityCompat.END)
            else drawer_layout.openDrawer(GravityCompat.END)
        }

        my_img_header.setOnClickListener{
            drawer_layout.closeDrawer(GravityCompat.END)
        }

        val toogle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_abrir, R.string.navigation_drawer_cerrar)
        toogle.isDrawerIndicatorEnabled = false
        drawer_layout.addDrawerListener(toogle)
        toogle.syncState()
        navigation_view.setNavigationItemSelectedListener(this)

        val menu: Menu = navigation_view.menu
        response.forEach{item->
            menu.add(0, item.id!!, Menu.FIRST, item.name)
            if(item.id == 3){
                val view: MenuItem = menu.getItem(2).setActionView(R.layout.notification_badge)
                val linearDot = view.actionView.findViewById<LinearLayout>(R.id.id_badge_menu2)
                val txt_number_dot = view.actionView.findViewById<TextView>(R.id.id_badge_menu_txt2)
                SetTotalNotifications(item.total_notificaciones!!, linearDot, txt_number_dot)
            }
        }
    }

    fun SetTotalNotifications(notifications: Int, lineardot: LinearLayout, txtNumber: TextView){
        totalNotifsPorLeer = notifications
        if(notifications == 0){
            id_badge_menu_header.visibility = View.GONE
            lineardot.visibility = View.GONE
            id_badge_menu.visibility = View.GONE
        }
        else{
            id_badge_menu_txt_header.setText(totalNotifsPorLeer.toString())
            txtNumber.setText(totalNotifsPorLeer.toString())
            id_badge_menu_txt.setText(totalNotifsPorLeer.toString())
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean{
        val id = item.itemId
        when(id){
            1->NetworkingActivity()
            2->startActivity<ProfileView>()
            3->NotificationsFragment()
            4->startActivity<TabsViewpagerActivity>()
            5->startActivity<AgendaView>()
            6->startActivity<ActivityProgramView>()
            7->{
                txt_general.visibility = View.VISIBLE
                txt_general.text = "Speakers"
                btn_netorking.visibility = View.GONE
                btn_projects.visibility = View.GONE
                btn_speakers.visibility = View.GONE
                SpeakersFragment()
            }
            8->startActivity<GeniusView>()
            9->showAlertCloseSession()
            10->{
                txt_general.visibility = View.VISIBLE
                txt_general.text = "Projects"
                btn_netorking.visibility = View.GONE
                btn_projects.visibility = View.GONE
                btn_speakers.visibility = View.GONE
                ProjectsFragment()
            }
        }

        val fragment: Fragment? = null
        if(fragment != null){
            val manager = supportFragmentManager
            manager.beginTransaction().replace(R.id.main_content, fragment).commit()
        }
        drawer_layout.closeDrawer(GravityCompat.END)
        return true
    }

    fun ConfigFiltroMatches(){

        filter_by.setOnClickListener{

            val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
            var dialog: AlertDialog? = null
            val mView: View = layoutInflater.inflate(R.layout.dialog_filter, null)

            if(filtroActivo == 1){ //Para mostrar los filtros que hay activos y el boton stop filtering
                mView.REC.visibility = View.VISIBLE
                mView.btn_stop_filtering.visibility = View.VISIBLE
                adapterChips.ChipsFilterAdapter(array_filters, this)
                mView.rclv_filter.adapter = adapterChips
            }else{

                filtros = ""
                array_filters.clear()
                mView.REC.visibility = View.GONE
                mView.btn_stop_filtering.visibility = View.GONE
            }

            InitRecycler(mView)
            mView.btn_add_filter.setOnClickListener{

                if(mView.edt_filter.text.toString().isNotEmpty()){

                    mView.REC.visibility = View.VISIBLE

                    if(filtros.isEmpty()){

                        filtros += mView.edt_filter.text.toString()
                        array_filters.add(mView.edt_filter.text.toString())
                        adapterChips.ChipsFilterAdapter(array_filters, this)
                        mView.rclv_filter.adapter = adapterChips
                    }
                    else{
                        filtros += ","+mView.edt_filter.text.toString()
                        array_filters.add(mView.edt_filter.text.toString())
                        adapterChips.notifyDataSetChanged()
                    }

                    mView.edt_filter.setText("") //Clear text

                }else toast("Please type a filter")
            }

            val search = mView.findViewById(R.id.btn_search) as Button
            search.setOnClickListener{view->

                if(array_filters.isEmpty()){
                    filtros = ""
                    filtroActivo = 0
                    mView.REC.visibility = View.GONE
                    mView.btn_stop_filtering.visibility = View.GONE
                    GetMatches(filtroActivo, "") //All results
                    toast("Please type a filter")
                }
                else{
                    HideKeyboard()
                    filtroActivo = 1
                    //GetMatches(filtroActivo, filtros)
                    val intent = Intent(this, PersonasByInteresesView::class.java)
                    intent.putExtra("STRING_FILTROS", filtros)
                    intent.putExtra("FILTRO_ACTIVO", filtroActivo)
                    intent.putStringArrayListExtra("ARRAY_FILTROS", array_filters)
                    startActivity(intent)
                    //dialog!!.dismiss()
                }
            }

            val stopFiltering = mView.findViewById(R.id.btn_stop_filtering) as Button
            stopFiltering.setOnClickListener{

                filtroActivo = 0
                GetMatches(filtroActivo, "")
                dialog!!.dismiss()
                filtros = ""
                array_filters.clear()
            }

            mBuilder.setView(mView)
            dialog = mBuilder.create()
            dialog.show()
        }
    }

    fun InitRecycler(view: View){
        view.rclv_filter.setHasFixedSize(true)
        view.rclv_filter.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
    }

    fun SetUpSearchField(){
        autocompleteGeneral!!.setHint("Search contact")
        autocompleteGeneral!!.setThreshold(1)
        autocompleteGeneral!!.setAdapter(adaptador_dropdownList)
        autocompleteGeneral!!.setOnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){

                if(type.equals("matches")){
                    HideKeyboard()
                    autocompleteGeneral!!.dismissDropDown()
                    val intent = Intent(this, PersonasByNameView::class.java)
                    intent.putExtra("NOMBRE", autocompleteGeneral!!.text.toString())
                    startActivity(intent)
                }
            }
            true
        }
    }

    var aux = 0
    var responseForAdapter: ArrayList<ResponseMatchUsersBusquedaOrder> = ArrayList()
    protected var yourTextWatcher: TextWatcher = object: TextWatcher{
        override fun afterTextChanged(s: Editable){}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int){}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int){

            if(autocompleteGeneral!!.isPerformingCompletion){

                if((aux == 0) && (type.equals("matches"))){ //To avoid double search when we activate a filter.

                    aux = 1
                    HideKeyboard()
                    autocompleteGeneral!!.dismissDropDown()
                    GetContactID(responseForAdapter, autocompleteGeneral!!.text.toString())
                }
                else if(type.equals("speakers")) ShowSpeaker(autocompleteGeneral!!.text.toString())
                else{
                    HideKeyboard()
                    MakeSearch(autocompleteGeneral!!.text.toString())
                }
            }
        }
    }

    fun HideKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(currentFocus == null) return
        if(currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    fun GetContactID(response: ArrayList<ResponseMatchUsersBusquedaOrder>, name: String){
        var id: Int? = null
        response.forEach{item->
            val fullName = item.name+" "+item.apellidos
            if(name == fullName)id = item.id
        }
        SearchContacto(id!!) //ID del contacto que voy a ver su perfil
    }

    override fun GetMenu(){
        presenterMenu.GetMenu()
    }

    override fun SuccessMenu(resp: ArrayList<ResponseMenuOrder>, photo: String){
        if(!menuAlreadyExist){
            menuAlreadyExist = true
            ConfigNavDrawer(resp)
        }
        Picasso.get().load(photo).into(my_img)
        Picasso.get().load(photo).into(my_img_header)
        txt_namedrawer.text = preferences.getNameProfile()
        txtpuestodrawer.text = preferences.getPuesto()
        preferences.SaveImageMenu(photo)
    }

    fun setToolbar() {
        setSupportActionBar(toolbar)
        getSupportActionBar()!!.setDisplayShowTitleEnabled(false)
    }

    fun SaveTokenOnServer(token: String, os: String){
        val retrofit = RetrofitController(this)
        retrofit.SaveToken(this, token, os)
    }

    override fun GetMatches(filtroState: Int, filtros: String){
        presenter.GetMatches(filtroState, filtros)
    }

    override fun SuccessMatches(response: ResponseMatch){

        if(!adaptador_dropdownList!!.isEmpty) adaptador_dropdownList!!.clear()
        arrayInfoMatches = response.data //Usado en MatchesView para el adapter de matches.
        response.usersRegistered.forEach{item->
            contactosNetworking.add("${item.name} ${item.apellidos}")
            adaptador_dropdownList!!.add("${item.name} ${item.apellidos}")
        }
        responseForAdapter = response.usersRegistered
        SetUpSearchField()
        SetMatchInfo()

        LOADER_LY.visibility = View.GONE
        LY_INFO.visibility = View.VISIBLE
    }

    override fun ErrorMatches(resp: String){
        if(resp == "Unauthorized"){

            val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
            var dialog: AlertDialog? = null
            val view = LayoutInflater.from(this).inflate(R.layout.alertdialog_custom_expired, null)

            mBuilder.setView(view)
            dialog = mBuilder.create()
            dialog.show()
            dialog.setCancelable(false)

            view.txt_message.text = "Your session has expired. Please log in again"
            view.txt_message.setTextColor(Color.parseColor("#5D002F"))

            view.btn_aceptar.setOnClickListener{
                dialog.dismiss()
                closeSession()
            }
        }
    }

    override fun SearchContacto(id_perfil: Int){
        presenter.SearchContacto(id_perfil)
        idperfil = id_perfil //TODO. idperfil is static in PerfilContactoView
    }

    override fun ContactoSuccess(response: ResponsePerfilContacto){
        responseContacto = response //TODO. responsContacto is static in PerfilContactoView
        startActivity<PerfilContactoView>()
    }

    fun showAlertCloseSession(){
        alert("Are you sure you want to close session?", "Warning"){
            positiveButton("Yes") {closeSession()}
            negativeButton("Cancel"){}
        }.show()
    }

    fun closeSession(){
        preferences.CloseSession()
        startActivity<LoginView>()
        finish()
    }

    override fun onResume(){
        super.onResume()

        if(auxiliar.equals("pause")){
            GetMenu()
            auxiliar = "no pause"
        } //For load photo

        aux = 0 //used in searchfield
    }

    override fun onPause(){
        super.onPause()
        auxiliar = "pause"
    }

    override fun onBackPressed(){
        if (drawer_layout.isDrawerOpen(GravityCompat.END)) drawer_layout.closeDrawer(GravityCompat.END)
        else super.onBackPressed()
    }
}