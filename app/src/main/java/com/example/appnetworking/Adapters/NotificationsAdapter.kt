package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Profile.PerfilContactoView
import com.example.appnetworking.R
import com.example.appnetworking.RedireccionAgendaInvitacion.AgendaInvitacionView
import com.example.appnetworking.ResponseNotificacionesOrder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_notifications.view.*
import java.util.ArrayList

class NotificationsAdapter: RecyclerView.Adapter<NotificationsAdapter.ViewHolder>(){

    var list: ArrayList<ResponseNotificacionesOrder>? = null
    lateinit var contexto: Context

    fun NotificationsAdapter(lista: ArrayList<ResponseNotificacionesOrder>, context: Context){
        list = lista
        contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val hora = holder.itemView.findViewById(R.id.txt_hora_c) as TextView
        val name = holder.itemView.findViewById(R.id.txt_name_c) as TextView
        val msg = holder.itemView.findViewById(R.id.txt_notification) as TextView
        val imagen = holder.itemView.findViewById(R.id.my_img) as ImageView

        if(list!![position].status == 0) holder.itemView.card_notifs
            .setCardBackgroundColor(Color.parseColor("#DAEDF9"))

        name.text = list!![position].title
        hora.text = list!![position].fecha_created
        msg.text = list!![position].msj
        Picasso.get().load(list!![position].img).into(imagen)

        holder.itemView.setOnClickListener{
            val intent: Intent?

            if(list!![position].match_type.equals("CitaEnviada")){
                intent = Intent(contexto, AgendaInvitacionView::class.java)
                intent.putExtra("ID_PERFIL", list!![position].profile_id)
                intent.putExtra("ID_CITA", list!![position].cita_id)
            }
            else{
                intent = Intent(contexto, PerfilContactoView::class.java)
                intent.putExtra("FROM", "Notification")
                Log.e("matchtype", list!![position].match_type.toString())
                intent.putExtra("MATCH", list!![position].match_type)
                intent.putExtra("ID_PERFIL", list!![position].profile_id)
                intent.putExtra("ID_NOTIFICACION", list!![position].id_notification)
            }
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_notifications, parent, false))
    }

    override fun getItemCount(): Int{
        return list!!.size
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}