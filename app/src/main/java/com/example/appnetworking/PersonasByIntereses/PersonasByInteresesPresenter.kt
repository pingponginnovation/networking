package com.example.appnetworking.PersonasByIntereses

import android.content.Context
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.ResponseMatch

class PersonasByInteresesPresenter(viewMatch: PersonasByIntereses.View, ctx: Context): PersonasByIntereses.Presenter{

    private val view = viewMatch
    private val model = PersonasByInteresesModel(this, ctx)

    //MODEL
    override fun GetMatchesFiltro(filtro: Int, filtros: String){
        model.GetMatchesFiltro(filtro, filtros)
    }

    //VIEW
    override fun ErrorMatches(resp: String){
        view.ErrorMatches(resp)
    }

    override fun SuccessMatchFiltro(response: ResponseMatch){
        view.SuccessMatchFiltro(response)
    }
}