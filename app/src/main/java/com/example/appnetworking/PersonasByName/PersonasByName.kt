package com.example.appnetworking.PersonasByName

import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePersonasByName

interface PersonasByName{

    interface View{
        fun ErrorPersonas(resp: String)
        fun SuccessPersonas(response: ResponsePersonasByName)
    }

    interface Presenter{
        fun GetPersonas(name: String)
        fun ErrorPersonas(resp: String)
        fun SuccessPersonas(response: ResponsePersonasByName)
    }

    interface Model{
        fun GetPersonas(name: String)
        fun ErrorPersonas(resp: String)
        fun SuccessPersonas(response: ResponsePersonasByName)
    }
}