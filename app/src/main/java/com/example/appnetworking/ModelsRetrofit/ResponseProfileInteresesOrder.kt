package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileInteresesOrder(

    @field:SerializedName("interes_id")
    val id: Int? = null,

    @field:SerializedName("interes")
    val interes: String? = null
)