package com.example.appnetworking.Ponentes

import android.content.Context
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseSpeakers

class PonentesPresenter(viewMatch: Ponentes.View, ctx: Context, prefs: PreferencesController): Ponentes.Presenter{

    private val view = viewMatch
    private val model = PonentesModel(this, ctx, prefs)

    //MODEL
    override fun GetSpeakers(){
        model.GetSpeakers()
    }

    //VIEW
    override fun ErrorPonentes(resp: String){
        view.ErrorPonentes(resp)
    }

    override fun SuccessPonentes(response: ResponseSpeakers){
        view.SuccessPonentes(response)
    }
}