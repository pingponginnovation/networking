package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponsePerfilContactoOrder
import com.google.gson.annotations.SerializedName

data class ResponseNotificaciones(

    @SerializedName("data")
    val data: ArrayList<ResponseNotificacionesOrder>
)