package com.example.appnetworking.Models
import java.io.Serializable

data class InteresesForEdit(
    val name: String,
    val id: Int
): Serializable