package com.example.appnetworking.ContactosAndChats
import android.content.BroadcastReceiver
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.Adapters.ContactosAdapter
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.contactos_activity.*
import org.jetbrains.anko.toast
import java.util.ArrayList

class ContactosFragment: Fragment(), Contactos.View{

    private lateinit var presenter: Contactos.Presenter
    private lateinit var preferences: PreferencesController
    private var filterList: ArrayList<ResponseSpeakersOrder>? = null
    private var adapterContacts: ContactosAdapter? = null
    private var searchView: SearchView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{

        preferences = PreferencesController(context!!)
        presenter = ContactosPresenter(this, context!!, preferences)
        GetContacts()
        return inflater.inflate(R.layout.fragment_contactos, container, false)
    }

    fun GetContacts(){
        presenter.GetContacts()
    }

    override fun ErrorContacts(resp: String){
        context!!.toast(resp)
    }

    override fun SuccessContacts(response: ResponseContacts){
        SetData(response)
    }

    fun SetData(datos: ResponseContacts){

        if(datos.contacts.isNullOrEmpty()){
            LY_LOADER_CONT.visibility = View.VISIBLE
            LY_CONTACTS.visibility = View.GONE
        }
        else{
            rclv_contactos.setHasFixedSize(true)
            rclv_contactos.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapterContacts = ContactosAdapter()
            adapterContacts!!.ContactosAdapter(datos.contacts, context!!)
            rclv_contactos.adapter = adapterContacts
        }
    }
}