package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.google.gson.annotations.SerializedName

data class ResponseVotarProyecto(
    @SerializedName("mensaje")
    val msj: String? = null,

    @SerializedName("proyecto_id")
    val id: Int? = null,

    @SerializedName("opcion_seleccionda")
    val optionVoted: String? = null
)