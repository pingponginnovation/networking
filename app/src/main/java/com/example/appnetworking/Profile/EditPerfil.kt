package com.example.appnetworking.Profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.edit_profile_activity.*
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream

class EditPerfil: AppCompatActivity(){

    private var miID: Int? = null
    companion object{
        private val IMAGE_PICK_CODE = 1000;
        private val PERMISSION_CODE = 1001;
    }
    var encodedImage = "nothing"
    private var flag_img = 0
    private lateinit var preferences: PreferencesController

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile_activity)

        preferences = PreferencesController(this)
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.close)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        icon_edit.setOnClickListener{
            CheckPermissionGallery()
        }
        GetExtras()
    }

    fun CheckPermissionGallery(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){

                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissions(permissions, PERMISSION_CODE)

            }else pickImageFromGallery()
        }else pickImageFromGallery()
    }

    private fun pickImageFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    fun GetExtras(){
        edt_name.setText(intent.getStringExtra("NAME"))
        edt_apellidos.setText(intent.getStringExtra("APELLIDOS"))
        edt_puesto.setText(intent.getStringExtra("PUESTO"))
        edt_city.setText(intent.getStringExtra("CITY"))
        edt_phone.setText(intent.getStringExtra("PHONE"))
        edt_email.setText(intent.getStringExtra("EMAIL"))
        edt_web.setText(intent.getStringExtra("WEBSITE"))
        edt_bio.setText(intent.getStringExtra("BIOGRAPHY"))
        Picasso.get().load(intent.getStringExtra("IMAGEN")).into(img_myphoto)
        miID = intent.getIntExtra("MI_ID", -1)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        menuInflater.inflate(R.menu.menu_edit_profile, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId){
        R.id.icon_confirm->{

            if(edt_name.text.toString().isEmpty() || edt_apellidos.text.toString().isEmpty() ||
                    edt_email.text.toString().isEmpty() || edt_city.text.toString().isEmpty() ||
                    edt_phone.text.toString().isEmpty() || edt_puesto.text.toString().isEmpty() ||
                    edt_bio.text.toString().isEmpty()){
                toast("Please fill in required fields")
            }
            else{
                val retrofit = RetrofitController(this)
                retrofit.EditProfile(miID!!, edt_name.text.toString(), edt_apellidos.text.toString(), this,
                    edt_email.text.toString(), edt_phone.text.toString(),
                    edt_city.text.toString(), edt_web.text.toString(), edt_puesto.text.toString(),
                    edt_puesto.text.toString(), edt_bio.text.toString(), encodedImage, flag_img)
            }
            true
        }
        else ->{
            super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray){
        when(requestCode){
            PERMISSION_CODE ->{
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    pickImageFromGallery()
                else toast("You must grant permission to access gallery")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            val image: Uri = data!!.data!!
            val typeImage = contentResolver.getType(image) //exmple:   image/png

            Glide.with(this)
                .asBitmap()
                .load(image)
                .into(object : CustomTarget<Bitmap>(1000, 1000){
                    override fun onLoadCleared(placeholder: Drawable?){}
                    override fun onResourceReady(bitmap: Bitmap,
                        transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?){

                        img_myphoto.setImageBitmap(bitmap)
                        ConvertImageBase64(bitmap, typeImage!!)
                    }
                })

            flag_img = 1
        }
    }

    fun ConvertImageBase64(bitmap: Bitmap, extension: String){

        val baoss = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baoss)
        val data = baoss.toByteArray()

        val codeimg = Base64.encodeToString(data, Base64.DEFAULT)
        encodedImage = "data:${extension};base64,${codeimg}"
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}