package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Agenda.Agenda
import com.example.appnetworking.AgendaEvento.AgendaEventoView
import com.example.appnetworking.AgendaEvento.AgendaEventoView.Companion.asistentes
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.ResponseActividadesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseAgendaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.R

class AgendaAdapter: RecyclerView.Adapter<AgendaAdapter.ViewHolder>(){

    var list_expe: List<ResponseAgendaOrder>? = null
    lateinit var contexto: Context

    fun AgendaAdapter(lista: List<ResponseAgendaOrder>, context: Context){
        list_expe = lista
        this.contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val hora = holder.itemView.findViewById(R.id.txt_hora) as TextView
        val asunto = holder.itemView.findViewById(R.id.txt_asunto) as TextView
        val lugar = holder.itemView.findViewById(R.id.txt_lugar) as TextView

        hora.text = "${list_expe!![position].inicio_hora} - ${list_expe!![position].hora_fin}"
        asunto.text = list_expe!![position].titulo
        lugar.text = list_expe!![position].location

        holder.itemView.setOnClickListener{view ->
            val intent = Intent(contexto, AgendaEventoView::class.java)
            intent.putExtra("ID_CITA", list_expe!![position].id)
            asistentes = list_expe!![position].attendants
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_agenda, parent, false))
    }

    override fun getItemCount(): Int {
        return list_expe!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}