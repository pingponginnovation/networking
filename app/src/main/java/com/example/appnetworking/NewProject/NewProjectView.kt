package com.example.appnetworking.NewProject

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.example.appgenesys.RequestCrearProyecto
import com.example.appgenesys.ResponseCrearProyecto
import com.example.appnetworking.Adapters.NewProjectCategoriesAdapter
import com.example.appnetworking.Adapters.NewProjectCategoriesAdapter.Companion.ClearCategories
import com.example.appnetworking.Adapters.NewProjectCategoriesAdapter.Companion.category_name
import com.example.appnetworking.Adapters.NewProjectCategoriesAdapter.Companion.id_categoria_selected
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.nuevo_proyecto_activity.*
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream

class NewProjectView: AppCompatActivity(), NewProject.View{

    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: NewProject.Presenter
    private var adaptador_dropdownList: ArrayAdapter<String>? = null
    private val IMAGE_PICK_CODE = 1000;
    private val PERMISSION_CODE = 1001;
    private var encodedImage = "nothing"
    private lateinit var preferences: PreferencesController
    private var addedCategoryBtn = 0
    private var flag_category = -1

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nuevo_proyecto_activity)

        controlAlert = ControllerAlertDialog(this)
        preferences = PreferencesController(applicationContext)
        presenter = NewProjectPresenter(this, this)
        adaptador_dropdownList = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)
        ConfigToolbar()
        SetCategories()

        btn_upload.setOnClickListener{
            CheckPermissionGallery()
        }

        btn_save.setOnClickListener{

            if(edt_projectname.text.isNullOrEmpty() || edt_description.text.isNullOrEmpty() ||
                encodedImage.equals("nothing") ||
                (id_categoria_selected == -1 && autocomp_more_cats.text.toString().isEmpty())){
                toast("Please fill in all fields")
            }
            else{

                if(addedCategoryBtn == 0 && id_categoria_selected == -1) toast("Select a category")
                else{
                    if(addedCategoryBtn == 1){
                        flag_category = 0
                        category_name = autocomp_more_cats.text.toString()
                    }
                    else flag_category = 1

                    val req = RequestCrearProyecto(preferences.getMyIDprofile()!!.toInt(),
                        edt_projectname.text.toString(),
                        flag_category.toString(), category_name, id_categoria_selected.toString(),
                        edt_description.text.toString(),
                        edt_urlvideo.text.toString(), encodedImage)

                    controlAlert!!.ProgressBar()
                    presenter.CrearProyecto(req)
                }
            }
        }

        btn_add.setOnClickListener{
            if(autocomp_more_cats.text.toString().isEmpty()) toast("Please type a category")
            else{
                addedCategoryBtn = 1
                Autocomplete(true)
            }
        }

        autocomp_more_cats.addTextChangedListener(yourTextWatcher)
    }

    protected var yourTextWatcher: TextWatcher = object: TextWatcher{
        override fun afterTextChanged(s: Editable){}
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int){}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int){
            if(s.isEmpty()) Autocomplete(false)
        }
    }

    fun Autocomplete(state: Boolean){
        if(state){
            ClearCategories()
            autocomp_more_cats.setTextColor(Color.WHITE)
            autocomp_more_cats.setBackgroundResource(R.drawable.design_autocomplete_selected)
            autocomp_more_cats.clearFocus()
            HideKeyboard()
        }
        else{
            if(autocomp_more_cats.text.toString().isNotEmpty() || addedCategoryBtn == 1){

                addedCategoryBtn = 0
                autocomp_more_cats.setText("")
                autocomp_more_cats.setTextColor(Color.BLACK)
                autocomp_more_cats.setBackgroundResource(R.drawable.design_autocomplete)
                autocomp_more_cats.clearFocus()
                HideKeyboard()
            }
        }
    }

    fun HideKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(currentFocus == null) return
        if(currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }

    override fun ErrorProyecto(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun SuccessProyecto(response: ResponseCrearProyecto){
        controlAlert!!.StopProgress()
        controlAlert!!.AlertSuccessCreatedProject(response.msj, this)
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
        switch_project.visibility = View.GONE
    }

    fun SetCategories(){
        val array: ArrayList<String> = intent.getStringArrayListExtra("CATEGORIES")!!
        val arrayIDS: ArrayList<String> = intent.getStringArrayListExtra("CATEGORIES_IDS")!!
        rclv_cats.setHasFixedSize(true)
        rclv_cats.layoutManager = GridLayoutManager(this, 3)
        val arrayAdapter = NewProjectCategoriesAdapter()
        arrayAdapter.NewProjectCategoriesAdapter(array, arrayIDS, this)
        rclv_cats.adapter = arrayAdapter

        ConfigAutocomplete(array)
    }

    private fun ConfigAutocomplete(categories: ArrayList<String>){

        categories.forEach{
            adaptador_dropdownList!!.add(it)
        }
        autocomp_more_cats.setThreshold(1)
        autocomp_more_cats.setAdapter(adaptador_dropdownList)
    }

    fun CheckPermissionGallery(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED){

                val permissions = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE);
                requestPermissions(permissions, PERMISSION_CODE)

            }else pickImageFromGallery()
        }else pickImageFromGallery()
    }

    private fun pickImageFromGallery(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?){
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){

            val image: Uri = data!!.data!!
            val typeImage = contentResolver.getType(image) //exmple:   image/png

            Glide.with(this)
                .asBitmap()
                .load(image)
                .into(object : CustomTarget<Bitmap>(1500, 1000){
                    override fun onLoadCleared(placeholder: Drawable?){}
                    override fun onResourceReady(bitmap: Bitmap,
                                                 transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?){

                        img_project.setImageBitmap(bitmap)
                        ConvertImageBase64(bitmap, typeImage!!)
                    }
                })
        }
    }

    fun ConvertImageBase64(bitmap: Bitmap, extension: String){
        val baoss = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baoss)
        val data = baoss.toByteArray()
        val codeimg = Base64.encodeToString(data, Base64.DEFAULT)
        encodedImage = "data:${extension};base64,${codeimg}"
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause(){
        super.onPause()
    }
}