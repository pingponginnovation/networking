package com.example.appnetworking.AgendaEvento

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.RequestJoinStartMeeting
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.*
import com.example.appnetworking.Adapters.ChipsContactoEventoNuevo
import com.example.appnetworking.ModelsRetrofit.ResponseAgendaAsistentesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseContactsOrder
import com.example.appnetworking.ModelsRetrofit.ResponseDatosCitaAsistentesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseDatosCitaContactosOrder
import com.example.appnetworking.Zoom.InMeeting.customizedmeetingui.MyMeetingActivity
import com.example.appnetworking.Zoom.InMeeting.customizedmeetingui.view.MeetingWindowHelper
import com.example.appnetworking.Zoom.InitAuthSDKCallback
import com.example.appnetworking.Zoom.InitAuthSDKHelper
import com.example.appnetworking.Zoom.StartJoinMeeting.apiuser.ApiUserStartMeetingHelper
import com.example.appnetworking.Zoom.StartJoinMeeting.joinmeetingonly.JoinMeetingHelper
import kotlinx.android.synthetic.main.dialog_change_hour_date.view.*
import kotlinx.android.synthetic.main.dialog_pickdate.view.*
import kotlinx.android.synthetic.main.evento_agenda_activity.*
import org.jetbrains.anko.toast
import us.zoom.sdk.*
import us.zoom.sdk.MeetingInviteMenuItem.MeetingInviteAction
import java.text.SimpleDateFormat
import java.util.*

class AgendaEventoView: AppCompatActivity(), AgendaEvento.View, MeetingServiceListener,
    InitAuthSDKCallback{

    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var preferences: PreferencesController
    private lateinit var presenter: AgendaEvento.Presenter
    //private var respContacts: ResponseContacts? = null
    private var contactsAdded = arrayListOf<String>()
    private var idsContacts = arrayListOf<Int>()
    val arrayAdapter = ChipsContactoEventoNuevo()
    private var caseAlert = -1
    private var enable = 0
    private var cita_id = -1
    var dateSelected: String? = null
    private var horaTimer1 = ""
    private var horatimer2 = ""
    private var actualHour = ""
    private var auxiliar = 0
    private var flagEditar = -1
    private var mbPendingStartMeeting = false
    private var mZoomSDK: ZoomSDK? = null
    private val isResumed = false
    companion object{
        var asistentes: List<ResponseAgendaAsistentesOrder> = listOf()
        var asistentesMeeting: ArrayList<ResponseDatosCitaAsistentesOrder> = arrayListOf() //From redireccion agenda
        var misContactos: ArrayList<ResponseContactsOrder> = arrayListOf() //Llenado en AgendaView
        private var misContactosMeeting: ArrayList<ResponseDatosCitaContactosOrder> = arrayListOf()
    }

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.evento_agenda_activity)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle("Details")

        controlAlert = ControllerAlertDialog(this)
        presenter = AgendaEventoPresenter(this, applicationContext)
        preferences = PreferencesController(applicationContext)

        cita_id = intent.getIntExtra("ID_CITA", -1)
        presenter.GetInfoCita(RequestDatosCita(cita_id, preferences.getMyIDprofile()!!.toInt()))
        EnableDisable(false)

        txt_time_Ev.setOnClickListener {
            ShowChangeDateTime(txt_date_Ev.text.toString())
        }

        txt_date_Ev.setOnClickListener {
            ShowChangeDateTime(txt_date_Ev.text.toString())
        }

        btn_zoom.setOnClickListener{
            controlAlert!!.ProgressBar()
            presenter.JoinStartMeeting(RequestJoinStartMeeting(cita_id))
        }

        //TODO. aqui evaluar con el perfil ID de quien creó el meeting y el perfil logueado
        //TODO. en lugar de usar FLAG_EDIT, tambien checar mi array de contactos, a ver si se agrega en el
        //TODO. servicio de getInfoCita
        btn_edit_save.setOnClickListener{

            if(flagEditar == 1){

                Log.e("IDS", idsContacts.toString())
                Log.e("ASISTENTES", contactsAdded.toString())
                Log.e("CONTACTOS", misContactos.toString())

                if(enable == 0){

                    btn_edit_save.text = "Save"
                    EnableDisable(true)
                    enable = 1
                }
                else{
                    btn_edit_save.text = "Edit"
                    EnableDisable(false)
                    enable = 0

                    if(txt_title_Ev.text.isNullOrEmpty() || txt_descrip_Ev.text.isNullOrEmpty()
                        || txt_time_Ev.text.isNullOrEmpty() || txt_location_Ev.text.isNullOrEmpty()
                        || txt_descrip_Ev.text.isNullOrEmpty()
                        || contactsAdded.isNullOrEmpty()) toast("You must complete all fields")
                    else{

                        val strs = txt_time_Ev.text.toString().trim().split("-").toTypedArray()

                        val editCita = RequestEditCita(
                            cita_id.toString(),
                            txt_title_Ev.text.toString(),
                            idsContacts,
                            txt_date_Ev.text.toString(),
                            strs[0],
                            strs[1],
                            txt_descrip_Ev.text.toString(),
                            txt_location_Ev.text.toString(),
                            caseAlert
                        )

                        //Log.e("Req", editCita.toString())

                        val retrofit = RetrofitController(this)
                        retrofit.EditCita(editCita)
                    }
                }
            }
            else toast("You aren't allowed to edit the meeting")
        }

        mZoomSDK = ZoomSDK.getInstance()
        InitAuthSDKHelper.getInstance().initSDK(this, this)
        if (mZoomSDK!!.isInitialized()) {
            ZoomSDK.getInstance().meetingService.addListener(this)
            ZoomSDK.getInstance().meetingSettingsHelper.enable720p(true)
        }

        registerListener()
    }

    var handle =
        InMeetingNotificationHandle { context: Context, intent: Intent ->
            Intent(context, MyMeetingActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
            if (context !is Activity) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            intent.action = InMeetingNotificationHandle.ACTION_RETURN_TO_CONF
            context.startActivity(intent)
            true
        }

    override fun onZoomSDKInitializeResult(errorCode: Int, internalErrorCode: Int){
        if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) toast("Failed to initialize Zoom")
        else{
            ZoomSDK.getInstance().meetingSettingsHelper.enable720p(false)
            ZoomSDK.getInstance().meetingSettingsHelper.enableShowMyMeetingElapseTime(true)
            ZoomSDK.getInstance().meetingService.addListener(this)
            ZoomSDK.getInstance().meetingSettingsHelper.setCustomizedNotificationData(null, handle)
            ZoomSDK.getInstance().zoomUIService.setZoomUIDelegate { context, inviteMenuList ->
                inviteMenuList.add(MeetingInviteMenuItem("SDK Contacts", R.drawable.zm_invite_contacts,
                        MeetingInviteAction{ _, info ->
                            Log.d("meetinginvitation", "onItemClick:" + info.topic + ":"
                                    + info.content + ":" + info.meetingUrl + ":" + info.meetingId)
                        })
                )
                false
            }
        }
    }

    override fun onMeetingStatusChanged(meetingStatus: MeetingStatus?, errorCode: Int, internalErrorCode: Int){
        Log.i("statusChanged", "onMeetingStatusChanged, meetingStatus=" + meetingStatus
                +", errorCode=" + errorCode + ", internalErrorCode=" + internalErrorCode)

        if (meetingStatus == MeetingStatus.MEETING_STATUS_FAILED && errorCode
            == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE) toast("Version of SDKZoom is too low")

        if (mbPendingStartMeeting && meetingStatus == MeetingStatus.MEETING_STATUS_IDLE)
            mbPendingStartMeeting = false

        if (meetingStatus == MeetingStatus.MEETING_STATUS_CONNECTING) showMeetingUi()

        refreshUI()
    }

    private fun showMeetingUi() {
        if (ZoomSDK.getInstance().meetingSettingsHelper.isCustomizedMeetingUIEnabled) {
            val intent = Intent(this, MyMeetingActivity::class.java)
            intent.putExtra("from", 2)
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            this.startActivity(intent)
        }
    }

    private fun refreshUI(){
        val meetingStatus = ZoomSDK.getInstance().meetingService.meetingStatus
        if (ZoomSDK.getInstance().meetingSettingsHelper.isCustomizedMeetingUIEnabled){
            if (meetingStatus == MeetingStatus.MEETING_STATUS_INMEETING && isResumed){
                MeetingWindowHelper.getInstance().showMeetingWindow(this)
            }
            else MeetingWindowHelper.getInstance().hiddenMeetingWindow(true)
        }
    }

    override fun onZoomAuthIdentityExpired(){
        if (mZoomSDK!!.isLoggedIn) {
            mZoomSDK!!.logoutZoom()
        }
    }

    private fun registerListener(){
        val meetingService = ZoomSDK.getInstance().meetingService
        meetingService?.addListener(this)
    }

    fun SetDatos(response: ResponseDatosCita){

        misContactosMeeting = response.contactos
        flagEditar = response.data.flag_editar!!
        asistentesMeeting = response.data.asistentes
        txt_title_Ev.setText(response.data.title)
        txt_date_Ev.setText(response.data.fecha)
        txt_time_Ev.setText(response.data.hora_fin + "-" + response.data.hora_fin)
        txt_location_Ev.setText(response.data.lugar)
        txt_descrip_Ev.setText(response.data.descripcion)
        val alert = response.data.alerta
        caseAlert = alert!!.toInt() //Guardamos el alert que tiene seleccionado, en caso de que no cambie mandamos el mismo
        SetUpSpinnerAlert(alert)
        SetUpSpinnerContacts()
        InitRecyclerContactos()

        if(flagEditar == 1) btn_zoom.setText("Start meeting")
        else btn_zoom.text = "Join meeting"
    }

    private fun SaveHourMeeting(horacomplete: String){
        val hour = horacomplete.trim().split("-").toTypedArray()//Ex: 12:30-2:30
        if(actualHour.isEmpty()) actualHour = hour[0]
        else if(hour[0] >= actualHour) actualHour = hour[0]
    }

    fun ShowChangeDateTime(fecha: String){
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog: AlertDialog? = null
        val mView: View = layoutInflater.inflate(R.layout.dialog_change_hour_date, null)

        dateSelected = fecha
        SaveHourMeeting(txt_time_Ev.text.toString())
        SetUpSpinnerTime(mView, txt_time_Ev.text.toString())

        mView.btn_save_change.setOnClickListener{
            txt_date_Ev.text = dateSelected
            val newTime = "${horaTimer1}-${horatimer2}"
            txt_time_Ev.text = newTime
            dialog!!.dismiss()
        }

        mView.txt_date_change.setText(dateSelected)
        mView.txt_date_change.setOnClickListener{
            ShowCalendar(mView.txt_date_change, mView)
        }

        mBuilder.setView(mView)
        dialog = mBuilder.create()
        dialog.show()
    }

    fun GetDatePhone(): String{
        val sdf = SimpleDateFormat("yyy-MM-dd")
        val currentDate = Date() //Fecha actual del celular
        val datePhone = sdf.format(currentDate)
        return datePhone
    }

    fun GetActualHour(): String{
        val currentDate = Date() //Fecha actual del celular y hora
        val hdf = SimpleDateFormat("HH:mm")
        val hour = hdf.format(currentDate)
        Log.e("hora", hour)
        return hour
    }

    fun ShowCalendar(date_field: TextView, mVieww: View){

        val sdf = SimpleDateFormat("yyy-MM-dd")
        //val currentDate = Date() //Fecha actual del celular
        //val datePhone = sdf.format(currentDate)

        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        var dialog: AlertDialog? = null
        val mView: View = layoutInflater.inflate(R.layout.dialog_pickdate, null)

        dateSelected = sdf.format(Date(mView.calendar_Ev.date))
        mView.calendar_Ev.setOnDateChangeListener{view, year, month, dayOfMonth ->
            dateSelected = sdf.format(Date(year - 1900, month, dayOfMonth))
        }

        mView.btn_save_date_Ev.setOnClickListener{

            if(dateSelected.toString() >= GetDatePhone()){

                auxiliar = 1
                GetActualHour()
                date_field.text = dateSelected
                dialog!!.dismiss()
                if(dateSelected.toString() > GetDatePhone()) SetUpSpinnerTimerGeneral(mVieww)
                else SetUpSpinnerTime(mVieww, txt_time_Ev.text.toString())

            }else toast("Date must be equals or greater")
        }

        mBuilder.setView(mView)
        dialog = mBuilder.create()
        dialog.show()
    }

    fun SetUpSpinnerTimerGeneral(mView: View){

        val options = arrayListOf<String>()
        var done = false
        var hora = 0
        var minuto = 0
        var minutoS = ""

        do{
            val horaS = hora.toString()
            if(minuto == 0) minutoS = "00"
            else minutoS = minuto.toString()

            val hour = "${horaS}:${minutoS}"
            if(hour.equals("23:30")){
                options.add(hour)
                done = true
            }
            else{
                options.add(hour)
                if(minutoS.equals("30")){
                    hora += 1
                    minuto = 0
                }else minuto = 30
            }
        }while(done == false)

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.spin_time_change.adapter = dataAdapter

        mView.spin_time_change.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){
                horaTimer1 = mView.spin_time_change.getItemAtPosition(position).toString()
                SetUpSpinnerTimeEnd(mView, horaTimer1)
                horaTimer1 = SplitHora(horaTimer1)
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        }
    }

    fun SetUpSpinnerTime(mView: View, horaComplete: String){

        val options = arrayListOf<String>()
        var done = false
        var minutoS = ""
        var minuto = 0

        if(auxiliar == 1){

            val horaIndex = GetActualHour().trim().split(":").toTypedArray() //Ex: 12:30 [0]:[1]
            var horaActual = horaIndex[0].toInt()
            val minuteActual = horaIndex[1].toInt()

            if(minuteActual in 30..59){
                minuto = 0
                horaActual += 1
            }
            else minuto = 30

            do{
                val horaS = horaActual.toString()
                if(minuto == 0) minutoS = "00"
                else minutoS = minuto.toString()

                val hour = "${horaS}:${minutoS}"
                if(hour.equals("23:30")){
                    options.add(hour)
                    done = true
                }
                else{
                    options.add(hour)
                    if(minutoS.equals("30")){
                        horaActual += 1
                        minuto = 0
                    }else minuto = 30
                }
            }while(done == false)
        }
        else{

            val horaBegin = horaComplete.trim().split("-").toTypedArray() //Ex: 12:30-2:30
            val horaIndex = actualHour.trim().split(":").toTypedArray() //Ex: 12:30 [0]:[1]
            //horaIndex[0] = 12  horaIndex[1] = 30
            var horaActual = horaIndex[0].toInt()
            var minuteActual = horaIndex[1].toInt()

            do{
                val horaS = horaActual.toString()
                if(minuteActual == 0) minutoS = "00"
                else minutoS = minuteActual.toString()

                val hour = "${horaS}:${minutoS}"
                if(hour.equals("23:30")){
                    options.add(hour)
                    done = true
                }
                else{
                    options.add(hour)
                    if(minutoS.equals("30")){
                        horaActual += 1
                        minuteActual = 0
                    }else minuteActual = 30
                }
            }while(!done) //mientras sea falso
        }

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.spin_time_change.adapter = dataAdapter

        mView.spin_time_change.onItemSelectedListener = object: OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){
                horaTimer1 = mView.spin_time_change.getItemAtPosition(position).toString()
                SetUpSpinnerTimeEnd(mView, horaTimer1)
                Log.e("hora1", horaTimer1)
                horaTimer1 = SplitHora(horaTimer1)
                Log.e("afterSplit", horaTimer1)
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        }
    }

    fun SetUpSpinnerTimeEnd(mView: View, lahora: String){

        val strs = lahora.trim().split(":").toTypedArray()
        val options = arrayListOf<String>()
        var done = false
        //TODO. we put the next half hour , exmple: we received 16:30, time end will be 17:00
        var hora = strs[0].toInt()
        var minuto: Int

        if(strs[1].equals("00")) minuto = 30
        else{
            hora += 1
            minuto = 0
        }
        var minutoS = ""

        do{
            val horaS = hora.toString()
            if(minuto == 0) minutoS = "00"
            else minutoS = minuto.toString()

            val hour = "${horaS}:${minutoS}"
            if(hour.equals("23:30")){
                options.add(hour)
                done = true
            }
            else{
                options.add(hour)
                if(minutoS.equals("30")){
                    hora += 1
                    minuto = 0
                }else minuto = 30
            }
        }while(!done)

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mView.spin2_time_change.adapter = dataAdapter

        mView.spin2_time_change.setOnItemSelectedListener(object: OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){

                horatimer2 = mView.spin2_time_change.getItemAtPosition(position).toString()
                horatimer2 = SplitHora(horatimer2)
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        })
    }

    fun SplitHora(hora: String): String{

        var min = ""
        val array = hora.trim().split(":").toTypedArray()

        if(array[0].toInt() in 0..9){
            min = array[0]
            min = "0${min}"
        }else min = array[0]

        return "${min}:${array[1]}"
    }

    fun EnableDisable(value: Boolean){
        txt_title_Ev.isEnabled = value
        autocomplete_contacts_Ev.isEnabled = value
        txt_date_Ev.isEnabled= value
        txt_time_Ev.isEnabled = value
        txt_location_Ev.isEnabled = value
        txt_descrip_Ev.isEnabled = value
        spinner_alerts_Ev.isEnabled = value
    }

    fun SetUpSpinnerAlert(alert: String) {

        val options = arrayListOf<String>()
        val caseSelected = GetCaseSelected(alert.toInt())
        options.add("Nunca")
        options.add("En el momento del evento")
        options.add("5 minutos antes")
        options.add("15 minutos antes")
        options.add("30 minutos antes")
        options.add("1 hora antes")
        options.add("2 horas antes")
        options.add("12 horas antes")
        options.add("1 dia antes")
        options.add("1 semana antes")

        if(options.contains(caseSelected)){
            options.remove(caseSelected)
            options.add(0, caseSelected) //Para que quede al inicio del spinner la opcion
        }

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(this,
            android.R.layout.simple_spinner_item, options)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_alerts_Ev.adapter = dataAdapter

        spinner_alerts_Ev.setOnItemSelectedListener(object: OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long){

                val option = spinner_alerts_Ev.getItemAtPosition(position).toString()
                when(option){
                    "Nunca" -> caseAlert = 0
                    "En el momento del evento" -> caseAlert = 1
                    "5 minutos antes" -> caseAlert = 2
                    "15 minutos antes" -> caseAlert = 3
                    "30 minutos antes" -> caseAlert = 4
                    "1 hora antes" -> caseAlert = 5
                    "2 horas antes" -> caseAlert = 6
                    "12 horas antes" -> caseAlert = 7
                    "1 dia antes" -> caseAlert = 8
                    "1 semana antes" -> caseAlert = 9
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?){}
        })
    }

    fun GetCaseSelected(case: Int): String{
        when(case){
            0 -> return "Nunca"
            1 -> return "En el momento del evento"
            2 -> return "5 minutos antes"
            3 -> return "15 minutos antes"
            4 -> return "30 minutos antes"
            5 -> return "1 hora antes"
            6 -> return "2 horas antes"
            7 -> return "12 horas antes"
            8 -> return "1 dia antes"
            9 -> return "1 semana antes"
        }
        return ""
    }

    fun InitRecyclerContactos(){

        if(asistentes.isNotEmpty()){
            asistentes.forEach{item->
                //Guardamos en los arrays los contactos y sus ids, que estan invitados al evento.
                contactsAdded.add(item.name.toString()+" "+item.apellidos)
                idsContacts.add(item.id!!)
            }
        }
        else{
            asistentesMeeting.forEach{item->
                //Guardamos en los arrays los contactos y sus ids, que estan invitados al evento.
                contactsAdded.add(item.name.toString()+" "+item.apellidos)
                idsContacts.add(item.id_perfil!!)
            }
        }

        rclv_contacts_Ev.setHasFixedSize(true)
        rclv_contacts_Ev.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL,
            false)
        //Utilizo el mismo adapter de crear nuevo evento.
        arrayAdapter.ChipsContactoEventoNuevo(contactsAdded, this, idsContacts, flagEditar)
        rclv_contacts_Ev.adapter = arrayAdapter
    }

    fun SetUpSpinnerContacts(){

        val adaptador_dropdownList: ArrayAdapter<String> =
            ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line)

        if(misContactos.isNotEmpty()){
            misContactos.forEach{item->
                if(!item.nombre.isNullOrEmpty()) adaptador_dropdownList.add(item.nombre+" "+item.apellidos)
            }
        }
        else{
            misContactosMeeting.forEach{item->
                if(!item.name.isNullOrEmpty()) adaptador_dropdownList.add(item.name+" "+item.apellidos)
            }
        }

        autocomplete_contacts_Ev!!.setThreshold(1)
        autocomplete_contacts_Ev!!.setAdapter(adaptador_dropdownList)
        autocomplete_contacts_Ev!!.addTextChangedListener(object: TextWatcher{

            override fun afterTextChanged(s: Editable?){}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int){

                if(autocomplete_contacts_Ev!!.isPerformingCompletion){
                    GetContactID(autocomplete_contacts_Ev!!.text.toString())
                }
            }
        })
        autocomplete_contacts_Ev!!.setOnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_NEXT) txt_location_Ev.requestFocus()
            true
        }
    }

    fun GetContactID(name: String){

        var id: Int? = null
        if(misContactos.isNotEmpty()){
            misContactos.forEach{item->
                val fullName = item.nombre+" "+item.apellidos
                if(name == fullName) id = item.id
            }
        }
        else{
            misContactosMeeting.forEach{item->
                val fullName = item.name+" "+item.apellidos
                if(name == fullName) id = item.id
            }
        }

        AddNewContactToEvent(name, id!!)
    }

    fun AddNewContactToEvent(name: String, id: Int){

        if(contactsAdded.contains(name)) toast("You have added this contact before")
        else{
            idsContacts.add(id)
            contactsAdded.add(name)
            arrayAdapter.notifyDataSetChanged()
        }
    }

    override fun ErrorCita(resp: String){
        toast(resp)
    }

    override fun SuccessInfo(response: ResponseDatosCita){
        SetDatos(response)
    }

    override fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting){

        controlAlert!!.StopProgress()

        val zoomSDK = ZoomSDK.getInstance()
        if (!zoomSDK.isInitialized){
            toast("Zoom SDK hasn't been initialized successfully")
            return
        }

        if(flagEditar == 1){

                //Start meeting
            val meetingService = zoomSDK.meetingService
            if (meetingService.meetingStatus != MeetingStatus.MEETING_STATUS_IDLE){

                var lMeetingNo: Long = 0

                lMeetingNo = try{ lMeetingNo
                }catch (e: NumberFormatException){
                    toast("Invalid meeting number")
                    return
                }

                if (meetingService.currentRtcMeetingNumber == lMeetingNo){
                    meetingService.returnToMeeting(this)
                    return
                }

                android.app.AlertDialog.Builder(this)
                    .setMessage("Do you want to leave current meeting and start another?")
                    .setPositiveButton("Yes"){dialog, which ->
                        mbPendingStartMeeting = true
                        meetingService.leaveCurrentMeeting(false)
                    }
                    .setNegativeButton("No") { dialog, which -> }
                    .show()
                return
            }

            val ret = ApiUserStartMeetingHelper.getInstance()
                .StartMeetingWithNumber(this, response.data_meeting.id_meeting,
                    response.data_meeting.user_ID, response.data_meeting.token_zoom,
                    response.data_meeting.token_access_zoom, preferences.getNameProfile())
        }
        else{
                //Join meeting
            val ret = JoinMeetingHelper.getInstance().joinMeetingWithNumber(this,
                response.data_meeting.id_meeting, response.data_meeting.password, preferences.getNameProfile())
        }
    }

    override fun ErrorJoinStartMeeting(resp: String){
        controlAlert!!.StopProgress()
        toast(resp)
    }

    override fun onDestroy() {
        val zoomSDK = ZoomSDK.getInstance()
        if (zoomSDK.isInitialized) {
            val meetingService = zoomSDK.meetingService
            meetingService.removeListener(this) //unregister meetingServiceListener
        }
        MeetingWindowHelper.getInstance().removeOverlayListener()
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}