package com.example.appnetworking.ModelsRetrofit

import com.google.gson.annotations.SerializedName

data class ResponseProfileExperienciasOrder(

    @field:SerializedName("experiencia_id")
    val id: Int? = null,

    @field:SerializedName("logo")
    val logo: String? = null,

    @field:SerializedName("cargo")
    val cargo: String? = null,

    @field:SerializedName("tipo_empleo_id")
    val id_empleo: Int? = null,

    @field:SerializedName("tipo_empleo")
    val type_job: String? = null,

    @field:SerializedName("empresa_id")
    val id_empresa: Int? = null,

    @field:SerializedName("empresa")
    val empresa: String? = null,

    @field:SerializedName("ubicacion")
    val location: String? = null,

    @field:SerializedName("actualmente")
    val actualmente: Int? = null,

    @field:SerializedName("fecha_inicio")
    val date_start: String? = null,

    @field:SerializedName("fecha_fin")
    val date_end: String? = null,

    @field:SerializedName("fecha_inicio_normal")
    val date_start_numbers: String? = null,

    @field:SerializedName("fecha_fin_normal")
    val date_end_numbers: String? = null,

    @field:SerializedName("tiempo_experiencia")
    val year_expe: String? = null,

    @field:SerializedName("descripcion")
    val description: String? = null
)