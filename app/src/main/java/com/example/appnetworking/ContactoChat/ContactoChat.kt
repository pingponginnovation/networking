package com.example.appnetworking.ContactoChat

import com.example.appgenesys.RequestEnviarMensaje
import com.example.appgenesys.ResponseChatThread
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.ResponseEnviarMensaje

interface ContactoChat{

    interface View{
        fun ErrorMensajes(resp: String)
        fun SuccessMensajes(response: ResponseChatThread)
        fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje)
        fun ErrorEnviarMensaje(resp: String)
    }

    interface Presenter{
        fun GetMensajes(id: Int)
        fun ErrorMensajes(resp: String)
        fun SuccessMensajes(response: ResponseChatThread)
        fun EnviarMensaje(request: RequestEnviarMensaje)
        fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje)
        fun ErrorEnviarMensaje(resp: String)
    }

    interface Model{
        fun GetMensajes(id: Int)
        fun ErrorMensajes(resp: String)
        fun SuccessMensajes(response: ResponseChatThread)
        fun EnviarMensaje(request: RequestEnviarMensaje)
        fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje)
        fun ErrorEnviarMensaje(resp: String)
    }
}