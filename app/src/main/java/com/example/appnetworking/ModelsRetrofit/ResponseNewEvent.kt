//package com.example.appnetworking
package com.example.appgenesys

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseNewEvent(
    @SerializedName("mensaje")
    val msj: String? = null
)