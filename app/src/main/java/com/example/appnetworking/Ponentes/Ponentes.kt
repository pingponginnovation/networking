package com.example.appnetworking.Ponentes

import com.example.appnetworking.ResponseSpeakers

interface Ponentes{

    interface View{
        fun ErrorPonentes(resp: String)
        fun SuccessPonentes(response: ResponseSpeakers)
    }

    interface Presenter{
        fun GetSpeakers()
        fun ErrorPonentes(resp: String)
        fun SuccessPonentes(response: ResponseSpeakers)
    }

    interface Model{
        fun GetSpeakers()
        fun ErrorPonentes(resp: String)
        fun SuccessPonentes(response: ResponseSpeakers)
    }
}