package com.example.appnetworking.Profile

import com.example.appgenesys.*
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.RequestCrearExperiencia
import com.example.appnetworking.RequestEditBiography
import com.example.appnetworking.RequestModificarExperiencia
import java.lang.Error

interface Profile{

    interface View{
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfile)

        fun SuccessCrearExperiencia(response: ResponseCrearExperiencia)
        fun ErrorCrearExperiencia(msj: String)

        fun SucccessEdit(resp: ResponseEditExperiencia)
        fun ErrorEdit(msj: String)

        fun SuccessEditBiography(response: ResponseEditBiography)
        fun ErrorEditBiography(resp: String)

        fun SuccessDeleteInteres(resp: ResponseDeleteInteres)
        fun ErrorDeleteInteres(resp: String)

        fun SuccessInterest(response: ResponseCrearInteres)
        fun ErrorInteres(resp: String)

        fun SuccessSkill(response: ResponseCrearSkill)
        fun ErrorSkill(resp: String)

        fun SuccessDeleteSkill(resp: ResponseDeleteSkill)
        fun ErrorDeleteSkill(resp: String)
    }

    interface Presenter{
        fun GetProfile()
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfile)

        fun CrearExperiencia(req: RequestCrearExperiencia)
        fun SuccessCrearExperiencia(response: ResponseCrearExperiencia)
        fun ErrorCrearExperiencia(msj: String)

        fun EditExperience(req: RequestModificarExperiencia)
        fun SucccessEdit(resp: ResponseEditExperiencia)
        fun ErrorEdit(msj: String)

        fun EditBiography(req: RequestEditBiography)
        fun SuccessEditBiography(response: ResponseEditBiography)
        fun ErrorEditBiography(resp: String)

        fun DeleteInteres(req: RequestDeleteInteres)
        fun SuccessDeleteInteres(resp: ResponseDeleteInteres)
        fun ErrorDeleteInteres(resp: String)

        fun CreateInteres(req: RequestCrearInteres)
        fun SuccessInterest(response: ResponseCrearInteres)
        fun ErrorInteres(resp: String)

        fun CreateSkill(req: RequestCrearSkill)
        fun SuccessSkill(response: ResponseCrearSkill)
        fun ErrorSkill(resp: String)

        fun DeleteSkill(req: RequestDeleteSkill)
        fun SuccessDeleteSkill(resp: ResponseDeleteSkill)
        fun ErrorDeleteSkill(resp: String)
    }

    interface Model{
        fun GetProfile()
        fun ErrorProfile(resp: String)
        fun SuccessProfile(response: ResponseProfile)

        fun CrearExperiencia(req: RequestCrearExperiencia)
        fun SuccessCrearExperiencia(response: ResponseCrearExperiencia)
        fun ErrorCrearExperiencia(msj: String)

        fun EditExperience(req: RequestModificarExperiencia)
        fun SucccessEdit(resp: ResponseEditExperiencia)
        fun ErrorEdit(msj: String)

        fun EditBiography(req: RequestEditBiography)
        fun SuccessEditBiography(response: ResponseEditBiography)
        fun ErrorEditBiography(resp: String)

        fun DeleteInteres(req: RequestDeleteInteres)
        fun SuccessDeleteInteres(resp: ResponseDeleteInteres)
        fun ErrorDeleteInteres(resp: String)

        fun CreateInteres(req: RequestCrearInteres)
        fun SuccessInterest(response: ResponseCrearInteres)
        fun ErrorInteres(resp: String)

        fun CreateSkill(req: RequestCrearSkill)
        fun SuccessSkill(response: ResponseCrearSkill)
        fun ErrorSkill(resp: String)

        fun DeleteSkill(req: RequestDeleteSkill)
        fun SuccessDeleteSkill(resp: ResponseDeleteSkill)
        fun ErrorDeleteSkill(resp: String)
    }
}