package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosCategoriasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosOrder
import com.google.gson.annotations.SerializedName

data class ResponseProyectos(

    @SerializedName("datos")
    val data: ArrayList<ResponseProyectosOrder>,

    @SerializedName("categorias")
    val categories: ArrayList<ResponseProyectosCategoriasOrder>
)