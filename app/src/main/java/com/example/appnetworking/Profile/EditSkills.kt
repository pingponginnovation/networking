package com.example.appnetworking.Profile

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appgenesys.*
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.Adapters.InterestsToEditAdapter
import com.example.appnetworking.Adapters.SkillsToEditAdapter
import com.example.appnetworking.ControllerAlertDialog
import com.example.appnetworking.Models.InteresesForEdit
import com.example.appnetworking.Models.SkillsForEdit
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.edit_biography.back_button
import kotlinx.android.synthetic.main.edit_intereses.*
import kotlinx.android.synthetic.main.edit_skills.*
import org.jetbrains.anko.toast

class EditSkills: AppCompatActivity(), Profile.View{

    private lateinit var preferences: PreferencesController
    private var controlAlert: ControllerAlertDialog? = null
    private lateinit var presenter: Profile.Presenter
    private var arraySkills: ArrayList<SkillsForEdit> = arrayListOf()
    private var arraySkillsString: ArrayList<String> = ArrayList()
    private var name_deleted = ""
    private var arrayAdapter = SkillsToEditAdapter()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_skills)

        presenter = ProfilePresenter(this, applicationContext)
        controlAlert = ControllerAlertDialog(this)
        ConfigToolbar()
        preferences = PreferencesController(this)
        GetExtras()

        btn_add_skill.setOnClickListener{
            if(autocomp_add_skill.text.toString().isEmpty()) toast("Field can't be empty")
            else{
                val exist = CheckExistSkill(autocomp_add_skill.text.toString())
                if(exist) toast("That skill already exist")
                else{
                    controlAlert!!.ProgressBar()
                    presenter.CreateSkill(RequestCrearSkill(preferences.getMyIDprofile()!!.toInt(),
                        autocomp_add_skill.text.toString())
                    )
                }
            }
        }
    }

    fun GetExtras(){
        val serializable = intent.getSerializableExtra("SKILLS")
        arraySkills = serializable as ArrayList<SkillsForEdit>
        arraySkills.forEach{
            arraySkillsString.add(it.name)
        }

        rclv_edit_skill.setHasFixedSize(true)
        rclv_edit_skill.layoutManager = GridLayoutManager(this, 3)
        arrayAdapter = SkillsToEditAdapter()
        arrayAdapter.SkillsToEditAdapter(arraySkillsString, arraySkills, this)
        rclv_edit_skill.adapter = arrayAdapter
    }

    private fun CheckExistSkill(skill: String): Boolean{
        arraySkillsString.forEach{
            if(it.equals(skill, true)) return true
        }
        return false
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    private fun UpdateRecycler(skill: String, id_skill: Int){
        arraySkillsString.add(skill)
        arraySkills.add(SkillsForEdit(skill, id_skill))
        arrayAdapter.notifyDataSetChanged()
    }

    fun DeleteSkill(id_skill: Int, name: String){
        name_deleted = name
        controlAlert!!.ProgressBar()
        presenter.DeleteSkill(RequestDeleteSkill(preferences.getMyIDprofile()!!.toInt(), id_skill))
    }

    override fun SuccessDeleteSkill(resp: ResponseDeleteSkill){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp.msj)

        var index = -1
        arraySkillsString.forEach{
            if(it == name_deleted) index = arraySkillsString.indexOf(it)
        }

        if(index != -1){
            arraySkillsString.removeAt(index)
            arraySkills.removeAt(index)
            arrayAdapter.notifyDataSetChanged()
        }
    }

    override fun ErrorDeleteSkill(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun SuccessSkill(response: ResponseCrearSkill){
        UpdateRecycler(autocomp_add_skill.text.toString(), response.skill_id!!)
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(response.msj!!)
    }
    override fun ErrorSkill(resp: String){
        controlAlert!!.StopProgress()
        controlAlert!!.ShowAlert(resp)
    }

    override fun onPause(){
        super.onPause()
        arraySkillsString.clear()
    }


    override fun ErrorProfile(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessProfile(response: ResponseProfile) {
        TODO("Not yet implemented")
    }
    override fun SuccessCrearExperiencia(response: ResponseCrearExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorCrearExperiencia(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SucccessEdit(resp: ResponseEditExperiencia) {
        TODO("Not yet implemented")
    }
    override fun ErrorEdit(msj: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessEditBiography(response: ResponseEditBiography){
    }
    override fun ErrorEditBiography(resp: String){
    }
    override fun SuccessInterest(response: ResponseCrearInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorInteres(resp: String) {
        TODO("Not yet implemented")
    }
    override fun SuccessDeleteInteres(resp: ResponseDeleteInteres) {
        TODO("Not yet implemented")
    }
    override fun ErrorDeleteInteres(resp: String) {
        TODO("Not yet implemented")
    }
}