package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseJoinStartMeetingOrder(
    @field:SerializedName("meeting_ig")
    val id_meeting: String? = null,

    @field:SerializedName("pwd_encrypted")
    val pass_encrypted: String? = null,

    @field:SerializedName("pwd")
    val password: String? = null,

    @field:SerializedName("userId")
    val user_ID: String? = null,

    @field:SerializedName("token")
    val token_zoom: String? = null,

    @field:SerializedName("token_zak")
    val token_access_zoom: String? = null
)