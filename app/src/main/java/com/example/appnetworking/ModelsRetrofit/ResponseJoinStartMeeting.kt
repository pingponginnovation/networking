package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseJoinStartMeeting(
    @SerializedName("mensaje")
    val msj: String,

    @SerializedName("data")
    val data_meeting: ResponseJoinStartMeetingOrder
)