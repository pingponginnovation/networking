package com.example.appnetworking

data class RequestRespuestaCita(val cita_id: Int, val perfil_id: Int, val respuesta: Int)