//package com.example.appnetworking
package com.example.appgenesys

import com.google.gson.annotations.SerializedName

data class ResponseDeleteSkill(
    @SerializedName("mensaje")
    val msj: String
)