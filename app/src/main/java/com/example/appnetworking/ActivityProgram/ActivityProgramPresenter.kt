package com.example.appnetworking.ActivityProgram

import android.content.Context
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appgenesys.Retrofit.ResponseActividades
import com.example.appnetworking.RequestSuscribeConference

class ActivityProgramPresenter(viewMatch: ActivityProgram.View, ctx: Context): ActivityProgram.Presenter{

    private val view = viewMatch
    private val model = ActivityProgramModel(this, ctx)

    //MODEL
    override fun GetListActivities(){
        model.GetListActivities()
    }

    override fun SubscribeProgram(req: RequestSuscribeConference){
        model.SubscribeProgram(req)
    }
    //VIEW
    override fun ErrorActivities(resp: String){
        view.ErrorActivities(resp)
    }

    override fun SuccessActivities(response: ResponseActividades){
        view.SuccessActivities(response)
    }

    override fun SuccessSubscribed(resp: ResponseSuscribeConference){
        view.SuccessSubscribed(resp)
    }

    override fun ErrorSubscribed(resp: String){
        view.ErrorSubscribed(resp)
    }
}