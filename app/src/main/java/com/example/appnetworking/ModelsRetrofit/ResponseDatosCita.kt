package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseDatosCita(
    @SerializedName("data")
    val data: ResponseDatosCitaOrder,

    @SerializedName("contactos")
    val contactos: ArrayList<ResponseDatosCitaContactosOrder>
)