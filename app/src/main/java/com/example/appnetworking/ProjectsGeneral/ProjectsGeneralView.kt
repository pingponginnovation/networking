package com.example.appnetworking.ProjectsGeneral

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.*
import com.example.appnetworking.Adapters.ProyectosAdapter
import com.example.appnetworking.Adapters.ProyectosGeneralAdapter
import com.example.appnetworking.Adapters.ProyectosGeneralCategoriasAdapter
import com.example.appnetworking.Adapters.ProyectosGeneralCategoriasAdapter.Companion.selectedCategories
import com.example.appnetworking.Matches.MatchesView
import com.example.appnetworking.Matches.MatchesView.Companion.type
import com.example.appnetworking.ModelsRetrofit.ResponseProyectosGeneralCategoriesOrder
import com.example.appnetworking.NewProject.NewProjectView
import com.example.appnetworking.Proyecto.EditProyectView
import com.example.appnetworking.Proyecto.EditProyectView.Companion.responseEditado
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.projects_list_fragment.*
import kotlinx.android.synthetic.main.projects_list_fragment.view.*
import kotlinx.android.synthetic.main.proyectos_activity.*
import kotlinx.android.synthetic.main.speaker_profile.*
import kotlinx.coroutines.selects.select
import org.jetbrains.anko.toast

class ProjectsGeneralView(autocomplete: AutoCompleteTextView): Fragment(), ProjectsGeneral.View{

    private var preferences: PreferencesController? = null
    private var autocompleteProjects = autocomplete
    private lateinit var presenter: ProjectsGeneral.Presenter
    private var adaptador_dropdownList: ArrayAdapter<String>? = null

    companion object{
        var categoriesOn = false //Usado en ProyectosGeneralCategoriasAdapter
        var categoriasArray = arrayListOf<String>() //Usado en NewProject
        var IDScategoriasArray = arrayListOf<String>() //Este igual
        var categoriesArray2: ArrayList<ResponseProyectosGeneralCategoriesOrder> = ArrayList()//Usado en EditProyectView
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View{
        val v: View = inflater.inflate(R.layout.projects_list_fragment, container, false)

        preferences = PreferencesController(context!!)
        presenter = ProjectsGeneralPresenter(this, context!!)
        adaptador_dropdownList = ArrayAdapter(context!!, android.R.layout.simple_dropdown_item_1line)
        GetProjects()

        v.btn_new_project.setOnClickListener{
            val intent = Intent(context, NewProjectView::class.java)
            intent.putStringArrayListExtra("CATEGORIES", categoriasArray)
            intent.putStringArrayListExtra("CATEGORIES_IDS", IDScategoriasArray)
            startActivity(intent)
        }

        return v
    }

    fun GetProjects(){
        val request = RequestProyectosGeneral(0, "", selectedCategories)
        presenter.GetProyectos(request)
    }

    override fun ErrorProyectos(resp: String){
        context!!.toast(resp)
    }

    override fun SuccessProyectos(response: ResponseProyectosGeneral){
        FillAutocomplete(response)
        SetProyectos(response)
        if(!categoriesOn) SetCategories(response.categories)
    }

    private fun FillAutocomplete(response: ResponseProyectosGeneral){

        categoriesArray2 = response.categories
        adaptador_dropdownList!!.clear()

        response.categories.forEach{
            adaptador_dropdownList!!.add("${it.categoria} (category)")
            categoriasArray.add(it.categoria!!)
            IDScategoriasArray.add(it.id_categorie!!.toString())
        }
        response.data.forEach{
            adaptador_dropdownList!!.add(it.titulo)
        }
        ConfigAutoComplete()
    }

    fun SetProyectos(response: ResponseProyectosGeneral){
        rclv_all_projects.setHasFixedSize(true)
        rclv_all_projects.layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        val arrayAdapter = ProyectosGeneralAdapter()
        arrayAdapter.ProyectosGeneralAdapter(response.data, context!!)
        rclv_all_projects.adapter = arrayAdapter
    }

    fun SetCategories(datos: ArrayList<ResponseProyectosGeneralCategoriesOrder>){
        rclv_categories.setHasFixedSize(true)
        //rclv_categories.layoutManager = GridLayoutManager(context!!, 3)
        rclv_categories.layoutManager = LinearLayoutManager(context!!, RecyclerView.HORIZONTAL, false)
        val arrayAdapter = ProyectosGeneralCategoriasAdapter()
        arrayAdapter.ProyectosGeneralCategoriasAdapter(datos, context!!, presenter)
        rclv_categories.adapter = arrayAdapter
    }

    fun ConfigAutoComplete(){
        autocompleteProjects.setHint("Search project or category")
        autocompleteProjects.setThreshold(1)
        autocompleteProjects.setAdapter(adaptador_dropdownList)
        autocompleteProjects.setOnEditorActionListener{v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                HideKeyboard()
                autocompleteProjects.dismissDropDown()
                presenter.GetProyectos(RequestProyectosGeneral(1,
                    autocompleteProjects.text.toString(), selectedCategories))
            }
            true
        }
    }

    fun HideKeyboard(){
        val activity = context as Activity
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if(activity.currentFocus == null) return
        if(activity.currentFocus!!.windowToken == null) return
        imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
    }

    override fun onResume(){
        super.onResume()
        responseEditado = null //Para mandar la info en intents cuando volvamos a entrar a detalle del proyecto
        type = "projects"
        GetProjects() //For update data
    }

    override fun onPause(){
        super.onPause()
        categoriesOn = false
    }
}