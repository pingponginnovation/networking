package com.example.appnetworking.NewProject

import android.content.Context
import com.example.appgenesys.RequestCrearProyecto
import com.example.appgenesys.ResponseCrearProyecto
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestProyectosGeneral
import com.example.appnetworking.ResponseProyectosGeneral

class NewProjectModel(viewPresenter: NewProject.Presenter, ctx: Context): NewProject.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun CrearProyecto(req: RequestCrearProyecto){
        val retrofit = RetrofitController(context!!)
        retrofit.CreateProject(this, req)
    }

    override fun ErrorProyecto(resp: String){
        presenter.ErrorProyecto(resp)
    }

    override fun SuccessProyecto(response: ResponseCrearProyecto){
        presenter.SuccessProyecto(response)
    }
}