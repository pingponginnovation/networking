//package com.example.appnetworking
package com.example.appgenesys.Retrofit

import com.example.appnetworking.ModelsRetrofit.ResponseActividadesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileOrder
import com.google.gson.annotations.SerializedName

data class ResponseActividades(

    @SerializedName("fecha_evento")
    val fechaevento: String? = null,

    @SerializedName("actividades")
    val data: List<ResponseActividadesOrder>
)