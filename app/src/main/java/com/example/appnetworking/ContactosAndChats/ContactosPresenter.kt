package com.example.appnetworking.ContactosAndChats

import android.content.Context
import com.example.appgenesys.ResponseContacts
import com.example.appnetworking.PreferencesController

class ContactosPresenter(viewMatch: Contactos.View, ctx: Context, prefs: PreferencesController): Contactos.Presenter{

    private val view = viewMatch
    private val model = ContactosModel(this, ctx, prefs)

    //MODEL
    override fun GetContacts(){
        model.GetContacts()
    }

    //VIEW
    override fun ErrorContacts(resp: String){
        view.ErrorContacts(resp)
    }

    override fun SuccessContacts(response: ResponseContacts){
        view.SuccessContacts(response)
    }
}