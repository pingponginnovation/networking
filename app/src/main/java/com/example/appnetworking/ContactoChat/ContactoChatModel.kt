package com.example.appnetworking.ContactoChat

import android.content.Context
import android.util.Log
import com.example.appgenesys.RequestChatThread
import com.example.appgenesys.RequestEnviarMensaje
import com.example.appgenesys.ResponseChatThread
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.ResponseEnviarMensaje

class ContactoChatModel(viewPresenter: ContactoChat.Presenter, ctx: Context, prefs: PreferencesController): ContactoChat.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
        prefes = prefs
    }

    override fun GetMensajes(id: Int){
        val req = RequestChatThread(id)
        val retrofit = RetrofitController(context!!)
        retrofit.GetMessagesChat(this, req)
    }

    override fun ErrorMensajes(resp: String){
        presenter.ErrorMensajes(resp)
    }

    override fun SuccessMensajes(response: ResponseChatThread){
        presenter.SuccessMensajes(response)
    }

    override fun EnviarMensaje(request: RequestEnviarMensaje){
        val retrofit = RetrofitController(context!!)
        retrofit.SendMessage(this, request)
    }

    override fun SuccessEnviarMensaje(resp: ResponseEnviarMensaje){
        presenter.SuccessEnviarMensaje(resp)
    }

    override fun ErrorEnviarMensaje(resp: String){
        presenter.ErrorEnviarMensaje(resp)
    }
}