package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseDatosCitaZoomOrder(

    @field:SerializedName("meeting_ig")
    val id_meeting: String? = null,

    @field:SerializedName("pwd_encrypted")
    val encrypted: String? = null,

    @field:SerializedName("pwd")
    val pwd: String? = null
)