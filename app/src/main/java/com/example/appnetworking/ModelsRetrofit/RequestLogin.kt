package com.example.appnetworking

data class RequestLogin(val username: String, val password: String)