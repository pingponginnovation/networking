package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseAgendaOrder(

    @field:SerializedName("cita_id")
    val id: Int? = null,

    @field:SerializedName("titulo")
    val titulo: String? = null,

    @field:SerializedName("fecha_cita")
    val date_evento: String? = null,

    @field:SerializedName("hora_inicio")
    val inicio_hora: String? = null,

    @field:SerializedName("hora_fin")
    val hora_fin: String? = null,

    @field:SerializedName("asunto")
    val asunto: String? = null,

    @field:SerializedName("ubicacion")
    val location: String? = null,

    @field:SerializedName("tiempo_alerta")
    val alerta: Int? = null,

    @field:SerializedName("flag_editar_cancelar")
    val flag_editar: Int? = null,

    @field:SerializedName("estatus_cita")
    val status_cita: String? = null,

    @SerializedName("asistentes")
    val attendants: List<ResponseAgendaAsistentesOrder>
)