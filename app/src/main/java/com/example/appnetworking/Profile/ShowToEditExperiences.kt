package com.example.appnetworking.Profile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Adapters.ExperiencesAdapter
import com.example.appnetworking.Profile.ProfileView.Companion.responseExperiences
import com.example.appnetworking.R
import kotlinx.android.synthetic.main.show_experiences_toedit_profile.*
import org.jetbrains.anko.startActivity

class ShowToEditExperiences: AppCompatActivity(){

    private var arrayAdapter: ExperiencesAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_experiences_toedit_profile)

        ConfigToolbar()
        SetExperiencias()

        add_experience.setOnClickListener{
            startActivity<CrearExperienciaView>()
        }
    }

    private fun ConfigToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        back_button.setOnClickListener{
            onBackPressed()
        }
    }

    fun SetExperiencias(){
        rclv_edit_experiences.setHasFixedSize(true)
        rclv_edit_experiences.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        arrayAdapter = ExperiencesAdapter()
        arrayAdapter!!.ExperiencesAdapter(responseExperiences!!, this, "EditExp")
        rclv_edit_experiences.adapter = arrayAdapter
    }
}