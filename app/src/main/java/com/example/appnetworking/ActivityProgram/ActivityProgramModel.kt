package com.example.appnetworking.ActivityProgram

import android.content.Context
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appgenesys.Retrofit.ResponseActividades
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestSuscribeConference

class ActivityProgramModel(viewPresenter: ActivityProgram.Presenter, ctx: Context): ActivityProgram.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetListActivities(){
        val retrofit = RetrofitController(context!!)
        retrofit.ListActivities(this)
    }

    override fun SubscribeProgram(req: RequestSuscribeConference){
        val retrofit = RetrofitController(context!!)
        retrofit.SubscribeProgram(this, req)
    }

    override fun ErrorActivities(resp: String){
        presenter.ErrorActivities(resp)
    }

    override fun SuccessActivities(response: ResponseActividades){
        presenter.SuccessActivities(response)
    }

    override fun SuccessSubscribed(resp: ResponseSuscribeConference){
        presenter.SuccessSubscribed(resp)
    }

    override fun ErrorSubscribed(resp: String){
        presenter.ErrorSubscribed(resp)
    }
}