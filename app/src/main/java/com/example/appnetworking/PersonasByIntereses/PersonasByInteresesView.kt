package com.example.appnetworking.PersonasByIntereses

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appgenesys.Retrofit.ResponseProfile
import com.example.appnetworking.Adapters.*
import com.example.appnetworking.ModelsRetrofit.*
import com.example.appnetworking.R
import com.example.appnetworking.ResponseMatch
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_myprofile_new.*
import kotlinx.android.synthetic.main.item_speakers.txt_name
import kotlinx.android.synthetic.main.personas_by_intereses_activity.*
import org.jetbrains.anko.toast

class PersonasByInteresesView: AppCompatActivity(), PersonasByIntereses.View{

    private lateinit var presenter: PersonasByIntereses.Presenter
    private var arrayAdapter: PeopleByInteresesAdapter? = null
    private var arrayAdapterFiltros: PeopleFiltrosAdapter? = null
    private var searchView: SearchView? = null
    private var filtros_array = arrayListOf<String>()
    private var filtros_string = ""
    private var filtroActivo = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.personas_by_intereses_activity)

        SetUpToolbar()
        presenter = PersonasByInteresesPresenter(this, applicationContext)

        btn_add_filter_.setOnClickListener{
            if(edt_filter.text.toString().isEmpty()) toast("Please type a filter")
            else{
                filtros_array.add(edt_filter.text.toString())
                arrayAdapterFiltros!!.notifyDataSetChanged()

                if(filtros_string.isEmpty()) filtros_string += edt_filter.text.toString()
                else filtros_string += ","+edt_filter.text.toString()

                edt_filter.setText("")
                GetMatchesFiltro(filtroActivo, filtros_string)
            }
        }

        filtroActivo = intent.getIntExtra("FILTRO_ACTIVO", -1)
        filtros_string = intent.getStringExtra("STRING_FILTROS")!!
        this.filtros_array = intent.getStringArrayListExtra("ARRAY_FILTROS")!!
        GetMatchesFiltro(filtroActivo, filtros_string)
    }

    fun SetUpToolbar(){
        val toolb = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(toolb)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.arrow_back_black)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchItem = menu!!.findItem(R.id.menu_search_icon)
        if(searchItem != null){

            searchView = searchItem.actionView as SearchView
            searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))

            searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

                override fun onQueryTextSubmit(query: String?): Boolean{
                    arrayAdapter!!.getFilter().filter(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    arrayAdapter!!.getFilter().filter(newText)
                    return false
                }

            })
        }
        return true
    }

    fun GetMatchesFiltro(filtroActivo: Int, filtros: String){
        presenter.GetMatchesFiltro(filtroActivo, filtros)
    }

    fun UpdateList(){
            //TODO. we update the filters when one it's deleted
        filtros_string = ""
        if(!filtros_array.isNullOrEmpty()){

            filtros_array.forEach{
                if(filtros_string.isEmpty()) filtros_string += it
                else filtros_string += ","+it
            }
        }
        GetMatchesFiltro(filtroActivo, filtros_string)
    }

    override fun ErrorMatches(resp: String){
        toast(resp)
    }

    override fun SuccessMatchFiltro(response: ResponseMatch){
        if(!response.data.isNullOrEmpty()){
            LY_NORESULTS_FILTER.visibility = View.GONE
            LY_PEOPLE_FILTER.visibility = View.VISIBLE
            SetData(response.data)
        }
        else{
            LY_NORESULTS_FILTER.visibility = View.VISIBLE
            LY_PEOPLE_FILTER.visibility = View.GONE
            SetData(response.data)
        }

        SetFiltersActive()
    }

    fun SetFiltersActive(){
        rclv_filtros.setHasFixedSize(true)
        rclv_filtros.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        arrayAdapterFiltros = PeopleFiltrosAdapter()
        arrayAdapterFiltros!!.PeopleFiltrosAdapter(filtros_array, this)
        rclv_filtros.adapter = arrayAdapterFiltros
    }

    fun SetData(datos: ArrayList<ResponseMatchOrder>){
        rclv_people_filter.setHasFixedSize(true)
        rclv_people_filter.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        arrayAdapter = PeopleByInteresesAdapter()
        arrayAdapter!!.PeopleByInteresesAdapter(datos, this@PersonasByInteresesView)
        rclv_people_filter.adapter = arrayAdapter
    }

    override fun onResume(){
        super.onResume()
    }

    override fun onPause(){
        super.onPause()
    }

    override fun onSupportNavigateUp(): Boolean{
        onBackPressed()
        return true
    }
}