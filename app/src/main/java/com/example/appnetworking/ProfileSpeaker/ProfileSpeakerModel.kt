package com.example.appnetworking.Profile

import android.content.Context
import com.example.appgenesys.ResponseSuscribeConference
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController
import com.example.appnetworking.RequestSuscribeConference
import com.example.appnetworking.ResponseProfilePonente

class ProfileSpeakerModel(viewPresenter: ProfileSpeaker.Presenter, ctx: Context): ProfileSpeaker.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
    }

    override fun GetProfile(idPonente: Int){
        val retrofit = RetrofitController(context!!)
        retrofit.GetPonente(this, idPonente)
    }

    override fun SuscribeToConference(req: RequestSuscribeConference){
        val retrofit = RetrofitController(context!!)
        retrofit.SuscribeConference(this, req)
    }

    override fun ErrorProfile(resp: String){
        presenter.ErrorProfile(resp)
    }

    override fun SuccessProfile(response: ResponseProfilePonente){
        presenter.SuccessProfile(response)
    }

    override fun SuccessSuscribeConference(response: ResponseSuscribeConference){
        presenter.SuccessSuscribeConference(response)
    }

    override fun ErrorSuscribe(msj: String){
        presenter.ErrorSuscribe(msj)
    }
}