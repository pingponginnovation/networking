package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class ResponseModificarProyectoOrder(

    @field:SerializedName("proyecto_id")
    val id_proyecto: Int? = null,

    @field:SerializedName("titulo")
    val titulo: String? = null,

    @field:SerializedName("categoria")
    val categoria_name: String? = null,

    @field:SerializedName("descripcion")
    val descripcion: String? = null,

    @field:SerializedName("contenido_img")
    val img_proyecto: String? = null,

    @field:SerializedName("contenido_video")
    val video: String? = null,

    @field:SerializedName("flag_editar")
    val flag_editar: Boolean? = null,

    @field:SerializedName("estatus")
    val status: Int? = null
)