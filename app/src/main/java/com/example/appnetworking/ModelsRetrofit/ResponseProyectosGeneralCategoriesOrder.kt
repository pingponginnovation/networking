package com.example.appnetworking.ModelsRetrofit

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class ResponseProyectosGeneralCategoriesOrder(

    @field:SerializedName("categoria_id")
    val id_categorie: Int? = null,

    @field:SerializedName("categoria")
    val categoria: String? = null,

    var selected: Boolean = false
)