package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.Models.ChipsFilter
import com.example.appnetworking.ModelsRetrofit.ResponseProfileExperienciasOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileHabilidadesOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileInteresesOrder
import com.example.appnetworking.R

class SkillsAdapter: RecyclerView.Adapter<SkillsAdapter.ViewHolder>(){

    var list_skills: List<ResponseProfileHabilidadesOrder>? = null
    lateinit var contexto: Context

    fun SkillsAdapter(lista: List<ResponseProfileHabilidadesOrder>, context: Context){
        list_skills = lista
        this.contexto = context
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val titulo = holder.itemView.findViewById(R.id.txt_skill) as TextView
        titulo.text = list_skills!![position].skill
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_skills, parent, false))
    }

    override fun getItemCount(): Int {
        if(list_skills!!.size > 8) return 8
        else return list_skills!!.size
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){

        /*val tituloConf = view.findViewById(R.id.idTxtTitulo) as TextView
        val date = view.findViewById(R.id.idTxtDate) as TextView
        val hora = view.findViewById(R.id.idTxtHour) as TextView
        val salon = view.findViewById(R.id.idTxtRoom) as TextView
        val linear = view.findViewById(R.id.idLinearCard) as LinearLayout

        fun bind(confe: ConferenciasModelo, context: Context){
            tituloConf.text = confe.Nombre
            date.text = confe.Fecha
            hora.text = confe.Hora
            salon.text = confe.Aula

            //var bitmapURL: Bitmap = Picasso.with(context).load(confe.IMG).get() //bitmap
            //cl.setImage(confe.IMG, context, linear)
            linear.setBackgroundResource(R.drawable.background)

            itemView.setOnClickListener{view ->

                context.toast(confe.Nombre)
            }
        }*/
    }
}