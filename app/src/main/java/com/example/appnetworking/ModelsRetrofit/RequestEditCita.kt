package com.example.appnetworking

data class RequestEditCita(
    val cita_id: String,
    val titulo: String,
    val contactos: ArrayList<Int>,
    val fecha_cita: String,
    val hora_inicio: String,
    val hora_fin: String,
    val asunto: String,
    val ubicacion: String,
    val tiempo_recordatorio: Int
)