package com.example.appnetworking.PersonasByIntereses

import com.example.appnetworking.ResponseMatch

interface PersonasByIntereses{

    interface View{
        fun ErrorMatches(resp: String)
        fun SuccessMatchFiltro(response: ResponseMatch)
    }

    interface Presenter{
        fun GetMatchesFiltro(filtro: Int, filtros: String)
        fun ErrorMatches(resp: String)
        fun SuccessMatchFiltro(response: ResponseMatch)
    }

    interface Model{
        fun GetMatchesFiltro(filtro: Int, filtros: String)
        fun ErrorMatches(resp: String)
        fun SuccessMatchFiltro(response: ResponseMatch)
    }
}