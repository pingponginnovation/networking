package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseProfileOrder
import com.example.appnetworking.ModelsRetrofit.ResponseSpeakersOrder
import com.google.gson.annotations.SerializedName

data class ResponseSpeakers(

    @SerializedName("data")
    val data: ArrayList<ResponseSpeakersOrder>
)