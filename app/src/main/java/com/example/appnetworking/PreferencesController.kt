package com.example.appnetworking

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import java.text.SimpleDateFormat
import java.util.*

class PreferencesController(context:Context){

    val NAME_PREFERENCES = "GENESYS"
    private var TOKEN = "TOK"
    private var ID_PROFILE = "ID"
    private var ID_NOTIFICATION = "NOT"
    private var IMG = "img"
    private var STATE_BROADCAST = "false"
    private var PAUSE = "pausa"
    private var NAME_PROFILE = "name"
    private var PUESTO_PROFILE = "puesto"
    private var TYPE_URL = "url"

    companion object {
        var preferences: SharedPreferences? = null
    }
    init {
        if(preferences == null) preferences = context.getSharedPreferences(NAME_PREFERENCES, Context.MODE_PRIVATE)
    }

    fun saveToken(responseToken: String){
        val editor = preferences!!.edit()
        editor.putString(TOKEN, responseToken)
        editor.apply()
    }

    fun SaveIDProfile(id: Int){
        val editor = preferences!!.edit()
        editor.putString(ID_PROFILE, id.toString())
        editor.apply()
    }

    fun SavePuesto(puesto: String){
        val editor = preferences!!.edit()
        editor.putString(PUESTO_PROFILE, puesto)
        editor.apply()
    }

    fun SaveNameProfile(name: String){
        val editor = preferences!!.edit()
        editor.putString(NAME_PROFILE, name)
        editor.apply()
    }

    fun SaveIDNotification(id: Int){
        val editor = preferences!!.edit()
        editor.putString(ID_NOTIFICATION, id.toString())
        editor.apply()
    }

    fun SaveImageMenu(imagen: String){
        val editor = preferences!!.edit()
        editor.putString(IMG, imagen)
        editor.apply()
    }

    fun CloseSession(){
        val editor = preferences!!.edit()
        editor.putString(TOKEN, "")
        editor.putString(NAME_PROFILE, "")
        editor.putString(ID_PROFILE, "")
        editor.apply()
    }

    fun SaveStateBroadcast(state: String){
        val editor = preferences!!.edit()
        editor.putString(STATE_BROADCAST, state)
        editor.apply()
    }

    fun SetPausa(state: String){
        val editor = preferences!!.edit()
        editor.putString(PAUSE, state)
        editor.apply()
    }

    fun SaveURLtype(state: String){
        val editor = preferences!!.edit()
        editor.putString(TYPE_URL, state)
        editor.apply()
    }

    fun getToken() = preferences!!.getString(TOKEN, "")
    fun getMyIDprofile() = preferences!!.getString(ID_PROFILE, "")
    fun getIDnotification() = preferences!!.getString(ID_NOTIFICATION, "")
    fun getNameProfile() = preferences!!.getString(NAME_PROFILE, "Failed")
    fun getPuesto() = preferences!!.getString(PUESTO_PROFILE, "Failed")
    fun getSession() = preferences!!.getString(TOKEN, "")
    fun getPhoto() = preferences!!.getString(IMG, "")
    fun getStateBroadcast() = preferences!!.getString(STATE_BROADCAST, "")
    fun getPause() = preferences!!.getString(PAUSE, "")
    fun getTypeURL() = preferences!!.getString(TYPE_URL, "")
}