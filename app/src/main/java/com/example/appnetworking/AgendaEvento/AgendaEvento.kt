package com.example.appnetworking.AgendaEvento

import com.example.appgenesys.RequestJoinStartMeeting
import com.example.appgenesys.ResponseAgenda
import com.example.appnetworking.RequestDatosCita
import com.example.appnetworking.ResponseDatosCita
import com.example.appnetworking.ResponseInfoGenius
import com.example.appnetworking.ResponseJoinStartMeeting

interface AgendaEvento{

    interface View{
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)

        fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting)
        fun ErrorJoinStartMeeting(resp: String)
    }

    interface Presenter{
        fun GetInfoCita(request: RequestDatosCita)
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)

        fun JoinStartMeeting(req: RequestJoinStartMeeting)
        fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting)
        fun ErrorJoinStartMeeting(resp: String)
    }

    interface Model{
        fun GetInfoCita(request: RequestDatosCita)
        fun ErrorCita(resp: String)
        fun SuccessInfo(response: ResponseDatosCita)

        fun JoinStartMeeting(req: RequestJoinStartMeeting)
        fun SuccessJoinStartMeeting(response: ResponseJoinStartMeeting)
        fun ErrorJoinStartMeeting(resp: String)
    }
}