package com.example.appnetworking.ContactosAndChats

import android.content.Context
import com.example.appgenesys.ResponseContacts
import com.example.appgenesys.Retrofit.RetrofitController
import com.example.appnetworking.PreferencesController

class ContactosModel(viewPresenter: Contactos.Presenter, ctx: Context, prefs: PreferencesController): Contactos.Model{

    private val presenter = viewPresenter
    private var context: Context? = null
    private var prefes: PreferencesController? = null

    init {
        context = ctx
        prefes = prefs
    }

    override fun GetContacts(){
        val retrofit = RetrofitController(context!!)
        retrofit.GetListaContacts(this)
    }

    override fun ErrorContacts(resp: String){
        presenter.ErrorContacts(resp)
    }

    override fun SuccessContacts(response: ResponseContacts){
        presenter.SuccessContacts(response)
    }
}