package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.ResponseMatchOrder
import com.example.appnetworking.ModelsRetrofit.ResponseMatchUsersBusquedaOrder
import com.google.gson.annotations.SerializedName

data class ResponseEditProfile(

    @SerializedName("mensaje")
    val msj: String? = null
)