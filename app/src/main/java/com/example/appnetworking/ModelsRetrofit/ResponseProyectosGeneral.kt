package com.example.appnetworking

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseProyectosGeneral(

    @SerializedName("datos")
    val data: ArrayList<ResponseProyectosGeneralOrder>,

    @SerializedName("categorias")
    val categories: ArrayList<ResponseProyectosGeneralCategoriesOrder>
)