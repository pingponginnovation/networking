package com.example.appgenesys.Login

import com.example.appnetworking.ResponseMatch
import com.example.appnetworking.ResponsePerfilContacto

interface Matches{

    interface View{
        fun GetMatches(typeFiltro: Int, filtros: String)
        fun ErrorMatches(resp: String)
        fun SuccessMatches(response: ResponseMatch)
        fun SearchContacto(id_perfil: Int)
        fun ContactoSuccess(response: ResponsePerfilContacto)
    }

    interface Presenter{
        fun GetMatches(filtro: Int, searchs: String)
        fun ErrorMatches(resp: String)
        fun SuccessMatches(response: ResponseMatch)
        fun SearchContacto(id_perfil: Int)
        fun ContactoSuccess(response: ResponsePerfilContacto)
    }

    interface Model{
        fun GetMatches(filtro: Int, searchs: String)
        fun ErrorMatches(resp: String)
        fun SuccessMatches(response: ResponseMatch)
        fun SearchContacto(id_perfil: Int)
        fun ContactoSuccess(response: ResponsePerfilContacto)
    }
}