package com.example.appnetworking.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.appnetworking.ContactoChat.ContactoChatView
import com.example.appnetworking.ContactosAndChats.ChatsFragment
import com.example.appnetworking.R
import com.squareup.picasso.Picasso
import java.util.ArrayList

class ChatsAdapter: RecyclerView.Adapter<ChatsAdapter.ViewHolder>(), Filterable{

    var list: ArrayList<ChatsFragment.Chats>? = null
    private var listFiltered: ArrayList<ChatsFragment.Chats>? = null
    lateinit var contexto: Context
    private var mi_id = -1

    fun ChatsAdapter(lista: ArrayList<ChatsFragment.Chats>, context: Context, mi_id: Int){
        list = lista
        listFiltered = lista
        contexto = context
        this.mi_id = mi_id
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int){

        val name = holder.itemView.findViewById(R.id.txt_name_c) as TextView
        val msj = holder.itemView.findViewById(R.id.message_chat) as TextView
        val timestamp = holder.itemView.findViewById(R.id.timestamp) as TextView
        val msj_unread_total = holder.itemView.findViewById(R.id.counter) as TextView
        val foto = holder.itemView.findViewById(R.id.my_img) as ImageView
        val icon_tick = holder.itemView.findViewById(R.id.icon_tick) as ImageView

        name.text = listFiltered!![position].name + " " + listFiltered!![position].apellidos
        msj.text = listFiltered!![position].lastMSJ
        timestamp.text = listFiltered!![position].hora
        if(listFiltered!![position].msj_unread_total > 0){
            val n: String = listFiltered!![position].msj_unread_total.toString()
            msj_unread_total.text = n
            msj_unread_total.visibility = View.VISIBLE
        }
        else msj_unread_total.visibility = View.GONE

        Picasso.get().load(listFiltered!![position].img).into(foto)
        if(listFiltered!![position].ID_sender == mi_id) icon_tick.visibility = View.VISIBLE
        else icon_tick.visibility = View.GONE

        holder.itemView.setOnClickListener{
            val intent = Intent(contexto, ContactoChatView::class.java)
            intent.putExtra("ID_HIS_PROFILE", listFiltered!![position].id_his_profile)
            intent.putExtra("NAME", listFiltered!![position].name)
            intent.putExtra("PUESTO", listFiltered!![position].puesto)
            intent.putExtra("IMG", listFiltered!![position].img)
            intent.putExtra("CHAT_ID", listFiltered!![position].chatID)
            contexto.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_chats, parent, false))
    }

    override fun getItemCount(): Int{
        return listFiltered!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {

                val charString = charSequence.toString()

                if (charString.isEmpty()) listFiltered = list
                else{

                    val filteredList = ArrayList<ChatsFragment.Chats>()
                    for (row in list!!) {

                        if (row.name.toString().toLowerCase().contains(charString.toLowerCase()) || row.apellidos.toString().toLowerCase().contains(charString.toLowerCase()))
                            filteredList.add(row)
                    }

                    listFiltered = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                listFiltered = filterResults.values as ArrayList<ChatsFragment.Chats>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view)
}