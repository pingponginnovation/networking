//package com.example.appnetworking
package com.example.appgenesys

import com.example.appnetworking.ModelsRetrofit.*
import com.google.gson.annotations.SerializedName

data class ResponseDateTimeOrder(

    @SerializedName("fecha_hora")
    val fecha_hora: String? = null,

    @SerializedName("fecha")
    val fecha: String? = null,

    @SerializedName("hora")
    val hora: String? = null

)